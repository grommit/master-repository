# No previous file for Hadai

culture = caddo
religion = totemism
capital = "Hadai"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1760.1.1  = {	owner = CAD
		controller = CAD
		add_core = CAD
		trade_goods = cotton
		is_city = yes } #Great Plain tribes spread over vast territories
