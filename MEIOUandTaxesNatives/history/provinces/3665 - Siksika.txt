# No previous file for Siksika

add_core = BLA
owner = BLA
controller = BLA
is_city = yes
culture = blackfoot
religion = totemism
capital = "Siksiksa"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 3
native_hostileness = 4

1792.1.1 = { 	
		owner = GBR
		controller = GBR
		citysize = 500
		culture = english
		religion = protestant 
		trade_goods = fur } #Fort Dunvegan
1817.1.1 = { add_core = GBR }
