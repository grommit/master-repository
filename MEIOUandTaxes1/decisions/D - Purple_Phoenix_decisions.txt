# rome restored???

country_decisions = {

	triumph_greece = {
		potential = {
			tag = BYZ
			has_dlc = "Purple Phoenix"
			NOT = { has_country_flag = triumph_greece }
			greece_region = { owned_by = BYZ }
		}
		allow = {
			greece_region = { type = all owned_by = BYZ }
			
		}
		effect = {
			add_prestige = 10
			add_army_tradition = 25
			set_country_flag = triumph_greece
		}
		ai_will_do = {
			factor = 1
		}
	}

	triumph_balkans = {
		potential = {
			tag = BYZ
			has_dlc = "Purple Phoenix"
			NOT = { has_country_flag = triumph_balkans }
			OR = {
				east_balkan_region = { owned_by = BYZ }
				west_balkan_region = { owned_by = BYZ }
			}
		}
		allow = {
			owns = 136
			owns = 137
			owns = 147
			owns = 151
			owns = 157
			owns = 159
			owns = 1396
			owns = 1401
			owns = 1426
			owns = 1427
			owns = 2238
			owns = 2374
			owns = 2381
			owns = 150
			owns = 1848
			owns = 2501
			owns = 155
			owns = 1591
			owns = 148
			owns = 1424
			owns = 2237
			owns = 2373
			owns = 138
			owns = 2388
			owns = 2571
		}
		effect = {
			add_prestige = 20
			add_army_tradition = 25
			set_country_flag = triumph_balkans
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	triumph_asia = {
		potential = {
			tag = BYZ
			has_dlc = "Purple Phoenix"
			NOT = { has_country_flag = triumph_asia }
			roman_asia_group = { owned_by = BYZ }
		}
		allow = {
			roman_asia_group = { type = all owned_by = BYZ }
			
		}
		effect = {
			add_prestige = 10
			add_army_tradition = 25
			set_country_flag = triumph_asia
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	triumph_anatolia = {
		potential = {
			tag = BYZ
			has_dlc = "Purple Phoenix"
			NOT = { has_country_flag = triumph_anatolia }
			OR = {
				coastal_anatolia_region = { owned_by = BYZ }
				inland_anatolia_region = { owned_by = BYZ }
			}
		}
		allow = {
			coastal_anatolia_region = { type = all owned_by = BYZ }
			inland_anatolia_region = { type = all owned_by = BYZ }
		}
		effect = {
			add_prestige = 20
			add_army_tradition = 25
			set_country_flag = triumph_anatolia
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	triumph_italy = {
		potential = {
			tag = BYZ
			has_dlc = "Purple Phoenix"
			NOT = { has_country_flag = triumph_italy }
			italy_region = { owned_by = BYZ }
		}
		allow = {
			italy_region = { type = all owned_by = BYZ }
			
		}
		effect = {
			add_prestige = 35
			add_army_tradition = 25
			set_country_flag = triumph_italy
		}
		ai_will_do = {
			factor = 1
		}
	}
	rebuild_theodosian_walls = {
		potential = {
			owns = 1402
			NOT = { 1402 = { has_province_modifier = theodosian_walls } }
			}
		allow = {
			1402 = { 
				controlled_by = owner
				has_siege = no
				}
			treasury = 100
			stability = 1
			OR = {
				advisor = fortification_expert
				MIL = 4
				}
			}
		effect = {
			1402 = { add_province_modifier = { name = theodosian_walls duration = -1 } }
			add_treasury = -100
			add_legitimacy = 1
			}
		ai_will_do = {
			factor = 1
			modifier = {
				NOT = { treasury = 200 }
				factor = 0
				}
			modifier = {
				1402 = { is_capital = no }
				factor = 0
				}
			}
		}
	repopulate_constantinople = {
		potential = {
			owns = 1402
			NOT = { 1402 = { has_province_modifier = major_city } }
			}
		allow = {
			1402 = {
				controlled_by = owner
				has_siege = no
				}
			total_development = 300
			ADM = 3
			}
		effect = {
			add_adm_power = -100
			add_treasury = -50
			1402 = { add_permanent_province_modifier = { name = major_city duration = -1 } }
			1402 = { change_trade_goods = services }
			}
		ai_will_do = {
			factor = 1
			modifier = {
				NOT = { treasury = 100 }
				factor = 0
				}
			}
		}
	
}
