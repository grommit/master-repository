########################################
#                                      #
#      Mughals.txt      #
#                                      #
########################################
#
# Prussia should move its capital to Berlin
#
########################################

country_decisions = {
	
	move_capital_to_berlin = {
		potential = {
			OR = {
				tag = PRU
				AND = {
					tag = BRA
					OR = {
						AND = {
							is_year = 1400
							NOT = { dynasty = "von Luxemburg" }
						}
						is_year = 1450
					}
				}
			}
			owns_core_province = 50 #Mittlemark
			NOT = { capital = 50 } #Mittlemark
			NOT = { has_country_flag = relocated_capital_berlin }
		}
		allow = {
			adm_power = 200
			is_core = 50
			is_at_war = no
		}
		effect = {
			set_country_flag = relocated_capital_berlin
			add_stability_1 = yes
			set_capital = 50
			add_adm_power = -200
			add_prestige = 10
			50 = {
			 	add_base_tax = 2
			 	add_base_production = 2
			 	add_base_manpower = 2
			}
			50 = {
				ROOT = { add_accepted_culture = PREV }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}