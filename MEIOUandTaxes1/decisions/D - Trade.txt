country_decisions = {

	#statute_of_monopolies = {
	#	potential = {
	#		num_of_merchants = 3
	#		NOT = { has_country_modifier = the_statute_of_monopolies }
	#	}
	#	allow = {
	#		mercantilism = 705
	#		trade_efficiency = 0.5
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "the_statute_of_monopolies"
	#			duration = -1
	##		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	close_foreign_trade = {
		potential = {
			technology_group = chinese
			any_known_country = {
				technology_group = western
				any_active_trade_node = {
					province_group = chinese_coast_group
					is_strongest_trade_power = PREV
				}
			}
			NOT = { has_country_modifier = condemn_western_influences }
		}
		allow = {
			any_known_country = {
				technology_group = western
				any_active_trade_node = {
					province_group = chinese_coast_group
					is_strongest_trade_power = PREV
				}
				NOT = { has_opinion = { who = ROOT value = -100 } }
			}
			mil = 3
		}
		effect = {
			add_country_modifier = {
				name = narrowminded_modifier
				duration = 3650
			}
			add_mercantilism = 5
			add_country_modifier = {
				name = "condemn_western_influences"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	close_the_scheldt = {
		potential = {
			OR = {
				owns = 96
				owns = 97
			}
			2360 = {
				has_province_modifier = center_of_trade_modifier
				NOT = { owned_by = ROOT }
			}
		}
		allow = {
			owns = 96
			owns = 97
			dip_tech = 30
			OR = {
				any_country = {
					owns = 2360
					war_with = ROOT
				}
				AND = {
					any_country = {
						owns = 2360
						NOT = { primary_culture = flemish }
						NOT = { primary_culture = brabantian }
						NOT = { primary_culture = dutch }
					}
					OR = {
						primary_culture = flemish
						primary_culture = brabantian
						primary_culture = dutch
					}
				}
			}
		}
		effect = {
			2360 = {
				province_event = { id = center_of_trade.3 days = 5 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	request_byz_trading_privileges = {
		potential = {
			OR = {
				government = merchant_republic
				government = venetian_republic
			}
			OR = {
				tag = VEN
				tag = GEN
			}
			exists = BYZ
			1402 = {
				owned_by = BYZ
				NOT = { has_trade_modifier = { who = VEN name = constantinople_trade_privileges } }
				NOT = { has_trade_modifier = { who = GEN name = constantinople_trade_privileges } }
			}
			BYZ = {
				NOT = { has_country_flag = refused_to_give_trade_privileges_151 }
				NOT = { has_country_modifier = trade_privileges_renewal_timer }
			}
		}
		allow = {
			NOT = { war_with = BYZ }
			treasury = 100
		}
		effect = {
			BYZ = { country_event = { id = trade_lt.44000 } }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					VEN = { ai = no }
					GEN = { ai = no }
				}
			}
		}
	}
	
	request_muslim_trading_privileges = {
		potential = {
			OR = {
				tag = VEN
				tag = GEN
			}
			1402 = {
				NOT = { owned_by = BYZ }
				owner = {
					religion_group = muslim
					NOT = { has_country_flag = refused_to_give_trade_privileges_151 }
					NOT = { has_country_modifier = trade_privileges_renewal_timer }
				}
				NOT = { has_trade_modifier = { who = VEN name = constantinople_trade_privileges } }
				NOT = { has_trade_modifier = { who = GEN name = constantinople_trade_privileges } }
			}
			OR = {
				government = merchant_republic
				government = venetian_republic
			}
		}
		allow = {
			treasury = 100
			1402 = {
				owner = { NOT = { war_with = ROOT } }
			}
		}
		effect = {
			1402 = {
				owner = { country_event = { id = trade_lt.44007 } }
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					VEN = { ai = no }
					GEN = { ai = no }
				}
			}
		}
	}
	
	genoa_sicilian_trade_privileges = {
		potential = {
			tag = GEN
			government = merchant_republic
			NOT = { 2531 = { has_trade_modifier = { who = GEN name = naples_trade_privileges } } }
			NOT = { 125 = {	owned_by = ROOT } }
			NOT = { 2531 = { owned_by = ROOT } }
		}
		allow = {
			treasury = 75
			1247 = { owned_by = ROOT }
			2851 = { owned_by = ROOT }
		}
		effect = {
			2531 = {
				add_trade_modifier = {
					who = ROOT
					duration = -1
					power = 25
					key = naples_trade_privileges
				}
			}
			add_treasury = -50
		}
		ai_will_do = {
			factor = 1
		}
	}

	stop_foreign_slave_trade = {
		potential = {
			has_country_modifier = slave_empire
		}
		allow = {
			has_institution = feudalism
			has_institution = new_world_i
			adm_tech = 12
			NOT = { government = tribal }
			adm_power = 200
		}
		effect = {
			add_adm_power = -200
			remove_country_modifier = slave_empire
			if = {
				limit = {
					any_owned_province = {
						has_province_modifier = major_slave_market
					}
				}
				every_owned_province = {
					change_trade_goods = unknown
					remove_province_modifier = major_slave_market
				}
			}
			custom_tooltip = stop_slave_trade_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						trade_goods = slaves
					}
					add_province_modifier = {
						name = "local_slave_chief"
						duration = 5475
					}
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}
