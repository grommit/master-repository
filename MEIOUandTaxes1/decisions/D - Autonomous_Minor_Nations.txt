country_decisions = {

	control_kurdish_princelings = {
		potential = {
			any_owned_province = {
				has_province_modifier = kurdish_princelings
			}
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
			mil_power = 150
			is_at_war = no
		}
		effect = {
			add_mil_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = kurdish_princelings
				}
				remove_province_modifier = kurdish_princelings
				if = {
					limit = {
						NOT = { owner = { tag = KRD }}
					}
					spawn_rebels = {
						type = noble_rebels
						size = 2
					}
				}
			}
		}
		ai_will_do = {
		    factor = 1
			modifier = {
				factor = 0
				NOT = { primary_culture = kurdish }
			}
			modifier = {
				factor = 0
				NOT = { stability = 2 }
			}
			modifier = {
				factor = 0
				is_at_war = no
			}
		}
		ai_importance = 1000
	}

	assimilate_ditmarsian_republic = {
		potential = {
			owns = 3722
			3722 = {
				has_province_modifier = ditmarsian_republic
			}
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -4 } }
			adm_power = 150
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			3722 = {
				remove_province_modifier = ditmarsian_republic
				spawn_rebels = {
					type = burgher_rebels
					size = 3
				}
			}
		}
		ai_will_do = {
		    factor = 1
			modifier = {
				factor = 0
				NOT = { stability = 2 }
			}
			modifier = {
				factor = 0
				is_at_war = no
			}
		}
		ai_importance = 100
	}

	assimilate_duchy_of_wroclaw = {
		potential = {
			owns = 1359
			1359 = {
				has_province_modifier = duchy_of_wroclaw
			}
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -4 } }
			OR = {
				tag = WRO
				dip_power = 150
			}
			is_at_war = no
		}
		effect = {
			1359 = {
				remove_province_modifier = duchy_of_wroclaw
				if = {
					limit = {
						NOT = { owner = { tag = WRO }}
					}
					owner = { add_dip_power = -100 }
					spawn_rebels = {
						type = noble_rebels
						size = 3
					}
				}
			}
		}
		ai_will_do = {
		    factor = 1
			modifier = {
				factor = 0
				culture_group = west_slavic
				NOT = { tag = WRO }
			}
			modifier = {
				factor = 0
				is_at_war = no
			}
		}
		ai_importance = 100
	}

	remove_aymara_autonomy = {
		potential = {
			NOT = { primary_culture = aimara }
			any_owned_province = {
				has_province_modifier = coalition_member
			}
		}
		allow = {
			dip_power = 100
			is_at_war = no
		}
		effect = {
			add_dip_power = -50
			every_owned_province = {
				limit = {
					has_province_modifier = coalition_member
				}
				remove_province_modifier = coalition_member
				add_core = AYM
				spawn_rebels = {
					type = nationalist_rebels
					size = 2
				}
			}
		}
		ai_will_do = {
		    factor = 1
			modifier = {
				factor = 0
				NOT = { culture_group = andean_american } 
			}
			modifier = {
				factor = 0
				NOT = { stability = 2 }
			}
			modifier = {
				factor = 0
				is_at_war = no
			}
		}
		ai_importance = 100
	}

	revoke_city_charter = {
		potential = {
			any_owned_province = {
				has_province_modifier = city_charter
			}
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -4 } }
			adm_power = 150
			is_at_war = no
		}
		effect = {
			add_adm_power = -100
			every_owned_province = {
				limit = {
					has_province_modifier = city_charter
				}
				remove_province_modifier = city_charter
				spawn_rebels = {
					type = burgher_rebels
					size = 3
				}
			}
		}
		ai_will_do = {
		    factor = 0
		}
	}
	abolish_qasim_khanate = {
		potential = {
			any_owned_province = {
				has_province_modifier = qasim_khanate
			}
		}
		allow = {
			mil_power = 150
			is_at_war = no
			OR = {
				adm_tech = 35
				technology_group = western
			}
		}
		effect = {
			add_mil_power = -100
			set_country_flag = qasim_abolished
			every_owned_province = {
				limit = {
					has_province_modifier = qasim_khanate
				}
				remove_province_modifier = qasim_khanate
				if = {
					limit = {
						NOT = { owner = { tag = KAZ }}
					}
					spawn_rebels = {
						type = noble_rebels
						size = 2
					}
				}
			}
		}
		ai_will_do = {
		    factor = 1
			modifier = {
				factor = 0
				NOT = { stability = 2 }
			}
		}
		ai_importance = 1000
	}

}
