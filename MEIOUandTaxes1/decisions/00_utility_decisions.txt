country_decisions = {

	show_utility_decisions = {
		potential = {
			ai = no
			has_country_flag = hide_utility_decisions
		}
		allow = {
		}
		effect = {
			custom_tooltip = show_utility_decisions_tt
			hidden_effect = {
				clr_country_flag = hide_utility_decisions
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	hide_utility_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
		}
		allow = {
		}
		effect = {
			custom_tooltip = hide_utility_decisions_tt
			hidden_effect = {
				set_country_flag = hide_utility_decisions
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## BYOA
	revise_demesne = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
		}
		allow = {}			
		effect = {
			census_and_ce_calc_effect = yes
			indepotent_byoa_calc_effect = yes
			cleanup_centralisation = yes
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## CE
	show_ce_modifiers = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			NOT = { has_country_flag = showing_ce_modifiers }
		}
		allow = {
		}			
		effect = {
			custom_tooltip = show_ce_modifiers_tt
			hidden_effect = {
				set_country_flag = showing_ce_modifiers
				every_owned_province = {
					if = { limit = { has_province_modifier = perfect_communication } add_province_modifier = { name = TT_perfect_communication duration = -1 } }
					if = { limit = { has_province_modifier = rapid_communication } add_province_modifier = { name = TT_rapid_communication duration = -1 } }
					if = { limit = { has_province_modifier = quick_communication } add_province_modifier = { name = TT_quick_communication duration = -1 } }
					if = { limit = { has_province_modifier = fairly_quick_communication } add_province_modifier = { name = TT_fairly_quick_communication duration = -1 } }
					if = { limit = { has_province_modifier = decent_communication } add_province_modifier = { name = TT_decent_communication duration = -1 } }
					if = { limit = { has_province_modifier = average_communication } add_province_modifier = { name = TT_average_communication duration = -1 } }
					if = { limit = { has_province_modifier = moderate_communication } add_province_modifier = { name = TT_moderate_communication duration = -1 } }
					if = { limit = { has_province_modifier = difficult_communication } add_province_modifier = { name = TT_difficult_communication duration = -1 } }
					if = { limit = { has_province_modifier = poor_communication } add_province_modifier = { name = TT_poor_communication duration = -1 } }
					if = { limit = { has_province_modifier = very_poor_communication } add_province_modifier = { name = TT_very_poor_communication duration = -1 } }
					if = { limit = { has_province_modifier = terrible_communication } add_province_modifier = { name = TT_terrible_communication duration = -1 } }
				}
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## CE
	hide_ce_modifiers = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			has_country_flag = showing_ce_modifiers
		}
		allow = {
		}			
		effect = {
			custom_tooltip = hide_ce_modifiers_tt
			hidden_effect = {
				clr_country_flag = showing_ce_modifiers
				every_owned_province = {
					remove_province_modifier = TT_perfect_communication
					remove_province_modifier = TT_rapid_communication
					remove_province_modifier = TT_quick_communication
					remove_province_modifier = TT_fairly_quick_communication
					remove_province_modifier = TT_decent_communication
					remove_province_modifier = TT_average_communication
					remove_province_modifier = TT_moderate_communication
					remove_province_modifier = TT_difficult_communication
					remove_province_modifier = TT_poor_communication
					remove_province_modifier = TT_very_poor_communication
					remove_province_modifier = TT_terrible_communication
				}
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## DO
	show_events = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			NOT = { has_country_flag = show_development }
		}
		allow = {
		}
		effect = {
			custom_tooltip = show_events_tt
			hidden_effect = {
				set_country_flag = show_development
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	conceal_events = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			has_country_flag = show_development
		}
		allow = {
		}
		effect = {
			custom_tooltip = conceal_events_tt
			hidden_effect = {
				clr_country_flag = show_development
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	show_policies = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			NOT = { has_country_flag = show_policies }
		}
	
		allow = {
		}
		effect = {
			custom_tooltip = show_policies_tt
			hidden_effect = {
				set_country_flag = show_policies
				country_event = { 
					id = DO_introduction.005
				}
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	conceal_policies = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			has_country_flag = show_policies
		}
		allow = {
		}
		effect = {
			custom_tooltip = conceal_policies_tt
			hidden_effect = {
				clr_country_flag = show_policies
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## SI
	hide_cultural_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
			    technology_group = western
				culture_group = west_slavic
			}
		}
		allow = {
		}
	 	effect = {
			set_country_flag = cultural_decision_off
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	view_cultural_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			has_country_flag = cultural_decision_off
		}
		allow = {
		}
	 	effect = {
			clr_country_flag = cultural_decision_off
		}
		ai_will_do = {
			factor = 1
		}
	}
}