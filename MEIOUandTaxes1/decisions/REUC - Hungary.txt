country_decisions = {

	revoke_pacta_conventa = {
		major = yes
		potential = {
			CRO = {
				has_country_flag = pacta_conventa
				is_subject_of = ROOT
			}
		}
		allow = {
			OR = {
				ADM = 3
				advisor = alderman
				advisor = statesman
			}
			is_at_war = no
			adm_power = 150
			NOT = { overextension_percentage = 0.01 }
			NOT = { check_variable = { which = "centralization_decentralization" value = 2 } }
		}
		effect = {
			CRO = {
				clr_country_flag = pacta_conventa
				add_opinion = {
					who = ROOT
					modifier = pacta_conventa_revoked
				}
			}
			add_adm_power = -150
		}
		ai_will_do = {
			factor = 1
		}
	}

}
