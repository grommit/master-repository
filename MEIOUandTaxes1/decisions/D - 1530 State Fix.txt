country_decisions = {	

	annex_SAR_1530 = {
		potential = {
			NOT = { has_country_flag = annex_SAR_1530 }
			tag = SPA
			has_global_flag = 1530_start_date
			has_country_flag = spanish_start
		}
		allow = {
			SAR = {
				ai = yes
			}
		}
		effect = {
			inherit = SAR
			set_country_flag = annex_SAR_1530
			add_accepted_culture = sardinian
			every_owned_province = {
				limit = {
					OR = {	
						province_id = 127
						province_id = 2241
						province_id = 2242
						province_id = 2852
					}
					NOT = { has_province_modifier = kingdom_of_sardinia }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_sardinia
					duration = -1
				}				
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 400
		}
	}

	annex_KNP_1530 = {
		potential = {
			NOT = { has_country_flag = annex_KNP_1530 }
			tag = SPA
			has_global_flag = 1530_start_date
			has_country_flag = spanish_start
		}
		allow = {
			KNP = {
				ai = yes
			}
		}
		effect = {
			inherit = KNP
			set_country_flag = annex_KNP_1530
			add_accepted_culture = neapolitan
			add_accepted_culture = sicilian
			every_owned_province = {
				limit = {
					OR = {
						area = naples_area
						area = calabria_area
						area = abruzzi_area
						area = apulia_area
					}
					NOT = { has_province_modifier = kingdom_of_naples }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_naples
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					area = sicily_area
					NOT = { has_province_modifier = kingdom_of_sicily }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_sicily
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 400
		}
	}

	annex_netherlands_1530 = {
		potential = {
			NOT = { has_country_flag = annex_netherlands_1530 }
			tag = SPA
			has_global_flag = 1530_start_date
			has_country_flag = spanish_start
		}
		allow = {
			BUR = {
				ai = yes
			}
		}
		effect = {
			set_country_flag = annex_netherlands_1530
			BUR = {
				every_owned_province = {
					limit = {
						OR = {
							region = belgii_region
							region = low_countries_region
							area = picardy_area
						}
					}
					cede_province = SPA
					add_core = SPA
					remove_core = BUR
				}
			}
			99 = { add_permanent_claim = SPA }
			2449 = { add_permanent_claim = SPA }
			1371 = { add_permanent_claim = SPA }
			2353 = { add_permanent_claim = SPA }
			add_accepted_culture = moselfranconian
			add_accepted_culture = picard
			every_owned_province = {
				limit = {
					OR = {
						province_id = 94
						province_id = 1511
					}
					NOT = { has_province_modifier = duchy_of_luxembourg }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_luxembourg
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					OR = {	
						province_id = 88
						province_id = 2307
						province_id = 2353
					}
					NOT = { has_province_modifier = county_of_artois }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_artois
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			add_accepted_culture = flemish
			every_owned_province = {
				limit = {
					OR = {	
						province_id = 87
						province_id = 90
						province_id = 2363
					}
					NOT = { has_province_modifier = county_of_flanders }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_flanders
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 87
					NOT = { has_province_modifier = bishopric_of_tournai }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = bishopric_of_tournai
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					OR = {
						province_id = 92
						province_id = 2367
						province_id = 95
						province_id = 2360
					}
					NOT = { has_province_modifier = duchy_of_brabant }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_brabant
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 2795
					NOT = { has_province_modifier = duchy_of_limburg }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_limburg
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 2367
					NOT = { has_province_modifier = lordship_of_mechelen }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_mechelen
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			add_accepted_culture = wallonian
			every_owned_province = {
				limit = {
					province_id = 2364
					NOT = { has_province_modifier = county_of_namur }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_namur
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 91
					NOT = { has_province_modifier = county_of_hainaut }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_hainaut
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			add_accepted_culture = dutch
			every_owned_province = {
				limit = {
					OR = {
						province_id = 97
						province_id = 2370
					}
					NOT = { has_province_modifier = county_of_holland }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_holland
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 96
					NOT = { has_province_modifier = county_of_zeeland }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_zeeland
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					province_id = 98
					NOT = { has_province_modifier = lordship_of_utrecht }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_utrecht
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			every_owned_province = {
				limit = {
					OR = {
						province_id = 1372
						province_id = 2450
					}	
					NOT = { has_province_modifier = lordship_of_overijssel }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_overijssel
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	#Ottomans
	annex_MAM_1530 = {
		potential = {
			NOT = { has_country_flag = annex_MAM_1530 }
			tag = TUR
			has_global_flag = 1530_start_date
			has_country_flag = turkish_start
		}
		allow = {
			MAM = {
				ai = yes
			}
		}
		effect = {
			inherit = MAM
			set_country_flag = annex_MAM_1530
			every_owned_province = {
				limit = {
					region = egypt_region
					NOT = { has_province_modifier = local_mamlukes }
				}
				add_permanent_province_modifier = {
					name = local_mamlukes
					duration = -1
				}
				add_core = ROOT
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	annex_SYR_1530 = {
		potential = {
			NOT = { has_country_flag = annex_SYR_1530 }
			tag = TUR
			has_global_flag = 1530_start_date
			has_country_flag = turkish_start
		}
		allow = {
			SYR = {
				ai = yes
			}
		}
		effect = {
			inherit = SYR
			set_country_flag = annex_SYR_1530
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	annex_QUD_1530 = {
		potential = {
			NOT = { has_country_flag = annex_QUD_1530 }
			tag = TUR
			has_global_flag = 1530_start_date
			has_country_flag = turkish_start
		}
		allow = {
			SYR = {
				ai = yes
			}
		}
		effect = {
			inherit = BHA
			set_country_flag = annex_QUD_1530
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	annex_HEJ_1530 = {
		potential = {
			NOT = { has_country_flag = annex_HEJ_1530 }
			tag = TUR
			has_global_flag = 1530_start_date
			has_country_flag = turkish_start
		}
		allow = {
			HEJ = {
				ai = yes
			}
		}
		effect = {
			inherit = HEJ
			set_country_flag = annex_HEJ_1530
		}
		ai_will_do = {
			factor = 400
		}
	}

}
