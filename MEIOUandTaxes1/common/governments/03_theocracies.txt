##############################   Religious   ##############################
##############################  Governments  ##############################

papal_government = { #Pope
	religion = yes
	papacy = yes
	
	color = { 230 195 195 }
	
	valid_for_new_country = no

	royal_marriage = no
	has_devotion = yes
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -4
	
	rank = {
		5 = {
			tolerance_own = 3
			tolerance_heretic = -2
			tolerance_heathen = -2
			prestige = 2
						
			global_autonomy = -0.10
		}
	}
	fixed_rank = 5
}

theocratic_government = {
	religion = yes
	
	color = { 230 175 175 }
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	
	royal_marriage = no
	has_devotion = yes
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		1 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 1
			
			global_autonomy = -0.05
		}
		2 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 2
					
			global_autonomy = -0.05
		}
		3 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 3
					
			global_autonomy = -0.05
		}
		4 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 4
					
			global_autonomy = -0.05
		}
		5 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 5
					
			global_autonomy = -0.05
		}
		6 = {
			tolerance_own = 2
			tolerance_heretic = -1
			papal_influence = 6
					
			global_autonomy = -0.05
		}
	}
}

monastic_order_government = {
	religion = yes
	
	valid_for_new_country = no valid_for_nation_designer = yes nation_designer_cost = 0
	
	color = { 230 155 155 }
	
	royal_marriage = no
	has_devotion = yes
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		4 = {
			tolerance_own = 2
			tolerance_heretic = -1
			tolerance_heathen = -2
			discipline = 0.025
			global_manpower_modifier = 0.10
			papal_influence = 2
			land_forcelimit_modifier = 0.10
			land_maintenance_modifier = -0.10
			fort_maintenance_modifier = -0.25
			
			global_autonomy = -0.06
		}
	}
	fixed_rank = 4
}
