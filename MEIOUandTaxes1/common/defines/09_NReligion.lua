
NDefines.NReligion.MAYA_COLLAPSE_PROVINCES_PER_REFORM = 1					-- Maya keeps this many extra provinces per reform
NDefines.NReligion.DOOM_REDUCTION_FROM_OCCUPATION = 2						-- Multiplied by base tax
NDefines.NReligion.SACRIFICE_COOLDOWN = 5									-- Years before you can sacrifice a ruler/heir from same country
NDefines.NReligion.AUTHORITY_FROM_AUTONOMY = 0.5							-- Authority loss from granting autonomy
NDefines.NReligion.CONVERSION_ZEAL_DURATION = 9001							-- Amount of days in which you cannot convert the province back.
NDefines.NReligion.MIN_CARDINALS = 1
NDefines.NReligion.MAX_CARDINALS = 80										-- Max amount of cardinals
NDefines.NReligion.MAX_CARDINALS_PER_COUNTRY = 20							-- Max cardinals in a single country
NDefines.NReligion.MINIMUM_DEVELOPMENT_ALLOWED = 999
NDefines.NReligion.COUNTRY_DEVELOPMENT_DIVIDER = 9999
NDefines.NReligion.YEARLY_PAPAL_INFLUENCE_CATHOLIC = 0
NDefines.NReligion.YEARLY_PAPAL_INFLUENCE_PER_CARDINAL = 0.2
NDefines.NReligion.INVEST_PAPAL_INFLUENCE = 5
NDefines.NReligion.MAX_UNLOCKED_ASPECTS = 7
NDefines.NReligion.ASPECT_ADD_COST = 999
NDefines.NReligion.REFORM_DESIRE_PER_YEAR = 0.001
NDefines.NReligion.AUTHORITY_FROM_DEVELOPMENT = 0.006	
