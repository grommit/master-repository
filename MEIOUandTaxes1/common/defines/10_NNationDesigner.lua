
NDefines.NNationDesigner.HEIR_MAX_AGE = 60
NDefines.NNationDesigner.BASE_TAX_COST_MODIFIER = 1
NDefines.NNationDesigner.PRODUCTION_COST_MODIFIER = 1
NDefines.NNationDesigner.MANPOWER_COST_MODIFIER = 1
NDefines.NNationDesigner.MEMBER_OF_HRE_COST = 30				-- Cost for being part of the HRE (base)
NDefines.NNationDesigner.ALL_MALE_MALE_CHANCE = 95				-- Chance for females if starting ruler and heir were both male.
