

NDefines.NEconomy.GOLD_MINE_SIZE_PRIMITIVES = 20
NDefines.NEconomy.GOLD_MINE_DEPLETION_THRESHOLD = 999			-- Gold mines above production level or above can be depleted
NDefines.NEconomy.GOLD_MINE_DEPLETION_CHANCE = 0				-- Chance of gold mine being depleted (yearly, per production above threshold) 

NDefines.NEconomy.AUTONOMY_AT_DIPLO_ANNEX = 30					-- Autonomy added when diplo-annexing
NDefines.NEconomy.AUTONOMY_AT_CONQUEST = 70						-- Autonomy added at conquest
NDefines.NEconomy.AUTONOMY_AT_CONQUEST_CLAIM = 40				-- Autonomy added at conquest if you have a claim
NDefines.NEconomy.AUTONOMY_AT_CONQUEST_CORE = 10				-- Autonomy added at conquest if you have a core
NDefines.NEconomy.OVERSEAS_MIN_AUTONOMY = 35						-- Overseas provinces always have at least this much autonomy
NDefines.NEconomy.CAPITAL_MAX_AUTONOMY = 100					-- Capital province autonomy can never be above this
NDefines.NEconomy.DECREASE_AUTONOMY_STEP = -10
NDefines.NEconomy.INCREASE_AUTONOMY_STEP = 10
NDefines.NEconomy.INCREASE_AUTONOMY_MAX = 90
NDefines.NEconomy.AUTONOMY_CHANGE_DURATION = 3650				-- about 10 years

NDefines.NEconomy.LAND_TECH_MAINTENANCE_IMPACT = 0.05 				-- % each tech increases it.   #CHANGED FROM 0.05 BY DO OVERHAUL
NDefines.NEconomy.GOLD_INFLATION_THRESHOLD = 0.1				-- _EDEF_GOLD_INFLATION_THRESHOLD_
NDefines.NEconomy.GOLD_INFLATION = 0.60							-- _EDEF_GOLD_INFLATION_
NDefines.NEconomy.TREASURE_FLEET_INFLATION = 0.25
NDefines.NEconomy.INFLATION_FROM_LOAN = 0.50						-- increase per loan
NDefines.NEconomy.INFLATION_FROM_PEACE_GOLD = 0.04				-- inflation per month of income taken in peace (also applied to province sales)
NDefines.NEconomy.INFLATION_ACTION_REDUCTION = 1				-- amount per action
NDefines.NEconomy.WARTAXES_DURATION = 12						-- _EDEF_WARTAXES_DURATION_
NDefines.NEconomy.BASE_INTERESTS = 8.0							-- Base interests
NDefines.NEconomy.LAND_MAINTENANCE_FACTOR = 0.30				-- _EDEF_LAND_MAINTENANCE_FACTOR
NDefines.NEconomy.HEAVY_SHIP_MAINT_FACTOR = 0.08					-- _EDEF_HEAVY_SHIP_MAINT_FACTOR_
NDefines.NEconomy.LIGHT_SHIP_MAINT_FACTOR = 0.08					-- _EDEF_LIGHT_SHIP_MAINT_FACTOR_
NDefines.NEconomy.GALLEY_MAINT_FACTOR = 0.06						-- _EDEF_GALLEY_MAINT_FACTOR_
NDefines.NEconomy.TRANSPORT_MAINT_FACTOR = 0.03					-- _EDEF_TRANSPORT_MAINT_FACTOR_
NDefines.NEconomy.COLONIAL_MAINTENANCE_FACTOR = 3.0			-- _EDEF_COLONIAL_MAINTENANCE_FACTOR_
NDefines.NEconomy.MISSIONARY_MAINTENANCE_FACTOR = 10.0			-- DEI GRATIA _EDEF_MISSIONARY_MAINTENANCE_FACTOR_
NDefines.NEconomy.COASTAL_MODIFIER = 0.7						-- _EDEF_COASTAL_MODIFIER_
NDefines.NEconomy.EMBARGO_BASE_EFFICIENCY = 1.5					-- EMBARGO_BASE_EFFICIENCY
NDefines.NEconomy.TRADE_ADDED_VALUE_MODIFER = 0.075
NDefines.NEconomy.TRADE_PROPAGATE_THRESHOLD = 5
NDefines.NEconomy.TRADE_PROPAGATE_DIVIDER = 15
NDefines.NEconomy.ALLOW_DESTROY_MANUFACTORY = 1					-- Should the player be permitted to destroy manufactories?
NDefines.NEconomy.PIRATES_TRADE_POWER_FACTOR = 2
NDefines.NEconomy.TRADE_COMPANY_STRONG_LIMIT = 0.34
NDefines.NEconomy.LARGE_COLONIAL_NATION_LIMIT = 10
NDefines.NEconomy.PRIVATEER_INCOME_COLLECTION_EFF = 0.5
NDefines.NEconomy.CARAVAN_FACTOR = 25.0							-- Development is divided by this factor, do not set to zero!
NDefines.NEconomy.CARAVAN_POWER_MAX = 150
NDefines.NEconomy.CARAVAN_POWER_MIN = 2
NDefines.NEconomy.MAX_BUILDING_SLOTS = 20						-- vanilla 12, Maximum number of buildings slots, i.e. max buildings possible.
NDefines.NEconomy.ADVISOR_COST_INCREASE_PER_YEAR = 0.01			-- yearly increase in price in percent

NDefines.NEconomy.COLONIST_CHANCE = 0.01

NDefines.NEconomy.DEBASE_MAX_STORED_MONTHS = 0
NDefines.NEconomy.DEBASE_MONTHS_PER_CHARGE = 12
NDefines.NEconomy.DEBASE_MAX_CORRUPTION = 5
NDefines.NEconomy.DEBASE_ADDED_CORRUPTION = 3