cb_plantagenet_french_throne = {
	ai_peace_desire = -50
	
	prerequisites = {
		tag = ENG
		OR = {
			dynasty = "Plantagenet"
			dynasty = "Lancaster"
			dynasty = "York"
		}
		FROM = {
			tag = FRA
			dynasty = "de Valois"
		}
		OR = { singleplayer_trigger = yes ai = yes }
	}
	
	war_goal = take_capital_personal_union
}

cb_de_jure_france = {
	ai_peace_desire = -10

	prerequisites = {
		tag = FRA
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
					OR = {
						region = france_region
						region = provence_region
						region = languedoc_region
						region = high_countries_region
					}
					NOT = {
						AND = {
							province_id = 202 
							owned_by = PAP
						}
					}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_france
}

cb_de_jure_england = {
	ai_peace_desire = -10

	prerequisites = {
		tag = ENG
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = england_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_england
}

cb_de_jure_scotland = {
	ai_peace_desire = -10

	prerequisites = {
		tag = SCO
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = scotland_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_scotland
}

cb_de_jure_ireland = {
	ai_peace_desire = -10

	prerequisites = {
		tag = IRE
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = ireland_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_ireland
}

cb_de_jure_portugal = {
	ai_peace_desire = -10

	prerequisites = {
		tag = POR
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = portugal_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_portugal
}

cb_de_jure_castile = {
	ai_peace_desire = -10

	prerequisites = {
		tag = CAS
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = leon_region
					region = castille_region
					region = andalucia_region
					region = extremadura_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_castile
}

cb_de_jure_aragon = {
	ai_peace_desire = -10

	prerequisites = {
		tag = ARA
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = aragon_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_aragon
}

cb_de_jure_spain = {
	ai_peace_desire = -10

	prerequisites = {
		tag = SPA
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = leon_region
					region = castille_region
					region = andalucia_region
					region = extremadura_region
					region = aragon_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_spain
}

cb_de_jure_navarre = {
	ai_peace_desire = -10

	prerequisites = {
		tag = NAV
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = navarra_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_navarre
}

cb_de_jure_netherland = {
	ai_peace_desire = -10

	prerequisites = {
		tag = NED
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = belgii_region
					region = low_countries_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_netherland
}

cb_conquer_netherland = {
	ai_peace_desire = -10

	prerequisites = {
		NOT = { tag = NED }
		num_of_owned_provinces_with = {
			OR = {
				region = belgii_region
				region = low_countries_region
			}
			
			value = 6
		}
		OR = { singleplayer_trigger = yes ai = yes }
		OR = {
			AND = {
				government_rank = 4
			}
			AND = {
				government_rank = 3
				tag = BUR
			}
		}
		
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = belgii_region
					region = low_countries_region
				}
				NOT = {
					AND = {
						province_id = 93 
						owned_by = LIE
					}
				}
				NOT = {
					AND = {
						province_id = 2451 
						owned_by = LIE
					}
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_conquer_netherland
}

cb_de_jure_germany = {
	ai_peace_desire = -10

	prerequisites = {
		OR = {
			tag = GER
			tag = ERG
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = lower_saxon_circle_region
					region = upper_saxon_circle_region
					region = westphalian_circle_region
					region = upper_rhenish_circle_region
					region = swabian_circle_region
					region = bavarian_circle_region
					region = franconian_circle_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_germany
}

cb_de_jure_italy = {
	ai_peace_desire = -10

	prerequisites = {
		OR = {
			tag = ITA
			tag = ITC
			tag = ITD
			tag = ITE
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = italy_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_italy
}

cb_de_jure_plc = {
	ai_peace_desire = -10

	prerequisites = {
		tag = PLC
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = poland_region
					region = ruthenia_region
					area = samogitia_area
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_plc
}

cb_de_jure_sweden = {
	ai_peace_desire = -10

	prerequisites = {
		tag = SWE
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = swedish_region
					region = finland_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_sweden
}

cb_de_jure_denmark = {
	ai_peace_desire = -10

	prerequisites = {
		tag = DEN
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = danish_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_denmark
}

cb_de_jure_norway = {
	ai_peace_desire = -10

	prerequisites = {
		tag = NOR
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = norwegian_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_norway
}

cb_de_jure_denmark_norway = {
	ai_peace_desire = -10

	prerequisites = {
		tag = DAN
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					region = norwegian_region
					region = danish_region
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_denmark_norway
}

cb_de_jure_poland = {
	ai_peace_desire = -10

	prerequisites = {
		tag = POL
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = poland_region
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_poland
}

cb_de_jure_lithuania = {
	ai_peace_desire = -10

	prerequisites = {
		tag = LIT
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = samogitia_area
					area = lithuania_area
				}
			}
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_lithuania
}


cb_de_jure_bohemia = {
	ai_peace_desire = -10
	prerequisites = {
		OR = {
			tag = BOH
			tag = MRA
			has_country_flag = king_of_bohemia_outside
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = bohemia_area
					area = moravia_area
					area = erzgebirge_area		
				}			
				NOT = { province_id = 2619 }
			}
			
		}
		is_neighbor_of = FROM
	}
	
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_bohemia
}

cb_de_jure_sicily = {
	ai_peace_desire = -10
	prerequisites = {
		OR = {
			tag = SIC
			tag = KNP
			tag = NAP
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				region = sicily_region
			}
			
		}
		is_neighbor_of = FROM
	}
	
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_sicily
}

# Dishonored an alliance
cb_foreign_interference = {
	valid_for_subject = yes

	is_triggered_only = yes
	months = 60
	
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_foreign_interference	
}
