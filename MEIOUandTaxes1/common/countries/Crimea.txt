# Crimea.txt
# Tag : CRI
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups

graphical_culture = muslimgfx

color = { 59  158  125 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = { #Russian group
	eastern_hospite_infantry
	eastern_druzhina_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_hand_gunner
	eastern_hussar_horse_archer_cavalry
	eastern_pischalniki_infantry
	eastern_cossack_cavalry
	eastern_streltsy_infantry
	eastern_light_cossack_cavalry
	eastern_soldaty_infantry
	eastern_pancerni_cavalry
	eastern_regular_infantry
	eastern_carabinier_cavalry
	eastern_line_infantry
	eastern_uhlan_cavalry
	eastern_mass_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_jominian_infantry
}

monarch_names = {
	"Hajji #0" = 20
	"Haidar #0" = 10
	"N�r Daulat #0" = 10
	"Mangl� #0" = 20
	"Muhammad #0" = 40
	"Ghazi #0" = 30
	"Sa'adet #0" = 30
	"Islam #0" = 30
	"Sahib #0" = 20
	"Devlet #0" = 30
	"Qaplan #0" = 20
	"Feth #0" = 20
	"Toqtamish #0" = 10
	"Selamet #0" = 20
	"Janbeg #0" = 10
	"Inayet #0" = 10
	"Bajadur #0" = 20
	"Adil #0" = 10
	"Selim #0" = 30
	"Murad #0" = 10
	"Safa #0" = 10
	"Sal�m #0" = 10
	"J�ni Beg #0" = 20
	"Qara Devlet #0" = 10
	"Arslan #0" = 10
	"Halim #0" = 10
	"Qirim #0" = 10
	"Shahin #0" = 10

	"Aybanu #0" = -1
	"Alsou #0" = -1
	"Arslanbika #0" = -1
	"Alfia #0" = -1
	"Aida #0" = -1
	"Aziz #0" = -1
	"Agdal #0" = -1
	"Bakir #0" = -1
	"Vazih #0" = -1
	"Walia #0" = -1
	"Gaysha #0" = -1
	"Gulnara #0" = -1
	"Zia #0" = -1
	"Zifa #0" = -1
	"Zakaria #0" = -1
	"Zamira #0" = -1
	"Zainulla #0" = -1
	"Irada #0" = -1
	"Islamia #0" = -1
	"Katib #0" = -1
	"Kirama #0" = -1
	"Lyabiba #0" = -1
	"Lutfulla #0" = -1
	"Maysara #0" = -1
	"Nailya #0" = -1
	"Nasima #0" = -1
	"Rushaniya #0" = -1
	"Sadiq #0" = -1
	"Salih #0" = -1
	"Fazil #0" = -1
	"Farida #0" = -1
	"Hayat #0" = -1
	"Habibullah #0" = -1
	"Shamil #0" = -1
	"Elvira #0" = -1
	"Elmira #0" = -1
	"Yuldus #0" = -1
}

leader_names = {
	Altan Asanin Bekmambet
	Ziyaeddin Sarig�l
	Bekt�re Celebicihan
	Cobanzade D�gci Gaspirali
	Giraybay
	Hablemitoglu Hilmi Inalcik
	Ipci Kirimal Seydahmet Ihsan Kubay
	Qirimoglu Mamut Mansiz
	Mehdi Neyazi Ortayli Semizade
	Soysal Zihni Tarhan Fazil
	Uralgiray Kosay Agah Mangitli
	T�gay Kemal
}

ship_names = {
	Bakhchisaray "��r�k Suv" "Chufut Kale"
	Hansaray "Haci Giray" "Meli I Giray"
	"Zincilri Medrese" Kyrym  "Qirq Yer"
	"Cufut Qale" "Chingiz Khan"
}
