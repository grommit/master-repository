#Riga

graphical_culture = easterngfx

color = { 183  176  194 }

historical_idea_groups = {
	trade_ideas
	administrative_ideas
	naval_ideas
	plutocracy_ideas
	economic_ideas
	mercenary_ideas
	merchant_marine_ideas
	innovativeness_ideas
	logistic_ideas
	diplomatic_ideas
}

historical_units = { #Baltic group
	eastern_hospite_infantry
	eastern_druzhina_cavalry
	eastern_bandiera_infantry
	eastern_balkan_feudal_cavalry
	eastern_hand_gunner
	eastern_hussar_horse_archer_cavalry
	eastern_draby_infantry
	eastern_medium_hussar_cavalry
	eastern_marauder_infantry
	eastern_light_hussar
	eastern_slavic_western_infantry
	eastern_reiter_cavalry
	eastern_regular_infantry
	eastern_armeblanche_cavalry
	eastern_bayonet_infantry
	eastern_uhlan_cavalry
	eastern_drill_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}

monarch_names = {
	"Johann #4" = 60
	"Henning #0" = 20
	"Kaspar #0" = 20
	"Michael #0" = 20
	"Silvester #0" = 20
	"Stefan #0" = 20
	"Thomas #0" = 20
	"Wilhelm #0" = 20
	"Fromhold #1" = 15
	"Henning #1" = 15
	"Siegfried #1" = 15
	"Albrecht #2" = 10
	"Engelbert #1" = 10
	"Friedrich #1" = 10
	"Nikolaus #1" = 10
	"Adalbert #0" = 1
	"Adam #0" = 1
	"Arnold #0" = 1
	"Bernhard #0" = 1
	"Bonifaz #0" = 1
	"Cornelius #0" = 1
	"Dietrich #0" = 1
	"Egon #0" = 1
	"Ferdinand #0" = 1
	"Gregor #0" = 1
	"G�nther #0" = 1
	"Humbert #0" = 1
	"Isidor #0" = 1
	"Joseph #0" = 1
	"Konstantin #0" = 1
	"Lothar #0" = 1
	"Marcel #0" = 1
	"Markus #0" = 1
	"Matthias #0" = 1
	"Oskar #0" = 1
	"Pankraz #0" = 1
	"Rudolf #0" = 1
	"Siegbert #0" = 1
	"Theodor #0" = 1
	"Xaver #0" = 1
	
	"Aleida #0" = -1
	"Cordula #0" = -1
	"Gundula #0" = -1
	"Selma #0" = -1
	"Ulrike #0" = -1
	"Barbara #0" = -1
	"Alexandra #0" = -1
	"Christiane #0" = -1
	"Emma #0" = -1
	"Corinna #0" = -1
	"Else #0" = -1
	"Hedwig #0" = -1
	"Sylvia #0" = -1
	"Wilhelmine #0" = -1
	"Alexis #0" = -1
	"Evelyn #0" = -1
	"Hannelore #0" = -1
	"Irma #0" = -1
	"Heinrike #0" = -1
	"Ilona #0" = -1
	"Andrea #0" = -1
}

leader_names = { 
	Stayn Finkelstein
	Kanter Neiman Saper
	Levin Neiman
	Elkins Herder Balderis 
	Eisenstein Strok Tal 
	Zander "von Nauen" 
	Suerbeer "von Lune" 
	"von Vechten" "von Schwerin"
	Takkon "von Pernstein"
	"von Dolen" Blomberg
	"Von Sinten" "von Wallenrodt"
	Ambundi Scharpenberg
}

ship_names = {
	"Stolz von Hamburg" "Stolz der Hanse" "Stolz von Wismar"
	"Stolz von Rostock" "Stolz von Stralsund" "Stolz von Ahlbeck"
	"Stolz von Flensburg" "Stolz von Bremen" "Stolz von Visby"
	"Stolz von Stockholm" "Stolz von Nowgorod" "Stolz von Pernau"
	"Stolz von Windau" "Stolz von Stettin" "Stolz von Danzig"
	"Stolz von Riga" "Stolz von Br�gge" "Stolz von Antwerpen"
	"Stolz von Buxtehude" "Stolz von Groningen" "Stolz von Stade"
	"Stolz von Anklam" "Stolz von Demmin" "Stolz von Greifswald"
	"Stolz von Kiel" "Stolz von Wolgast" "Stolz von Belgard"
	"Stolz von Dramburg" "Stolz von Gollnow" "Stolz von Greifenberg"
	"Stolz von Kammin" "Stolz von Kolberg" "Stolz von K�slin"
	"Stolz von Schlawe" "Stolz von Stargard" "Stolz von Stolp"
	"Stolz von Treptow" "Stolz von Wollin" "Stolz von Braunsberg"
	"Stolz von Breslau" "Stolz von Elbing" "Stolz von K�nigsberg"
	"Stolz von Kulm" "Stolz von Thorn" "Stolz von Krakau"
	"Stolz von Dorpat" "Stolz von Fellin" "Stolz von Goldingen"
	"Stolz von Gross Roop" "Stolz von Kokenhusen" "Stolz von Lemsal"
	"Stolz von Reval" "Stolz von Wenden" "Stolz von Wolmar"
	"Stolz von K�ln" "Stolz von Dinant" "Stolz von Dinslaken"
	"Stolz von Duisburg" "Stolz von Emmerich am Rhein" "Stolz von Grieth"
	"Stolz von Neuss" "Stolz von Nimwegen" "Stolz von Roermond"
	"Stolz von Tiel" "Stolz von Venlo" "Stolz von Wesel"
	"Stolz von Zaltbommel" "Stolz von Arnhem" "Stolz von Deventer"
	"Stolz von Doesburg" "Stolz von Elburg" "Stolz von Harderwijk"
	"Stolz von Hasselt" "Stolz von Hattem" "Stolz von Kampen"
	"Stolz von Oldenzaal" "Stolz von Ommen" "Stolz von Stavoren"
	"Stolz von Zutphen" "Stolz von Zwolle" "Stolz von Dortmund"
	"Stolz von M�nster" "Stolz von Osnabr�ck" "Stolz von Soest"
	"Stolz von Ahlen" "Stolz von Allendorf" "Stolz von Altena"
	"Stolz von Arndsberg" "Stolz von Attendorn" "Stolz von Balve"
	"Stolz von Beckum" "Stolz von Belecke" "Stolz von Bielefeld"
	"Stolz von Billerbeck" "Stolz von Blankenstein" "Stolz von Bocholt"
	"Stolz von Bochum" "Stolz von B�defeld" "Stolz von Borgentreich"
	"Stolz von Borken" "Stolz von Brakel" "Stolz von Breckerfeld"
	"Stolz von Brilon" "Stolz von Coesfeld" "Stolz von Dorsten"
	"Stolz von Drolshagen" "Stolz von D�lmen" "Stolz von Essen"
	"Stolz von Eversberg" "Stolz von Freienohl" "Stolz von F�rstenau"
	"Stolz von Geseke" "Stolz von Grevenstein" "Stolz von Hachen"
	"Stolz von Hagen" "Stolz von Haltern am See" "Stolz von Hamm"
	"Stolz von Hasel�ne" "Stolz von Hattingen" "Stolz von Herford"
	"Stolz von H�xter" "Stolz von Hirschberg" "Stolz von H�rde"
	"Stolz von H�sten" "Stolz von Iburg" "Stolz von Iserlohn"
	"Stolz von Kallenhardt" "Stolz von Kamen" "Stolz von Korbach"
	"Stolz von Langscheid" "Stolz von Lemgo" "Stolz von Lennep"
	"Stolz von Lippstadt" "Stolz von L�denscheid" "Stolz von L�nen"
	"Stolz von Medebach" "Stolz von Melle" "Stolz von Menden"
	"Stolz von Meppen" "Stolz von Minden" "Stolz von Neuenrade"
	"Stolz von Nieheim" "Stolz von Olpe" "Stolz von Paderborn"
	"Stolz von Peckelsheim" "Stolz von Plettenberg" "Stolz von Quakenbr�ck"
	"Stolz von Ratingen" "Stolz von Recklinghausen" "Stolz von Rheine"
	"Stolz von Rinteln" "Stolz von R�then" "Stolz von Schmallenberg"
	"Stolz von Sch�ttorf" "Stolz von Schwerte" "Stolz von Solingen"
	"Stolz von Sundern" "Stolz von Telgte" "Stolz von Unna"
	"Stolz von V�rden" "Stolz von Vreden" "Stolz von Warburg"
	"Stolz von Warendorf" "Stolz von Warstein" "Stolz von Wattenscheid"
	"Stolz von Werl" "Stolz von Werne" "Stolz von Westhofen"
	"Stolz von Wetter" "Stolz von Wiedenbr�ck" "Stolz von Wipperf�rth"
	"Stolz von Berlin-C�lln" "Stolz von Brandenburg" "Stolz von Frankfurt/Oder"
	"Stolz von Havelberg" "Stolz von Kyritz" "Stolz von Perleberg"
	"Stolz von Pritzwalk" "Stolz von Duderstadt" "Stolz von Erfurt"
	"Stolz von G�ttingen" "Stolz von Halle" "Stolz von Merseburg"
	"Stolz von M�hlhausen" "Stolz von Naumburg/Saale" "Stolz von Nordhausen"
	"Stolz von Northeim" "Stolz von Osterode am Harz" "Stolz von Uslar"
	"Stolz von Braunschweig" "Stolz von Magdeburg" "Stolz von Aschersleben"
	"Stolz von Bockenem" "Stolz von Einbeck" "Stolz von Gardelegen"
	"Stolz von Goslar" "Stolz von Gronau"
	"Stolz von Halberstadt" "Stolz von Hameln" "Stolz von Hannover"
	"Stolz von Helmstedt" "Stolz von Hildesheim" "Stolz von L�neburg"
	"Stolz von Osterburg" "Stolz von Quedlinburg" "Stolz von Salzwedel"
	"Stolz von Seehausen" "Stolz von Stendal" "Stolz von Tangerm�nde"
	"Stolz von Uelzen" "Stolz von Werben" "Stolz von Bergen"
	"Stolz von London" "Stolz von Bordeaux" "Stolz von Boston"
	"Stolz von Bourgneuf-en-Retz" "Stolz von Bristol" "Stolz von Helsingborg"
	"Stolz von Hull" "Stolz von Ipswich" "Stolz von Kalmar"
	"Stolz von Kaunas" "Stolz von King�s Lynn" "Stolz von La Rochelle"
	"Stolz von Lissabon" "Stolz von L�d�se" "Stolz von Malm�"
	"Stolz von Nantes" "Stolz von Narwa" "Stolz von Norwich"
	"Stolz von Nyk�ping" "Stolz von Oslo" "Stolz von Pleskau"
	"Stolz von Polozk" "Stolz von Porto" "Stolz von Ribe"
	"Stolz von Smolensk" "Stolz von T�nsberg" "Stolz von Turku"
	"Stolz von Venedig" "Stolz von Wilna" "Stolz von Wizebsk"
	"Stolz von Yarmouth" "Stolz von York" "Stolz von Falsterbo"
	"Stolz von Newcastle" "Stolz von Pasewalk" "Stolz von Leith"
	"Stolz von Wesenberg" "Stolz von Wei�enstein" "Stolz von Walk"
	"Stolz von Twer" "Stolz von Trondheim" "Stolz von Scalloway"
	"Stolz von Lippe" "Stolz von Neapel" "Stolz von Hindeloopen"
}

army_names = {
	"Armee von $PROVINCE$" 
}
