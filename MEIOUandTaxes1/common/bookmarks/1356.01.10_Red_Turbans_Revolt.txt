bookmark =
{
	name = "RED_TURBAN_NAME"
	desc = "RED_TURBAN_DESC"
	date = 1356.12.25

	country = YUA
	country = MNG
	country = SNG
	country = DAA
	country = TIA
	country = XIA
	country = ZOU
	
	easy_country = MNG
	
	effect = {
		HAB = {
			set_global_flag = 1356_start
		}
	}
}