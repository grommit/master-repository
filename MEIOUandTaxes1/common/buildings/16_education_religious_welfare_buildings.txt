### DO OVERHAUL MODIFIED ###

#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# Education Buildings
################################################
#Medieval University
#Early Modern University
#Modern University

#Schools

################################################
# Religious Buildings
################################################
#Temple
#Great Temple

################################################
# Welfare Buildings
################################################
#Public Baths
#Hospital
#Water Works

#City Watch

################################################
# Tier 1, 15th Century Buildings
################################################
city_watch = {		
	cost = 100
	time = 12
	
	modifier = {
		local_defensiveness = 0.10
		garrison_growth = 0.20
		# local_spy_defence = 0.10
		local_development_cost = -0.025
		local_autonomy = -0.02
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			NOT = { development = 6 }
		}		
		modifier = {
			factor = 0.5
			NOT = { development = 9 }
		}
		modifier = {
			factor = 1.5
			development = 15
		}	
		modifier = {
			factor = 1.5
			development = 20
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					has_building = fort_14th
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

public_baths = {		
	cost = 100
	time = 12

	trigger = {
		NOT = {  has_building = water_works }
		NOT = {  has_building = hospital }
	}
	
	modifier = {
		local_unrest = -0.5
		supply_limit = 0.5
		garrison_growth = 0.1
		local_development_cost = -0.05
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}	
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

temple = {		
	cost = 100
	time = 12
	
	trigger = {
		NOT = {  has_building = great_temple }
	}
	
	modifier = {
		local_missionary_strength = 0.005
		local_unrest = -1
		tax_income = 1
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { piety = -0.60 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { piety = -0.20 } }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}	
	}
}

medieval_university = {		
	cost = 1000
	time = 24
	
	trigger = {
		NOT = {  has_building = early_modern_university }
		NOT = {  has_building = modern_university }
	}
	
	modifier = {
		local_development_cost = -0.1
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}		
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

################################################
# Tier 3, 17th Century Buildings
################################################

early_modern_university = {		
	cost = 2000
	time = 24
	
	trigger = {
		NOT = {  has_building = modern_university }
	}
	
	make_obsolete = medieval_university
	
	modifier = {
		local_development_cost = -0.15
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}			
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

great_temple = {
		
	cost = 300
	time = 12
	
	make_obsolete = temple
		

	modifier = {
		tax_income = 3
		local_missionary_strength = 0.01
		local_unrest = -2
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { piety = -0.60 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { piety = -0.20 } }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}	
	}
}

water_works = {		
	cost = 300
	time = 24
	
	trigger = {
		NOT = {  has_building = hospital }
	}
			
	make_obsolete = public_baths
	
	modifier = {
		local_unrest = -1
		supply_limit = 1
		garrison_growth = 0.2
		local_development_cost = -0.1
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

################################################
# Tier 4, 18th Century Buildings
################################################
schools = {		
	cost = 400
	time = 12
	
	modifier = {
		local_development_cost = -0.1
		local_autonomy = -0.05
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

################################################
# Tier 5, 19th Century Buildings
################################################
modern_university = {		
	cost = 3000
	time = 24
	
	make_obsolete = early_modern_university
	
	modifier = {
		local_development_cost = -0.2
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}

hospital = {		
	cost = 500
	time = 24
		
	make_obsolete = water_works
	
	modifier = {
		local_unrest = -1.5
		supply_limit = 1.5
		garrison_growth = 0.3
		local_development_cost = -0.15
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}				
		modifier = {
			factor = 0.1
			is_overseas = yes
		}		
	}
}
