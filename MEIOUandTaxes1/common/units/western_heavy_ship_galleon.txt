#23 - Early Galleon (San Salvador, 1542)
type = heavy_ship

hull_size = 49 #200 tons
base_cannons = 48 #Based on early Ark Royal
sail_speed = 8 #8 knots

sailors = 270

sprite_level = 3

trigger = { 
	OR = { 
		technology_group = western
		has_idea_group = grand_fleet_ideas
		AND = { 
			technology_group = eastern
			has_idea_group = naval_ideas
			has_country_modifier = western_arms_trade
			}
		}
	NOT = { primary_culture = english }
	NOT = { technology_group = indian } # DLC use separate indian galley
	NOT = { technology_group = chinese } # DLC use separate indian galley
	NOT = { technology_group = austranesian } # DLC use separate indian galley
	NOT = { technology_group = hawaii_tech } # DLC use separate indian galley
	NOT = { technology_group = mongol_tech } # DLC use separate indian galley
	NOT = { technology_group = south_american } # DLC use separate indian galley
	NOT = { technology_group = mesoamerican } # DLC use separate indian galley
	NOT = { technology_group = north_american } # DLC use separate indian galley
	NOT = { technology_group = muslim } # DLC use separate muslim galley
	NOT = { technology_group = turkishtech } # DLC use separate muslim galley
	NOT = { technology_group = nomad_group } # DLC use separate muslim galley
	NOT = { technology_group = steppestech } # DLC use separate muslim galley
	NOT = { technology_group = soudantech } # DLC use separate muslim galley
	NOT = { technology_group = sub_saharan } # DLC use separate muslim galley
}	
