#36 - Two-Decker Ship of the Line (La Couronne, 1636)
type = heavy_ship

hull_size = 76 #Displaced 2460 tons (/30)
base_cannons = 73 #Later 72
sail_speed = 12 #12 knots maximum

sailors = 480

sprite_level = 5

trigger = { 
	OR = { 
		technology_group = western
		has_idea_group = grand_fleet_ideas
		AND = { 
			has_idea_group = naval_ideas
			has_country_modifier = western_arms_trade
			}
		}
	}
	