pashtun_tribal_area = {
	local_defensiveness = 0.250
	local_manpower_modifier = 0.2
	local_tax_modifier = -0.15	
	local_autonomy = 0.050
	local_unrest = 1
	local_hostile_attrition = 1	
	garrison_growth = 0.10	

	picture = "coa_pashtun_tribal_area"
}

arab_tribal_area = {
	local_defensiveness = 0.250
	local_manpower_modifier = 0.10
	local_production_efficiency = 0.25
	local_autonomy = 0.050
	local_unrest = 1
	local_hostile_attrition = 1	
	garrison_growth = 0.10	

	picture = "coa_arab_tribal_area"
}

qizilbash_tribal_area = {
	local_defensiveness = 0.250
	local_manpower_modifier = 0.10
	local_production_efficiency = 0.25
	local_autonomy = 0.050
	local_unrest = 1
	local_hostile_attrition = 1	
	garrison_growth = 0.10	

	picture = "coa_arab_tribal_area"
}

arab_cavalry_1 = {
	cavalry_cost = -0.02
	cavalry_power = 0.01
}

arab_cavalry_2 = {
	cavalry_cost = -0.03
	cavalry_power = 0.02
}

arab_cavalry_3 = {
	cavalry_cost = -0.04
	cavalry_power = 0.03
}

pashtun_infantry_1 = {
	infantry_cost = -0.02
	infantry_power = 0.01
}

pashtun_infantry_2 = {
	infantry_cost = -0.03
	infantry_power = 0.02
}

pashtun_infantry_3 = {
	infantry_cost = -0.04
	infantry_power = 0.03
}

trading_post_province = {
	picture = "logcabin_good"
}
