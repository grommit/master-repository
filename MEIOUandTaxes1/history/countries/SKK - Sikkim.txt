# SIK - Sikkim

government = amalgamation_government government_rank = 1
#government = indian_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = sikkimese
religion = vajrayana
technology_group = indian
capital = 562

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1642.1.1 = {
	government = indian_monarchy government_rank = 1
	monarch = {
		name = "Phuntsog"
		dynasty = "Namgyal"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1642.1.1 = {
	heir = {
		name = "Tensung"
		monarch_name = "Tensung"
		dynasty = "Namgyal"
		birth_Date = 1632.1.1
		death_date = 1700.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1670.1.1 = {
	monarch = {
		name = "Tensung"
		dynasty = "Namgyal"
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1670.1.1 = {
	heir = {
		name = "Chakdor"
		monarch_name = "Chakdor"
		dynasty = "Namgyal"
		birth_Date = 1660.1.1
		death_date = 1717.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1700.1.1 = {
	monarch = {
		name = "Chakdor"
		dynasty = "Namgyal"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1700.1.1 = {
	heir = {
		name = "Gyurmed"
		monarch_name = "Gyurmed"
		dynasty = "Namgyal"
		birth_Date = 1690.1.1
		death_date = 1733.1.1
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1717.1.1 = {
	monarch = {
		name = "Gyurmed"
		dynasty = "Namgyal"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1717.1.1 = {
	heir = {
		name = "Phuntsog"
		monarch_name = "Phuntsog II"
		dynasty = "Namgyal"
		birth_Date = 1700.1.1
		death_date = 1780.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1733.1.1 = {
	monarch = {
		name = "Phuntsog II"
		dynasty = "Namgyal"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1733.1.1 = {
	heir = {
		name = "Tenzing"
		monarch_name = "Tenzing"
		dynasty = "Namgyal"
		birth_Date = 1720.1.1
		death_date = 1793.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1780.1.1 = {
	monarch = {
		name = "Tenzing"
		dynasty = "Namgyal"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1785.1.1 = {
	heir = {
		name = "Tsugphud"
		monarch_name = "Tsugphud"
		dynasty = "Namgyal"
		birth_Date = 1785.1.1
		death_date = 1863.1.1
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 1
	}
}

1793.1.1 = {
	monarch = {
		name = "Tsugphud"
		dynasty = "Namgyal"
		ADM = 2
		DIP = 4
		MIL = 1
	}
}

#Annexed by Great Britain 1861
