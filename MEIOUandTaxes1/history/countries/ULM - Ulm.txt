#MEIOU-GG Governemnt changes

government = imperial_city
mercantilism = 5
primary_culture = bavarian
religion = catholic
technology_group = western
capital = 80
fixed_capital = 80	# Ulm

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1356.1.1 = {
	monarch = {
		name = "Stadtrat"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}

1530.1.1 = {
	religion = protestant
}

1608.5.14 = {
	join_league = protestant
}

1648.10.24 = {
	leave_league = protestant
}
