# Country: Bhutan
# file name: BHU - Bhutan
# MEIOU-FB India 1337+ mod Aug 08

government = amalgamation_government government_rank = 1
#government = indian_monarchy government_rank = 4
mercantilism = 0.0
technology_group = indian
religion = vajrayana #DEI GRATIA
primary_culture = bhutanese
capital = 565	# Bhutan

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

#Independent from Tibet 1616
1616.1.1 = {
	government = indian_monarchy government_rank = 4
	monarch = {
		name = "Ngawang Namgyal"
		dynasty = "Wangchuck"
		ADM = 6
		DIP = 6
		MIL = 5
	}
}

#Dharma Rajas 
1651.3.1 = {
	monarch = {
		name = "Pekar Jungney"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1698.1.1 = {
	monarch = {
		name = "Kunga Gyaltshen"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1713.2.1 = {
	monarch = {
		name = "Phyogla Namgyal"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1730.1.1 = {
	monarch = {
		name = "Jigme Norbu"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1735.1.1 = {
	monarch = {
		name = "Mipham Wangpo"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1738.1.1 = {
	monarch = {
		name = "Jigme Dragpa"
		dynasty = "Wangchuck"
		ADM = 2
		DIP = 3
		MIL = 5
	}
}

1761.1.1 = {
	monarch = {
		name = "Choeki Gyaltshen"
		dynasty = "Wangchuck"
		ADM = 4
		DIP = 5
		MIL = 3
	}
}

1791.1.1 = {
	monarch = {
		name = "Jigme Dragpa II"
		dynasty = "Wangchuck"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}
