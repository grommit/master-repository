#MEIOU-GG Governemnt changes

government = feudal_monarchy government_rank = 3
# centralization_decentralization = -5
mercantilism = 0.0
primary_culture = occitain
add_accepted_culture = gascon
add_accepted_culture = limousin
add_accepted_culture = auvergnat
religion = catholic
technology_group = western
capital = 196	# Toulouse

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -5 }
}

1515.1.1 = {
	government = despotic_monarchy government_rank = 3
}

1560.1.1 = {
	religion = reformed
	}

1589.8.3 = {
	government = administrative_monarchy government_rank = 3
}

1661.3.9 = {
	government = absolute_monarchy government_rank = 3
}
