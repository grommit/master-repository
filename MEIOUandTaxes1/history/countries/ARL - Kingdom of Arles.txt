# ARL - Kingdom of Arles

government = feudal_monarchy
government_rank = 5
mercantilism = 0.1 # mercantilism_freetrade = 2
primary_culture = provencal
add_accepted_culture = arpitan
religion = catholic
technology_group = western
capital = 201	# Arles

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1506.9.26 = { government = despotic_monarchy }

1589.8.3 = { government = administrative_monarchy }

1661.3.9 = { government = absolute_monarchy }
