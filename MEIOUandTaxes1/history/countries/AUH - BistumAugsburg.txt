# AUH - Bistum Augsburg

government = theocratic_government government_rank = 3
mercantilism = 0.0
primary_culture = schwabisch
religion = catholic
technology_group = western
capital = 2614

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}
1348.1.1 = {
	monarch = {
		name = "Marquard I"
		dynasty = "von Randeck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1365.1.1 = {
	monarch = {
		name = "Walter II"
		dynasty = "von Hochschlitz"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1369.1.1 = {
	monarch = {
		name = "Johann I"
		dynasty = "Schadland"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1372.1.1 = {
	monarch = {
		name = "Burkhard"
		dynasty = "von Ellerbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1404.1.1 = {
	monarch = {
		name = "Eberhard II"
		dynasty = "von Kirchberg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1413.1.1 = {
	monarch = {
		name = "Friedrich"
		dynasty = "von Grafeneck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1414.1.1 = {
	monarch = {
		name = "Anselm"
		dynasty = "von Nenningen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1423.1.1 = {
	monarch = {
		name = "Peter"
		dynasty = "von Schaumberg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1469.1.1 = {
	monarch = {
		name = "Johann II"
		dynasty = "von Werdenberg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1486.1.1 = {
	monarch = {
		name = "Friedrich II"
		dynasty = "von Zollern"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1505.1.1 = {
	monarch = {
		name = "Heinrich IV"
		dynasty = "von Lichtenau"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1517.1.1 = {
	monarch = {
		name = "Christoph"
		dynasty = "von Stadion"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1543.1.1 = {
	monarch = {
		name = "Otto Truchsess"
		dynasty = "von Waldburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1573.1.1 = {
	monarch = {
		name = "Johann Eglof"
		dynasty = "von Knöringen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1575.1.1 = {
	monarch = {
		name = "Marquard II"
		dynasty = "vom Berg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1591.1.1 = {
	monarch = {
		name = "Johann Otto"
		dynasty = "von Gemmingen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1598.1.1 = {
	monarch = {
		name = "Heinrich V"
		dynasty = "von Knöringen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1609.7.1 = {
	join_league = catholic
}

1646.1.1 = {
	monarch = {
		name = "Sigismund"
		dynasty = "Franz"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1648.10.24 = {
	leave_league = catholic
}

1665.1.1 = {
	monarch = {
		name = "Johann Christoph"
		dynasty = "von Freyberg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1690.1.1 = {
	monarch = {
		name = "Alexander Sigmund"
		dynasty = "von Pfalz-Neuburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1737.1.1 = {
	monarch = {
		name = "Johann Franz Schenk"
		dynasty = "von Stauffenberg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1740.1.1 = {
	monarch = {
		name = "Joseph Ignaz Philipp"
		dynasty = "von Hessen-Darmstadt"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1768.1.1 = {
	monarch = {
		name = "Clemens Wenceslaus"
		dynasty = "von Sachsen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1812.1.1 = {
	monarch = {
		name = "Franz Friedrich"
		dynasty = "von Sturmfeder"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

#1818
