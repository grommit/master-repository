# HUS - Hussites

government = republican_dictatorship
mercantilism = 0.0
primary_culture = czech
religion = hussite
capital = 266	#Prag
technology_group = western

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1419.1.1 = {
	monarch = {
		name = "Jan Zizka"
		ADM = 5
		DIP = 2
		MIL = 6
		leader = {
			name = "Jan Zizka"
			type = general
			rank = 0
			fire = 6
			shock = 3
			manuever = 5
			siege = 2
		}
	}
}

1421.1.1 = {
	leader = {
		name = "Prokop Hol�"
		type = general
		rank = 1
		fire = 5
		shock = 2
		manuever = 5
		siege = 1
	}
}

1424.10.11 = {
	monarch = {
		name = "Prokop Hol�"
		ADM = 5
		DIP = 3
		MIL = 6
	}
}

1434.5.30 = {
	monarch = {
		name = "Utrakvist�"
		ADM = 4
		DIP = 3
		MIL = 5
	}
			}

1457.11.24 = {
	monarch = {
		name = "Jiri z Podebrad"
		ADM = 6
		DIP = 3
		MIL = 4
	}
}