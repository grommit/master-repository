# MHF - Mahafaly

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = vezu
religion = animism
technology_group = malagasy_tech
capital = 3895

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1350.1.1 = {
	monarch = {
		name = "Tribal Elders"
		dynasty = "Mahafaly"
		ADM = 3
		DIP = 3
		MIL = 3
		regent = yes
	}
}
