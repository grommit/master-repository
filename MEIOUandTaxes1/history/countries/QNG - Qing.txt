# QNG - Qing

government = chinese_monarchy_3 government_rank = 6
mercantilism = 0.0
primary_culture = manchu
religion = confucianism
technology_group = chinese
capital = 708	# Beijing

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = -1 }
}

1626.9.30 = {
	monarch = {
		name = "Chongde"
		dynasty = "Aisin Gioro"
		ADM = 5
		DIP = 4
		MIL = 6
		leader = { name = "Chongde"   	type = general		rank = 0	fire = 0	shock = 5	manuever = 3	siege = 0 }
	}
}

1628.1.1 = { leader = {	name = "Dorgon"			type = general		rank = 1	fire = 4	shock = 4	manuever = 5	siege = 2	death_date = 1650.12.31 } }

1638.3.15 = {
	heir = {
		name = "Shunzhi"
		monarch_name = "Shunzhi"
		dynasty = "Aisin Gioro"
		birth_date = 1638.3.15
		death_date = 1661.2.5
		claim = 95
		ADM = 4
		DIP = 5
		MIL = 5
	}
}

1643.9.22 = {
	monarch = {
		name = "Shunzhi"
		dynasty = "Aisin Gioro"
		ADM = 4
		DIP = 5
		MIL = 5
	}
}

1654.5.4 = {
	heir = {
		name = "Kangxi"
		monarch_name = "Kangxi"
		dynasty = "Aisin Gioro"
		birth_date = 1654.5.4
		death_date = 1722.12.20
		claim = 95
		ADM = 6
		DIP = 6
		MIL = 6
	}
}

1661.2.5 = {
	monarch = {
		name = "Kangxi"
		dynasty = "Aisin Gioro"
		ADM = 6
		DIP = 6
		MIL = 6
	}
}

1678.12.13 = {
	heir = {
		name = "Yongzheng"
		monarch_name = "Yongzheng"
		dynasty = "Aisin Gioro"
		birth_date = 1678.12.13
		death_date = 1735.10.8
		claim = 95
		ADM = 6
		DIP = 6
		MIL = 5
	}
}

1683.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 0 }
} # Ming is conquered

1689.1.1 = {
	} # The treaty of Nerchiinsk

1722.12.21 = {
		set_variable = { which = "centralization_decentralization" value = 1 }
} # The expulsion of the Jesuits

1722.12.21 = {
	monarch = {
		name = "Yongzheng"
		dynasty = "Aisin Gioro"
		ADM = 6
		DIP = 6
		MIL = 5
	}
}

1722.12.21 = {
	heir = {
		name = "Qianlong"
		monarch_name = "Qianlong"
		dynasty = "Aisin Gioro"
		birth_date = 1711.9.25
		death_date = 1799.2.7
		claim = 95
		ADM = 6
		DIP = 5
		MIL = 5
	}
}

1735.10.10 = {
	monarch = {
		name = "Qianlong"
		dynasty = "Aisin Gioro"
		ADM = 6
		DIP = 5
		MIL = 5
	}
}

1736.1.1 = { leader = { name = "Lung"	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0	death_date = 1761.1.1 } }

1760.11.13 = {
	heir = {
		name = "Jiaqing"
		monarch_name = "Jiaqing"
		dynasty = "Aisin Gioro"
		birth_date = 1760.11.13
		death_date = 1820.9.2
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1770.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 0 }
	# innovative_narrowminded = 4
} # The appointment of Heshen

1796.2.9 = {
	monarch = {
		name = "Jiaqing"
		dynasty = "Aisin Gioro"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1796.2.9 = {
	heir = {
		name = "Daoguang"
		monarch_name = "Daoguang"
		dynasty = "Aisin Gioro"
		birth_date = 1782.9.16
		death_date = 1850.2.25
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1820.9.3 = {
	monarch = {
		name = "Daoguang"
		dynasty = "Aisin Gioro"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1831.7.17 = {
	heir = {
		name = "Xianfeng"
		monarch_name = "Xianfeng"
		dynasty = "Aisin Gioro"
		birth_date = 1831.7.17
		death_date = 1861.8.22
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1850.3.9 = {
	monarch = {
		name = "Xianfeng"
		dynasty = "Aisin Gioro"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
