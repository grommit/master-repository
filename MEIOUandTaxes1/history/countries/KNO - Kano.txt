# KNO - Kano
# 2010-jan-16 - FB - HT3 changes

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = haussa
religion = sunni
technology_group = sub_saharan

capital = 1554	# Kano

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1349.1.1 = {
	monarch = {
		name = "Yaji"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1385.1.1 = {
	monarch = {
		name = "Bugaya"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1390.1.1 = {
	monarch = {
		name = "Kanajeji"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1410.1.1 = {
	monarch = {
		name = "Umaru"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1421.1.1 = {
	monarch = {
		name = "Daud"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1438.1.1 = {
	monarch = {
		name = "Abdullah Burja"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1452.1.1 = {
	monarch = {
		name = "Dakauta"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1452.5.1 = {
	monarch = {
		name = "Atuma"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1452.9.1 = {
	monarch = {
		name = "Yaqub"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1463.1.1 = {
	monarch = {
		name = "Muhammad Rumfa"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1499.1.1 = {
	monarch = {
		name = "Abdullah"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1509.1.1 = {
	monarch = {
		name = "Muhammad Kisoki"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1565.1.1 = {
	monarch = {
		name = "Yakufu"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1565.5.1 = {
	monarch = {
		name = "Daud Abasama"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1565.9.1 = {
	monarch = {
		name = "Abu-Bakr Kado"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1573.1.1 = {
	monarch = {
		name = "Muhammad Shashere"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1582.1.1 = {
	monarch = {
		name = "Muhammad Zaki"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1618.1.1 = {
	monarch = {
		name = "Muhammad Nazaki"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1623.1.1 = {
	monarch = {
		name = "Kutumbi"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1648.1.1 = {
	monarch = {
		name = "al-Hajj"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1649.1.1 = {
	monarch = {
		name = "Shekarau"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1651.1.1 = {
	monarch = {
		name = "Muhammad Kukuna"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1652.1.1 = {
	monarch = {
		name = "Soyaki"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1652.6.1 = {
	monarch = {
		name = "Muhammad Kukuna"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1660.1.1 = {
	monarch = {
		name = "Bawa"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1670.1.1 = {
	monarch = {
		name = "Dadi"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1703.1.1 = {
	monarch = {
		name = "Muhammad Sharif"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1731.1.1 = {
	monarch = {
		name = "Kumbari"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1743.1.1 = {
	monarch = {
		name = "al-Hajj Kabe"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1753.1.1 = {
	monarch = {
		name = "Yaji II"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1768.1.1 = {
	monarch = {
		name = "Baba Zaki"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1776.1.1 = {
	monarch = {
		name = "Daud Abasama II"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1781.1.1 = {
	monarch = {
		name = "Muhammad al-Walid"
		dynasty = Barbushe  
		DIP = 3
		ADM = 3
		MIL = 3
	}
}
