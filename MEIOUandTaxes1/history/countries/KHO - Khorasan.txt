# KHO - Turcoman Federation
# 2010-jan-16 - FB - HT3 changes

government = amalgamation_government government_rank = 1
mercantilism = 0.0
primary_culture = turkmeni
religion = sunni
technology_group = steppestech
capital = 439

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1356.1.1 = {
	monarch = {
		name = "Baba"
		dynasty = "Ersari"
		ADM = 4
		DIP = 4
		MIL = 4
	}
} # Legendary ruler, founder of the Sain-Khan Turkmen Confederation in Mangyshlak (Mangystau Province) and Balkan mountains (Balkan Province)

1511.1.1 = {
	government = tribal_monarchy government_rank = 3
}