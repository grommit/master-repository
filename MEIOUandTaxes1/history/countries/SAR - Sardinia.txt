#SAR - Sardinia

government = feudal_monarchy government_rank = 5
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = sardinian
capital = 2241

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1387.1.1 = {
	monarch = { 
		name = "Mariano V"
		dynasty = "d'Arborea"
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1387.1.1 = {
	heir = {
		name = "Guglielmo"
		monarch_name = "Guglielmo"
		dynasty = "d'Arborea"
		birth_date = 1387.1.1
		death_date = 1430.1.1
		claim = 80
		DIP = 3
		MIL = 2
		ADM = 2
	}
}

1407.1.1 = {
	monarch = { 
		name = "Guglielmo"
		dynasty = "d'Arborea"
		DIP = 3
		MIL = 2
		ADM = 2
	}
}

1410.3.1  = {
	capital = 127	# Cagliari
}
