# NAG - Nagaur

government = indian_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = marwari
religion = sunni
technology_group = indian
capital = 519
fixed_capital = 519 # 

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
	set_country_flag = indian_sunni_state
}

1399.1.1 = {
	monarch = {
		name = "Firuz Khan"
		dynasty = "Dandani"
		ADM = 1
		DIP = 2
		MIL = 3
	}
}

1399.1.1 = {
	heir = {
		name = "Qiyam"
		monarch_name = "Qiyam Khan"
		dynasty = "Dandani"
		birth_date = 1399.1.1
		death_date = 1453.1.1
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 3
	}
}

1428.1.1 = {
	monarch = {
		name = "Qiyam Khan"
		dynasty = "Dandani"
		ADM = 2
		DIP = 1
		MIL = 3
	}
}

1428.1.1 = {
	heir = {
		name = "Shams"
		monarch_name = "Shams Khan"
		dynasty = "Dandani"
		birth_date = 1410.1.1
		death_date = 1500.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 3
	}
}

1453.1.1 = {
	monarch = {
		name = "Shams Khan"
		dynasty = "Dandani"
		ADM = 1
		DIP = 2
		MIL = 3
	}
}

1453.1.1 = {
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad Khan"
		dynasty = "Dandani"
		birth_date = 1410.1.1
		death_date = 1513.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1500.1.1 = {
	monarch = {
		name = "Muhammad Khan"
		dynasty = "Dandani"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}
