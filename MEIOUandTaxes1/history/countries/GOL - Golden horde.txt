# GOL - Golden Horde

government = steppe_horde government_rank = 5
mercantilism = 0.0
primary_culture = tartar
religion = sunni
technology_group = steppestech
capital = 1310	# Sarai al Jadid
historical_rival = SHY
historical_rival = CRI
historical_rival = KAZ
historical_rival = AST
historical_rival = NOG

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Berdi Beg"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 3
	}
	add_horde_unity = -50
	add_country_modifier = {
		name = genghis_khanate
		duration = -1
	}
	set_country_flag = tatar_treaty
}

1360.5.28 = {
	monarch = {
		name = "Hizr"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1361.8.1 = {
	monarch = {
		name = "Temur Hoja"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1362.2.14 = {
	monarch = {
		name = "Amurat"
		dynasty = "Jochid"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1367.10.27 = {
	monarch = {
		name = "Aziz Hoja"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1369.1.5 = {
	monarch = {
		name = "Jani Beg II"
		dynasty = "Jochid"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

1370.6.4 = {
	monarch = {
		name = "Tulun Beg Khanum"
		dynasty = "Jochid"
		ADM = 3
		DIP = 3
		MIL = 2
		female = yes
	}
}

1373.3.19 = {
	monarch = {
		name = "Ai Beg"
		dynasty = "Jochid"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1376.1.1 = {
	monarch = {
		name = "Tokhtamysh"
		dynasty = "Jochid"
		ADM = 6
		DIP = 6
		MIL = 6
		leader = {
			name = "Tokhtamysh"
			type = general
			rank = 1
			fire = 4
			shock = 4
			manuever = 5
			siege = 0
			death_date = 1406.1.1
		}
	}
}
1394.1.1 = {
	monarch = {
		name = "Temur Qutlugh"
		dynasty = "Jochid"
		ADM = 4
		DIP = 2
		MIL = 4
	}
	heir = {
		name = "Shadi Beg"
		monarch_name = "Shadi Beg"
		dynasty = "Jochid"
		birth_date = 1380.1.1
		death_date = 1410.1.1
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 3
	}
	add_country_modifier = { name = wrath_of_god duration = -1 }
}

1400.1.1 = {
	monarch = {
		name = "Shadi Beg"
		dynasty = "Jochid"
		ADM = 1
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Pulad"
		monarch_name = "Pulad Khan"
		dynasty = "Jochid"
		birth_date = 1380.1.1
		death_date = 1410.1.1
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 1
	}
}

1407.1.1 = {
	monarch = {
		name = "Pulad Khan"
		dynasty = "Jochid"
		ADM = 5
		DIP = 4
		MIL = 1
	}
}

1410.1.1 = {
	monarch = {
		name = "Tem�r"
		dynasty = "Jochid"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1412.1.1 = {
	monarch = {
		name = "Jalal Al-Din"
		dynasty = "Jochid"
		ADM = 2
		DIP = 4
		MIL = 1
	}
	heir = {
		name = "Karim Berdi"
		monarch_name = "Karim Berdi"
		dynasty = "Jochid"
		birth_date = 1380.1.1
		death_date = 1414.1.1
		claim = 65
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1413.1.1 = {
	monarch = {
		name = "Karim Berdi"
		dynasty = "Jochid"
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1414.1.1 = {
	monarch = {
		name = "Kebek"
		dynasty = "Jochid"
		ADM = 4
		DIP = 3
		MIL = 4
	}
}

1417.1.1 = {
	monarch = {
		name = "Jabbar Berdi"
		dynasty = "Jochid"
		ADM = 2
		DIP = 3
		MIL = 5
	}
}

1419.1.1 = {
	monarch = {
		name = "Ulugh Muhammad"
		dynasty = "Jochid"
		ADM = 4
		DIP = 2
		MIL = 2
	}
}

1420.1.1 = {
	monarch = {
		name = "Devlat Berdi"
		dynasty = "Jochid"
		ADM = 5
		DIP = 5
		MIL = 2
	}
}

1421.1.1 = {
	monarch = {
		name = "Baraq"
		dynasty = "Jochid"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1428.1.1 = {
	monarch = {
		name = "Ulugh Muhammad"
		dynasty = "Jochid"
		ADM = 4
		DIP = 2
		MIL = 2
	}
}

1433.1.1 = {
	monarch = {
		name = "Sayy�d Ahmad I"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1465.1.1 = {
	monarch = {
		name = "Ahmad"
		dynasty = "Jochid"
		ADM = 2
		DIP = 1
		MIL = 3
	}
}

1481.1.1 = {
	monarch = {
		name = "Sayy�d Ahmad II"
		dynasty = "Jochid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1481.1.1 = { leader = {	name = "Sheyhk Ahmad"          	type = general	rank = 0	fire = 1	shock = 2	manuever = 4	siege = 0	death_date = 1505.1.1 } }
