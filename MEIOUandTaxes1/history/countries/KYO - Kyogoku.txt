# KYO - Kyogoku
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = early_medieval_japanese_gov
government_rank = 3
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = chubu
religion = mahayana
technology_group = chinese
capital = 2796 #Omi

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1306.1.1 = {
	monarch = {
		name = "Takauji" 
		dynasty = "Kyogoku"
		ADM = 5
		DIP = 6
		MIL = 5
		}
}

1328.1.1 = {
	heir = {
		name = "Takahide"
		monarch_name = "Takahide"
		dynasty = "Togashi"
		birth_date = 1328.1.1
		death_date = 1391.11.7
		claim = 90
		ADM = 4
		DIP = 5
		MIL = 4
	}
}
