
government = chinese_monarchy_2 government_rank = 4
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = lanyin
capital = 2251

historical_rival = SNG
historical_rival = QII

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 1 }
}
1343.7.19 = {
	monarch = {
		name = "Siqi"
		dynasty = "Li"
		ADM = 3
		DIP = 3
		MIL = 6
	}
}
1351.1.1  = {
#	set_country_flag = lost_mandate_of_heaven
#	add_country_modifier = { name = wrath_of_god duration = -1 }
}
