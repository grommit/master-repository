# CSK - Chosokabe
# 2010-jan-20 - FB - HT3 changes

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = shikoku
religion = mahayana
technology_group = chinese
capital = 1022	# Tosa

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1415.1.1 = {
	monarch = {
		name = "Yoshishige" 
		dynasty = "Chosokabe"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1430.1.1 = {
	monarch = {
		name = "Fumikane" 
		dynasty = "Chosokabe"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1450.1.1 = {
	monarch = {
		name = "Motokado" 
		dynasty = "Chosokabe"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1460.1.1 = {
	heir = {
		name = "Katsuchika"
		monarch_name = "Katsuchika"
		dynasty = "Chosokabe"
		birth_date = 1440.1.1
		death_date = 1478.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 2
	}
}
	
1477.1.1 = { 
	monarch = {
		name = "Katsuchika"
		dynasty = "Chosokabe"
#		birth_date = 1440.1.1
#		death_date = 1478.1.1
		ADM = 2
		DIP = 3
		MIL = 2
		}
	}

1477.1.1 = {
	heir = {
		name = "Kanetsugu"
		monarch_name = "Kanetsugu"
		dynasty = "Chosokabe"
		birth_date = 1460.1.1
		death_date = 1508.9.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
	
1478.1.1 = { 
	monarch = {
		name = "Kanetsugu"
		dynasty = "Chosokabe"
#		birth_date = 1460.1.1
#		death_date = 1508.9.1
		ADM = 3
		DIP = 2
		MIL = 3
		}
	}

1504.1.1 = {
	heir = {
		name = "Kunichika"
		monarch_name = "Kunichika"
		dynasty = "Chosokabe"
		birth_date = 1504.1.1
		death_date = 1560.7.8
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 4
	}
}
	
1508.9.1 = { 
	monarch = {
		name = "Kunichika"
		dynasty = "Chosokabe"
#		birth_date = 1504.1.1
#		death_date = 1560.7.8
		ADM = 4
		DIP = 4
		MIL = 4
		}
	}

1539.1.1 = {
	heir = {
		name = "Motochika"
		monarch_name = "Motochika"
		dynasty = "Chosokabe"
		birth_date = 1539.1.1
		death_date = 1599.7.11
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 5
	}
}
	
1560.7.8 = { 
	monarch = {
		name = "Motochika"
		dynasty = "Chosokabe"
#		birth_date = 1539.1.1
#		death_date = 1599.7.11
		ADM = 4
		DIP = 4
		MIL = 5
		}
	}

1565.1.1 = {
	heir = {
		name = "Nobuchika"
		monarch_name = "Nobuchika"
		dynasty = "Chosokabe"
		birth_date = 1565.1.1
		death_date = 1587.1.20
		claim = 90
		ADM = 4
		DIP = 3
		MIL = 3
	}
}	
	
1587.1.20 = {
	heir = {
		name = "Morichika"
		monarch_name = "Morichika"
		dynasty = "Chosokabe"
		birth_date = 1575.1.1
		death_date = 1615.6.11
		claim = 80
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

# 1599.7.11 - Chosokabe Motochika dies, end of the clan
