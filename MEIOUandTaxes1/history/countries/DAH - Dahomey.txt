# DAH - Dahomey

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = sub_saharan
religion = west_african_pagan_reformed
primary_culture = yorumba
capital = 1129

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1440.1.1 = {
	monarch = {
		name = "Ewuare"
		dynasty = "Dan-Home"
		ADM = 6
		DIP = 2
		MIL = 3
	}
}

1600.1.1 = {
	monarch = {
		name = "Gangnihessou"
		dynasty = "Dan-Home"
		ADM = 6
		DIP = 2
		MIL = 3
	}
}

1620.1.1 = {
	monarch = {
		name = "Dakodonou"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1645.1.1 = {
	monarch = {
		name = "Houegbadja"
		dynasty = "Dan-Home"
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1685.1.1 = {
	monarch = {
		name = "Akaba"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1708.1.1 = {
	monarch = {
		name = "Agadja"
		dynasty = "Dan-Home"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1732.1.1 = {
	monarch = {
		name = "Tegbessou"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1774.1.1 = {
	monarch = {
		name = "Kpengla"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1789.1.1 = {
	monarch = {
		name = "Agonglo"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1797.1.1 = {
	monarch = {
		name = "Adandozan"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1818.1.1 = {
	monarch = {
		name = "Ghezo"
		dynasty = "Dan-Home"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
