#YOR - York
#MEIOU-GG Governemnt changes
#10.01.27 FB-HT3 - make HT3 changes

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
technology_group = western
primary_culture = northern_e
religion = catholic
capital = 245	# York

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -1 }
}

1341.6.5 = {
	monarch = {
		name = "Edmund"
		dynasty = "Plantagenet"
		ADM = 5
		DIP = 5
		MIL = 4
	}
}

1402.8.1 = {
	monarch = {
		name = "Edward"
		dynasty = "Plantagenet"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1431.1.1 = {
	monarch = {
		name = "Richard II"
		dynasty = "Plantagenet"
		ADM = 5
		DIP = 2
		MIL = 4
	}
}

1460.1.1 = {
	monarch = {
		name = "Edward"
		dynasty = "York"
		ADM = 3
		DIP = 5
		MIL = 5
	}
}

1474.1.1 = {
	monarch = {
		name = "Richard III"
		dynasty = "York"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1494.1.1 = {
	monarch = {
		name = "Henry"
		dynasty = "Tudor"
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1547.1.1 = {
	monarch = {
		name = "Richard IV"
		dynasty = "Tudor"
		ADM = 2
		DIP = 4
		MIL = 3
	}
}

1605.1.1 = {
	monarch = {
		name = "Charles"
		dynasty = "Stuart"
		ADM = 3
		DIP = 2
		MIL = 5
	}
}

1633.1.1 = {
	monarch = {
		name = "James"
		dynasty = "Stuart"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1685.1.1 = {
	monarch = {
		name = "Richard V"
		dynasty = "Stuart"
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1716.1.1 = {
	monarch = {
		name = "Ernest Augustus"
		dynasty = "von Hannover"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1728.1.1 = {
	monarch = {
		name = "Richard VI"
		dynasty = "von Hannover"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1760.1.1 = {
	monarch = {
		name = "Richard VII"
		dynasty = "von Hannover"
		ADM = 2
		DIP = 4
		MIL = 1
	}
}

1767.1.1 = {
	monarch = {
		name = "Frederick Augustus"
		dynasty = "von Hannover"
		ADM = 2
		DIP = 1
		MIL = 6
	}
}
