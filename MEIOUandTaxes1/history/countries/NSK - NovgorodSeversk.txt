# NSK - Novgorod-Seversk

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 311

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1386.1.1 = {
	monarch = {
		name = "Yury I"
		dynasty = "Rostaslavichi"
		adm = 0
		dip = 0
		mil = 3
	}
}
