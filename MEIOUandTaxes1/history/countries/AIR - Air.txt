# No previous file for Agades

government = tribal_republic government_rank = 5
mercantilism = 0.0
primary_culture = tuareg
religion = sunni
technology_group = soudantech
capital = 2316	# Agades

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
}
#Sultans of Air
1444.1.1 = {
	monarch = {
		name = "Yunus"
		dynasty = "Istanbulewa"
		dip = 2
		adm = 3
		mil = 2
	}
} #Founder. Most dating says 1449, but it's somewhat uncertain.
#(Later) dynastic claims of ties to the Ottomans explain dynastic name
#Unknown number of unknown rulers

1687.1.1 = {
	monarch = {
		name = "Muhammad Agg-Abba"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1721.1.1 = {
	monarch = {
		name = "Muhammad al-Mu'min"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1722.1.1 = {
	monarch = {
		name = "Muammad Agg-'A'isha"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1735.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1739.1.1 = {
	monarch = {
		name = "Muhammad Guma"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 6
		MIL = 2
	}
}

1744.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		dynasty = "Istanbulewa"
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1759.1.1 = {
	monarch = {
		name = "Muhammad Guma II"
		dynasty = "Istanbulewa"
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1763.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1768.1.1 = {
	monarch = {
		name = "Muhammad al-'Adil"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1810.1.1 = {
	monarch = {
		name = "Muhammad ad-Dani"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1815.1.1 = {
	monarch = {
		name = "Muhammad al-Baqiri"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}

1816.1.1 = {
	monarch = {
		name = "Muhammad Guma III"
		dynasty = "Istanbulewa"
		DIP = 2
		ADM = 1
		MIL = 2
	}
}