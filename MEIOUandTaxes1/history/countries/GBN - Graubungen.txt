# GBN - Grischun (Graubunden)

government = administrative_republic
mercantilism = 0.0	##0
technology_group = western
religion = catholic
primary_culture = romansh
capital = 166	# Grischun

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "The Tagsatzung"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1528.1.1 = {
	religion = reformed
	primary_culture = high_alemanisch
}

1798.1.1 = {
	government = constitutional_republic
}
