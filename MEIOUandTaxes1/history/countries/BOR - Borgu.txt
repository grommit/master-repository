# BOR - Kingdom of Borgu

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = mali
religion = sunni
technology_group = sub_saharan
capital = 2780

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}
