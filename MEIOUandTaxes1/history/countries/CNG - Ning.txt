
government = chinese_monarchy_3 government_rank = 5
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = wuhan
capital = 2480

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = -2 }
}
1377.1.1 = {
	monarch = {
		name = "Quan"
		dynasty = "Zhu"
		ADM = 5
		DIP = 3
		MIL = 6
	}
}
1418.1.1 = {
	heir = {
		name = "Dienpei"
		monarch_name = "Dienpei"
		dynasty = "Zhu"
		birth_date = 1418.1.1
		death_date = 1491.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1448.1.1 = {
	monarch = {
		name = "Dienpei"
		dynasty = "Zhu"
		ADM = 5
		DIP = 3
		MIL = 6
	}
}
1449.7.19 = {
	heir = {
		name = "Kunjun"
		monarch_name = "Kunjun"
		dynasty = "Zhu"
		birth_date = 1449.7.19
		death_date = 1497.7.28
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1491.1.1 = {
	monarch = {
		name = "Kunjun"
		dynasty = "Zhu"
		ADM = 5
		DIP = 3
		MIL = 6
	}
	heir = {
		name = "Chenhao"
		monarch_name = "Chenhao"
		dynasty = "Zhu"
		birth_date = 1469.1.1
		death_date = 1521.1.12
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
1497.7.28 = {
	monarch = {
		name = "Chenhao"
		dynasty = "Zhu"
		ADM = 5
		DIP = 3
		MIL = 6
	}
}
