# MAM - Mameluks
# 2010-jan-21 - FB - HT3 changes

government = despotic_monarchy government_rank = 5 #SULTANATE
mercantilism = 10
primary_culture = egyptian
add_accepted_culture = shami
religion = sunni
technology_group = muslim
capital = 361	# Cairo
historical_rival = TIM
historical_rival = JAI
historical_rival = ERE
historical_neutral = AYD

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1258.1.1 = {
	set_country_flag = caliph
	add_country_modifier = { name = obstacle_traditional_military duration = -1 }
	add_country_modifier = { name = obstacle_succession duration = -1 }
	add_country_modifier = { name = obstacle_shifting_loyalties duration = -1 }
}

1354.4.9 = {
	monarch = {
		name = "al-Hasan"
		dynasty = "Burji"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1360.1.1 = {
	set_country_flag = ramazanid_beylik_indp
}

1361.3.26 = {
	monarch = {
		name = "Muhammad III"
		dynasty = "Burji"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1363.1.12 = {
	monarch = {
		name = "Shaban II"
		dynasty = "Burji"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1376.8.13 = {
	monarch = {
		name = "'Ali IV"
		dynasty = "Burji"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1381.7.1 = {
	monarch = {
		name = "Hajji II"
		dynasty = "Burji"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1382.2.19 = {
	monarch = {
		name = "Barquq"
		dynasty = "Burji"
		ADM = 2
		DIP = 6
		MIL = 5
	}
}
1390.1.1 = { #Formation of Turkish Empire
	historical_rival = TUR
	}
1399.1.1 = {
	monarch = {
		name = "al-Nasr Faraj"
		dynasty = "Burji"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1412.1.1 = {
	monarch = {
		name = "Shaikh al-Mahmudi"
		dynasty = "Burji"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1421.1.1 = {
	monarch = {
		name = "Muhammad IV"
		dynasty = "Burji"
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

1422.1.1 = {
	monarch = {
		name = "Barsbai"
		dynasty = "Burji"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1438.6.8 = {
	monarch = {
		name = "Jaqmaq"
		dynasty = "Burji"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1453.2.13 = {
	monarch = {
		name = "In�l"
		dynasty = "Burji"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1461.2.25 = {
	monarch = {
		name = "Kh�shqadam"
		dynasty = "Burji"
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1461.2.25 = {
	heir = {
		name = "Q�'itbay"
		monarch_name = "Q�'itbay"
		dynasty = "Burji"
		birth_date = 1418.1.1
		death_date = 1496.7.8
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1465.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
	# innovative_narrowminded = 4
			} # The Mameluk Nobility

1467.10.10 = {
	monarch = {
		name = "Q�'itbay"
		dynasty = "Burji"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1496.7.8 = {
	monarch = {
		name = "Muhammad II"
		dynasty = "Burji"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1498.10.30 = {
	monarch = {
		name = "Q�ns�h I"
		dynasty = "Burji"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1500.6.29 = {
	monarch = {
		name = "J�nbalat"
		dynasty = "Burji"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1501.6.26 = {
	monarch = {
		name = "Q�ns�h II"
		dynasty = "Burji"
		ADM = 2
		DIP = 2
		MIL = 4
		leader = {	name = "Al Kansur"             	type = general	rank = 0	fire = 1	shock = 4	manuever = 5	siege = 0 }
	}
}

1510.1.1 = {
}

1516.8.25 = {
	monarch = {
		name = "T�m�n Bay II"
		dynasty = "Burji"
		ADM = 1
		DIP = 1
		MIL = 3
		leader = {	name = "T�m�n Bay II"		type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 1 }
	}
}

1517.1.1 = {
	clr_country_flag = caliph
}	#defeat by Ottomans

1530.1.1 = {
	capital = 361	# Cairo
	remove_accepted_culture = shami
	remove_historical_rival = TUR
}

1768.1.1 = {
	monarch = {
		name = "Al� l-Kab�r"
		dynasty = "Beg"
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1773.5.2 = {
	monarch = {
		name = "Mehmed"
		dynasty = "Beg"
		ADM = 5
		DIP = 2
		MIL = 2
	}
}

1775.6.11 = {
	monarch = {
		name = "Murad"
		dynasty = "Beg"
		ADM = 3
		DIP = 4
		MIL = 4
	}
}

1792.1.1 = { leader = {	name = "Ibrahim Bey"           	type = general	rank = 1	fire = 2	shock = 2	manuever = 4	siege = 0	death_date = 1804.3.14 } }

1792.1.1 = { leader = {	name = "Murad Bey"             	type = general	rank = 2	fire = 1	shock = 3	manuever = 4	siege = 0	death_date = 1801.4.22 } }

1801.4.23 = {
	monarch = {
		name = "Osman"
		dynasty = "Bardisi"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1804.3.14 = {
	monarch = {
		name = "Muhammad 'Al�"
		dynasty = "Muhammad"
		ADM = 6
		DIP = 5
		MIL = 6
		leader = { name = "Mehmet Ali"		type = general	rank = 0	fire = 1	shock = 4	manuever = 5	siege = 0 }
	}
}
