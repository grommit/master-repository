# BSL - Basel

government = theocratic_government government_rank = 3
mercantilism = 5
primary_culture = high_alemanisch
religion = catholic
technology_group = western
capital = 2359	# Basel

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}
1335.1.1 = {
	monarch = {
		name = "Johann II von Munsingen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1365.1.1 = {
	monarch = {
		name = "Jean III de Vienne"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1382.1.1 = {
	monarch = {
		name = "Imer von Ramstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

 1392.1.1 = {
	monarch = {
		name = "Friedrich von Blankenheim"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1393.1.1 = {
	monarch = {
		name = "Konrad Munch von Landskron"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1399.1.1 = {
	monarch = {
		name = "Humbrecht von Neuenburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1418.1.1 = {
	monarch = {
		name = "Hartmann II von Munchenstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1423.1.1 = {
	monarch = {
		name = "Johann IV von Fleckenstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1437.1.1 = {
	monarch = {
		name = "Friedrich von der Pfalz"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1451.1.1 = {
	monarch = {
		name = "Arnold von Rothburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1458.1.1 = {
	monarch = {
		name = "Johann V von Venningen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1479.1.1 = {
	monarch = {
		name = "Caspar von Mühlhausen"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1502.1.1 = {
	monarch = {
		name = "Christoph von Utenheim"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1527.1.1 = {
	monarch = {
		name = "Philipp von Gundelsheim"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1530.1.1 = {
	religion = protestant
	}

1554.1.1 = {
	monarch = {
		name = "Melchior von Lichtenfels"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1575.1.1 = {
	monarch = {
		name = "Jakob Christoph Blarer von Wartensee"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1608.1.1 = {
	monarch = {
		name = "Wilhelm Rink von Baldenstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1628.1.1 = {
	monarch = {
		name = "Johann Heinrich von Ostheim"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1646.1.1 = {
	monarch = {
		name = "Beatus Albrecht von Ramstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1651.1.1 = {
	monarch = {
		name = "Johann Franz von Schonau-Zell"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1656.1.1 = {
	monarch = {
		name = "Johann Konrad I von Roggenbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1693.1.1 = {
	monarch = {
		name = "Wilhelm Jakob Rink von Baldenstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1705.1.1 = {
	monarch = {
		name = "Johann Konrad II von Reinach-Hirtzbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1737.1.1 = {
	monarch = {
		name = "Jakob Sigismund von Reinach-Steinbrunn"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1744.1.1 = {
	monarch = {
		name = "Josef Wilhelm Rink von Baldenstein"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1762.1.1 = {
	monarch = {
		name = "Simon Nikolas von Froburg"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1775.1.1 = {
	monarch = {
		name = "Ludwig von Wangen-Geroldseck"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1782.1.1 = {
	monarch = {
		name = "Sigismund von Roggenbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1794.1.1 = {
	monarch = {
		name = "Franz Xaver von Neveu"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
