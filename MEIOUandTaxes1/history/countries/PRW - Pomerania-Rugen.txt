# PRW - Pomerania-R�gen

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = pommeranian
religion = catholic
technology_group = western
capital = 28

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1326.8.1 = {
	monarch = {
		name = "Barnim IV"
		dynasty = "Gryf"
		adm = 2
		dip = 3
		mil = 2
	}
}

1345.1.1 = {
	heir = {
		name = "Wartislaw"
		monarch_name = "Wartislaw VI" 
		dynasty = "Gryf"
		birth_date = 1345.1.1 #um
		death_date = 1394.6.13
		claim = 95
		adm = 2
		dip = 2
		mil = 2
	}
}

1368.8.24 = {
	monarch = {
		name = "Wartislaw VI"
		dynasty = "Gryf"
		adm = 2
		dip = 2
		mil = 2
	}
}

1373.1.1 = {
	heir = {
		name = "Wartislaw"
		monarch_name = "Wartislaw VIII" 
		dynasty = "Gryf"
		birth_date = 1373.1.1 
		death_date = 1415.8.23
		claim = 95
		adm = 3
		dip = 4
		mil = 3
	}
}

1394.6.13 = {
	monarch = {
		name = "Wartislaw VIII"
		dynasty = "Gryf"
		adm = 3
		dip = 4
		mil = 3
	}
}

1407.1.1 = {
	heir = {
		name = "Swantibor"
		monarch_name = "Swantibor II" 
		dynasty = "Gryf"
		birth_date = 1407.1.1 #oder 1408
		death_date = 1432.5.23 #nach 12.5 vor 24.5.
		claim = 95
		adm = 2
		dip = 2
		mil = 2
	}
}

1415.8.23 = {
	monarch = {
		name = "Swantibor II"
		dynasty = "Gryf"
		adm = 2
		dip = 2
		mil = 2
	}
}

1415.8.23 = {
	heir = {
		name = "Barnim"
		monarch_name = "Barnim VIII" 
		dynasty = "Gryf"
		birth_date = 1405.1.1 #bis 1407
		death_date = 1451.12.19
		claim = 95
		adm = 2
		dip = 3
		mil = 4
	}
}

1432.5.23 = {
	monarch = {
		name = "Barnim VIII"
		dynasty = "Gryf"
		adm = 2
		dip = 3
		mil = 4
	}
}

1432.5.23 = {
	heir = {
		name = "Wartislaw"
		monarch_name = "Wartislaw IX" 
		dynasty = "Gryf"
		birth_date = 1400.1.1 #um
		death_date = 1457.4.17
		claim = 95
		adm = 3
		dip = 3
		mil = 4
	}
}

1451.12.19 = {
	monarch = {
		name = "Wartislaw IX"
		dynasty = "Gryf"
		adm = 3
		dip = 3
		mil = 4
	}
}

1451.12.19 = {
	heir = {
		name = "Wartislaw"
		monarch_name = "Wartislaw X" 
		dynasty = "Gryf"
		birth_date = 1435.1.1 #um
		death_date = 1478.12.17
		claim = 95
		adm = 2
		dip = 4
		mil = 4
	}
}

1457.4.17 = {
	monarch = {
		name = "Wartislaw X"
		dynasty = "Gryf"
		adm = 2
		dip = 4
		mil = 4
	}
}

#Ende 1478 zu Pommern-Stettin

1569.2.3 = { 
	monarch = {
		name = "Bogislaw XIII"
		dynasty = "Gryf"
		adm = 3
		dip = 3
		mil = 2
	}
}
