name = "Delhi Bengal War"
#casus_belli = cb_disloyal_vassal
war_goal = {
	type = take_capital_disloyal
	casus_belli = cb_disloyal_vassal
	tag = BNG
}

1356.1.1 = {
	add_attacker = DLH
	add_defender = BNG
}
1369.1.1 = {
	rem_attacker = DLH
	rem_defender = BNG
}
