#MEIOU-FB Indonesia mod v3 - for IN+JV
#note: see dutch_indonesian_alliances.txt for alliances including the dutch
#note: the name of this file is inherited from standard EU3 -
#it should really be called south-east-asian_alliances.txt!

#ALLIANCES

#Bacan alliances

vassal = { first = TER second = BAC start_date = 1578.1.1 end_date = 1658.1.1 }
alliance = { first = TER second = BAC start_date = 1356.1.1 end_date = 1577.12.31 #1658.1.1 validator fix no vassal and alliance at same time
 }

#Demak alliances
alliance = { first = DEM second = BAN start_date = 1525.1.1 end_date = 1677.7.13 }
alliance = { first = DEM second = CBT start_date = 1478.1.1 end_date = 1616.12.31 }

#ROYAL MARRIAGES

# Chey Chettha II and Sai Vuong's daughter
royal_marriage = { first = KHM second = ANN start_date = 1620.1.1 end_date = 1628.1.1 }

#VASSALS

#Vietnamese vassal,  tonkin annexed by Annam in 1787
#vassal = { first = DAI second = CHA start_date = 1471.4.1 end_date = 1529.12.31 } 
vassal = { first = ANN second = CHA start_date = 1693.1.1 end_date = 1832.1.1 } 
vassal = { first = ANN second = KHM start_date = 1658.1.1 end_date = 1673.1.1 }
vassal = { first = ANN second = KHM start_date = 1710.1.1 end_date = 1720.1.1 }
#vassal = { first = DAI second = KHM date = 1834.1.1 end_date = 1840.12.31 } 

#Burmese vassals
vassal = { first = AVA second = TAU start_date = 1481.1.1 end_date = 1527.1.1 }  #uncertain begin
vassal = { first = TAU second = AYU start_date = 1564.2.1 end_date = 1584.5.3 }
vassal = { first = TAU second = LNA start_date = 1558.1.1 end_date = 1578.1.1 }
vassal = { first = TAU second = LNA start_date = 1663.1.1 end_date = 1752.2.28 }
vassal = { first = BRM second = LUA start_date = 1771.1.1 end_date = 1778.1.1 }
vassal = { first = BRM second = LNA start_date = 1752.2.28 end_date = 1775.2.14 }

#Ayutthayan vassals
vassal = { first = AYU second = NST start_date = 1356.1.1 end_date = 1467.4.8 }
#vassal = { first = AYU second = KHM start_date = 1431.1.1 end_date = 1618.1.1 }
vassal = { first = AYU second = PAT start_date = 1636.1.1 end_date = 1767.4.8 }
#vassal = { first = AYU second = KHM start_date = 1697.1.1 end_date = 1710.1.1 }
#vassal = { first = AYU second = KHM start_date = 1720.1.1 end_date = 1767.4.8 }
vassal = { first = AYU second = PAT start_date = 1500.1.1 end_date = 1632.1.1 }

#Siamese vassals
#vassal = { first = SIA second = CHK start_date = 1772.1.1 end_date = 1899.1.1 }
vassal = { first = SIA second = KHM start_date = 1767.4.8 end_date = 1834.1.1 }
vassal = { first = SIA second = KHM start_date = 1841.1.1 end_date = 1909.1.1 } #about
vassal = { first = SIA second = LUA start_date = 1779.1.1 end_date = 1795.1.1 }
vassal = { first = SIA second = PAT start_date = 1767.4.8 end_date = 1902.1.1 }
vassal = { first = SIA second = VIE start_date = 1778.1.1 end_date = 1828.1.1 }

# 1885, 17-28 November third war GBR-BRM
