
#Pommern Stettin-Wolgast
alliance = {
	first = PST
	second = PWO
	start_date = 1356.1.1
	end_date = 1625.2.6
}

#Pommern Stettin-R�gen
alliance = {
	first = PST
	second = PRW
	start_date = 1356.1.1
	end_date = 1474.1.1
}

#Pommern Stettin-Stolp
alliance = {
	first = PST
	second = PSP
	start_date = 1368.5.25
	end_date = 1459.5.3
}

#Pommern Stettin-Stolp2
alliance = {
	first = PST
	second = PSP
	start_date = 1569.2.3
	end_date = 1622.10.31
}

#Pommern Wolgast-R�gen
union = {
	first = PWO
	second = PRW
	start_date = 1474.1.1
	end_date = 1478.12.17
}

#Pommern Wolgast-R�gen
#validator fix - no union and alliance at the same time
# alliance = {
#	first = PWO
#	second = PRW
#	start_date = 1474.1.1
#	end_date = 1478.12.17
# }

#Pommern Stettin-R�gen
alliance = {
	first = PST
	second = PRW
	start_date = 1569.2.3
	end_date = 1603.1.1
}
