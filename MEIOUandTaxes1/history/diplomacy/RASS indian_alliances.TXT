#MEIOU-FB India 1337+ mod Aug 08
#BAS temporarily removed as not currently active in the game

# Suri Empire
#union = {
#	first = MUG
#	second = BNG
#	start_date = 1540.6.1
#	end_date = 1553.1.1
#}

#alliance = {
#	first = MUG
#	second = RAJ
#	start_date = 1556.1.1
#	end_date = 1658.1.1
#}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
#alliance = {
#	first = BAS
#	second = BRR
#	start_date = 1564.1.1
#	end_date = 1565.7.26
#}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
#alliance = {
#	first = BAS
#	second = BIJ
#	start_date = 1564.1.1
#	end_date = 1565.7.26
#}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
#alliance = {
#	first = BAS
#	second = GOC
#	start_date = 1564.1.1
#	end_date = 1565.7.26
#}
protectorate = {
	first = GBR
	second = COC
	start_date = 1792.1.1
	end_date = 1947.1.1
}

protectorate = {
	first = FRA
	second = KRK
	start_date = 1749.8.1 # Chanda Sahib assumes the throne with French aid
	end_date = 1754.8.2
}

protectorate = {
	first = GBR
	second = KRK
	start_date = 1754.8.2 # British client recognized as king
	end_date = 1801.1.1
}


protectorate = {
	first = NED
	second = TRV
	start_date = 1729.1.1 # Travancore expands with Dutch aid
	end_date = 1739.1.1 # Travancore�Dutch War
}

protectorate = {
	first = GBR
	second = TRV
	start_date = 1788.1.1 # Travancore seeks british aid fearing Mysore
	end_date = 1795.1.1 # Travancore becomes more dependent of GBR
}

protectorate = {
	first = GBR
	second = TRV
	start_date = 1795.1.1 # Travancore becomes more dependent of GBR
	end_date = 1949.1.1
}

protectorate = {
	first = FRA
	second = MYS
	start_date = 1780.1.1
	end_date = 1789.7.14
}

protectorate = {
	first = GBR
	second = MYS
	start_date = 1799.1.1
	end_date = 1947.1.1
}

protectorate = {
	first = GBR
	second = MAW
	start_date = 1818.1.1
	end_date = 1946.1.1
}

protectorate = {
	first = GBR
	second = MER
	start_date = 1818.1.1
	end_date = 1858.1.1
}


# The battle of Buxar
alliance = {
	first = MUG
	second = ODH
	start_date = 1764.1.1
	end_date = 1764.10.1
}

# The battle of Buxar
#alliance = {
#	first = MUG
#	second = BNG
#	start_date = 1764.1.1
#	end_date = 1764.10.1
#}

#Vassal of Vijayanagar
#vassal = {
#	first = VIJ
#	second = MYS
#	start_date = 1376.1.1
#	end_date = 1565.1.1
#}

#Vassal of Vijayanagar
#vassal = {
#	first = VIJ
#	second = TRV
#	start_date = 1376.1.1
#	end_date = 1565.1.1
#}

#Vassal of Vijayanagar
#vassal = {
#	first = VIJ
#	second = MAD
#	start_date = 1376.1.1
#	end_date = 1565.1.1
#}

#Vassal of Delhi
#vassal = {
#	first = DLH
#	second = SAM
#	start_date = 1249.1.1
#	end_date = 1526.4.21
#}

#Vassal of Mughal
#vassal = {
#	first = MUG
#	second = SAM
#	start_date = 1526.4.21
#	end_date = 1592.1.1
#}

#Vassal of afghan Durani
vassal = {
	first = DUR
	second = SND
	start_date = 1747.6.1
	end_date = 1815.1.1
}

#Bundelkhand overlord
protectorate = {
	first = GBR
	second = BND
	start_date = 1803.1.1
	end_date = 1946.1.1
}

#Vassal of Marathas
vassal = {
	first = MAR
	second = SND
	start_date = 1738.1.1
	end_date = 1818.6.3
}

#Bahawalpur
vassal = {
	first = MUG
	second = BHW
	start_date = 1690.1.1
	end_date = 1739.5.1
}
vassal = {
	first = DUR
	second = BHW
	start_date = 1752.1.1
	end_date = 1802.1.1
}
