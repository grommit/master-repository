owner = NOR
controller = NOR
add_core = NOR
culture = norwegian
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Kungah�lla"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1088.1.1 = { dock = yes shipyard = yes }
1133.1.1 = { mill = yes }
1337.1.1  = {
	fort_14th = yes
}
1515.1.1 = { training_fields = yes }
1522.2.15 = { dock = yes shipyard = yes }
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1529.12.17 = { merchant_guild = yes }
1536.1.1  = { religion = protestant } #Unknown date
1612.1.1  = {
	capital = "Kung�lv"
}
1658.2.26  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = DAN
} #The Peace of Roskilde
