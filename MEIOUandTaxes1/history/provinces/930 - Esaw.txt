# No previous file for Esaw

culture = woccon
religion = totemism
capital = "Esaw"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1 
native_hostileness = 6

1673.1.1  = { discovered_by = ENG } # James Needham and Gabriel Arthur
1707.5.12 = { discovered_by = GBR } 
1755.1.1 = {	owner = GBR
		controller = GBR
		citysize = 500
		trade_goods = tobacco
		culture = english
		capital = "Charlotte"
		religion = protestant }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
	    } # Declaration of independence
1782.11.1 = { unrest = 0 } # Preliminary articles of peace, the British recognized American independence
