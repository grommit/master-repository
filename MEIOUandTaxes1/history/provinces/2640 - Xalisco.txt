# 2406 - Xalixko

owner = XAL
controller = XAL
add_core = XAL
culture = tecos
religion = nahuatl
capital = "Xalixko"
base_tax = 9
base_production = 9
base_manpower = 6
is_city = yes
trade_goods = maize
discovered_by = mesoamerican
hre = no


# 1530.1.1 = {
#	owner = AUT
#	controller = AUT
# }
1530.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	religion = catholic
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
	regional_capital_building = yes
}
1531.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 2000
	base_tax = 4
base_production = 4
#	base_manpower = 1.0
	base_manpower = 2.0
	religion = catholic
	culture  = castillian
} # Francisco V�zquez de Coronado y Luj�n
1600.1.1   = {
	citysize = 3000
}
1608.1.1   = {
	add_core = SPA
	citysize = 1000
}
1650.1.1   = {
	citysize = 7500
}
1700.1.1   = {
	citysize = 15000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 30000
}
1800.1.1   = {
	citysize = 60000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
