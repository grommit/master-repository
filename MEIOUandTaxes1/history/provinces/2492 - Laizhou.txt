# 2492 - Shantong Laizhou

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Yexian"
trade_goods = silk
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes

discovered_by = chinese
discovered_by = steppestech

#1111.1.1 = { post_system = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1351.1.1  = {
	owner = QII
	controller = QII
    add_core = QII
}
1362.11.1  = {
	owner = YUA
	controller = YUA
}
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = QII
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1640.1.1 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
