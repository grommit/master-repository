# 1114- Cayor

owner = DJO
controller = DJO
culture = senegambian
religion = west_african_pagan_reformed
capital = "Tivaouane"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = CAY
	add_core = DJO
}
1445.1.1 = {
	discovered_by = POR
}
1549.1.1 = {
	owner = CAY
	controller = CAY
}
