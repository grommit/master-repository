# No previous file for Urzhar

owner = YUA
controller = YUA
add_core = YUA
culture = oirats
religion = tengri_pagan_reformed
capital = "Urzhar"
trade_goods = salt
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = steppestech

1392.1.1 = {
	owner = OIR
	controller = OIR
	add_core = OIR
	remove_core = YUA
}
1515.1.1 = { training_fields = yes }
1615.1.1 = { religion = vajrayana } # State religion
1635.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
	remove_core = OIR
}
1755.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
