# No previous file for Argyn

owner = BLU
controller = BLU
add_core = BLU
capital = "Argyn"
culture = khazak
religion = sunni
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1356.1.1   = {
	add_core = SIB
	add_core = NOG
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
	remove_core = NOG
}
1465.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
	remove_core = SIB
	remove_core = SHY
}
1515.1.1 = { training_fields = yes }
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = KZH controller = KZH }
