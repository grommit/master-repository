# 832 - Karaguaje

owner = CBS
controller = CBS
add_core = CBS
is_city = yes
culture = muisca
religion = aztec_reformed
capital = "Karaguaje"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1.0
native_size = 20
native_ferocity = 1
native_hostileness = 2


1530.1.1   = {
	
}
1539.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 1000
	capital = "Santa F� de Bogot�"
	culture = castillian
	religion = catholic
	trade_goods = coffee
	base_tax = 7
	set_province_flag = trade_good_set
}
1700.1.1   = {
	citysize = 5000
}
1750.1.1   = {
	citysize = 13420
	add_core = COL
	culture = colombian
}
1800.1.1   = {
	citysize = 24100
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
