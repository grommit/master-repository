# 1556 - Bauchi

owner = JUK
controller = JUK
culture = jukun		
religion = west_african_pagan_reformed		 
capital = "Bauchi"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = livestock

discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = JUK
}
1807.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
