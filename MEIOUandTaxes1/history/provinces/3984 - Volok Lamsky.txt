# 3984 - Volok Lamsky

owner = NOV
controller = NOV   
culture = russian
religion = orthodox
hre = no
base_tax = 3
base_production = 3
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Volok"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1345.1.1  = {
	owner = NOV
	controller = NOV   
	#add_core = SMO
	add_core = NOV
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1383.1.1  = {
	owner = NOV
	controller = NOV
} # Reverted to Novgorod shortly after Vladimir's defeat of Tokhtamysh in the province
1398.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
}
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
