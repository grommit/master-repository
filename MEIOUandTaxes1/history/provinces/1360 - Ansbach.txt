# 1360 - Ansbach

culture = eastfranconian
owner = ANS
controller = ANS
capital = "Ansbach"
religion = catholic
trade_goods = wheat
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
fort_14th = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

790.1.1 = {
	rename_capital = "Onoltesbach" 
	change_province_name = "Onoltesbach"
} # Onold's Brook in Old High German
837.1.1 = {
	rename_capital = "Onoltespah" 
	change_province_name = "Onoltespah"
}
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1141.1.1 = {
	rename_capital = "Onoldesbach" 
	change_province_name = "Onoldesbach"
}
1200.1.1 = { road_network = yes }
1230.1.1 = {
	rename_capital = "Onoldsbach" 
	change_province_name = "Onoldsbach"
}
1338.1.1 = {
	rename_capital = "Onelspach" 
	change_province_name = "Onelspach"
}
1356.1.1   = {
	add_core = ANS
}
1500.1.1 = { road_network = yes }
1508.1.1 = {
	rename_capital = "Onsbach" 
	change_province_name = "Onsbach"
}
1519.1.1   = {
	religion = protestant
	fort_14th = no fort_15th = yes
} # After attacking the free town of Reutlingen the Duke of Würtemberg is sent fleeing and the country governed by  the Austrians.

1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1732.1.1 = {
	rename_capital = "Ansbach" 
	change_province_name = "Ansbach"
}
1796.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
}
1805.12.15 = {
	controller = FRA
	add_core = FRA
	owner = FRA
} # First Treaty of Schönburg
1806.7.12  = {
	hre = no
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = PRU
	remove_core = FRA
}
