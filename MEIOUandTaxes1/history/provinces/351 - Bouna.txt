# 351 - Benzart

owner = HAF
controller = HAF
add_core = HAF 
culture = tunisian
religion = sunni
capital = "Benzart"
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
discovered_by = eastern
hre = no

900.1.1    = { set_province_flag = barbary_port }
1088.1.1 = { dock = yes shipyard = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	add_claim = TUR
}
1535.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
} # Conquered by the troops of the emperor Charles V
1574.9.13 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = ALG
	remove_core = HAF
	remove_core = SPA
}  # Established direct control of the province 
1585.1.1 = {
	remove_core = SPA
}
1671.1.1 = {
	owner = ALG
	controller = ALG
	remove_core = TUR
} # Virtually independent of the Ottoman empire
1852.1.1 = {
	owner = TUN
	controller = TUN
}
1881.5.12 = {
	owner = FRA
	controller = REB
}
1881.10.28 = { controller = FRA }
