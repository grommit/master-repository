#677 - Macau
# LS - Chinese Civil War

owner = YUA
controller = YUA
culture = yueyu
religion = confucianism
capital = "Macau"
trade_goods = fish
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
discovered_by = chinese
discovered_by = muslim
discovered_by = steppestech
discovered_by = indian

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
}
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	add_core = YUE
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1356.1.1 = {
	owner = YUE
	controller = YUE
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUE
	remove_core = YUA
	marketplace = yes
}
1513.1.1 = {
 	discovered_by = POR
}
1535.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
}
1547.1.1 = {
	
		base_tax = 5
base_production = 5
}
1587.1.1 = {
	trade_goods = silk
}
1605.1.1 = {
	fort_14th = yes
}
1638.1.1 = {
	
} #guiafortress
1641.1.1 = {
	
}
1650.1.1 = {
	
}
1662.1.1 = {
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty
1760.1.1 = {  }
