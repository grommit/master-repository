# 2123 - Gidgigannup

culture = aboriginal
religion = polynesian_religion
trade_goods = unknown #grain
capital = "Gidgigannup"
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 1

1625.1.1 = {
	discovered_by = NED
}
# Future Perth
