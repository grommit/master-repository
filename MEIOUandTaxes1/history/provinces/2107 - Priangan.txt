# 2107 - Priangan

owner = GLH
controller = GLH
culture = sundanese
religion = hinduism
capital = "Bandung"
trade_goods = gold
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1   = {
	add_core = GLH
	add_core = CBT
}
1478.1.1   = {
	owner = SUN
	controller = SUN
	religion = sunni
	add_core = SUN
	discovered_by = DEM
}  # Sunda and Galuh merge
1512.1.1   = { discovered_by = POR }	#FB Antonio de Abreu
1515.2.1 = { training_fields = yes }
1579.1.1  = {
	owner = CBT
	controller = CBT
	religion = sunni
	remove_core = SUN
}  # Cirebon sultanate expands
1599.1.1   = { discovered_by = NED }
1617.1.1   = {
	owner = MTR
	controller = MTR
	add_core = MTR
} # The Sultanate of Mataram
1741.7.1   = { unrest = 1 } #FB Mataram joins forces with Chinese - revolt becomes war
1743.11.1  = {
	owner = NED
	controller = NED
	unrest = 1
}
1800.1.1   = { add_core = NED unrest = 0 }
1811.9.1   = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1   = { owner = NED controller = NED remove_core = GBR } # Returned to the Dutch
