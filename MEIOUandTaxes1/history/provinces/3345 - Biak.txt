# 3345 - Biak

culture = papuan
religion = polynesian_religion
capital = "Biak"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 60
native_ferocity = 4
native_hostileness = 8

1000.1.1   = {
	set_province_flag = papuan_natives
}
1526.1.1 = {
	discovered_by = POR
} # Don Jorge de Mneses
1545.1.1 = {
	discovered_by = SPA
} # Y�igo Ortiz de Retez
