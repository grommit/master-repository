# 536 - Tiruelveli

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Nellai"
trade_goods = pepper
hre = no
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim


	
1000.1.1   = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1250.1.1 = { temple = yes }
1356.1.1 = { add_core = MAD }
1478.1.1 = {
	owner = VIJ
	controller = VIJ
}
1505.1.1 = {  discovered_by = POR }
1528.1.1 = { add_core = VIJ }
1530.1.1 = {
	#owner = MAD
	#controller = MAD
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
	road_network = yes
}
1565.7.1 = {
	owner = MAD
	controller = MAD
} # The Vijayanagar empire collapses, the Nayaks proclaimed themselves rulers
1600.1.1 = { discovered_by = ENG discovered_by = FRA discovered_by = NED }
1660.1.1 = { unrest = 8 } # Madurai was invaded by Maratha & Mysorne resulting in chaos
1689.1.1 = { unrest = 0 } # The rule of Rani Mangammal stabilized the situation.
1707.5.12 = { discovered_by = GBR }
1736.4.1 = {
	owner = KRK
	controller = KRK
	add_core = KRK
}
1741.1.1 = {
	controller = MAR
} #Marathas
1743.1.1 = {
	controller = KRK
}
1799.1.1 = {
	owner = GBR
	controller = GBR
}
1849.1.1 = { add_core = GBR }
