# 94 - Letzeburg

owner = LUX
controller = LUX
add_core = LUX
culture = moselfranconian
religion = catholic 
capital = "L�tzebuerg"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
fort_14th = yes
trade_goods = iron
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1300.1.1 = { road_network = yes }
1443.8.1   = { owner = BUR controller = BUR add_core = BUR }
1477.1.5  = { unrest = 10 } # death of Charles the Bold
1477.8.18 = { unrest = 0 } # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27  = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Mary of burgondy dies, Lowlands to Austria

#1500.1.1 = { road_network = yes }
1530.1.1  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = BUR
}
1540.1.1   = { fort_14th = yes }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1559.5.12  = { unrest = 3 } # New bishoprics in the Lowlands create an outrage
1567.9.10  = { unrest = 4 } # Counts of Egmont & Hoorne arrested
1568.6.5   = { unrest = 6 } # Counts of Egmont & Hoorne beheaded
1569.1.1   = { unrest = 8 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 9 } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1577.2.12  = { unrest = 5 } # 'Perpetual Edict' (Eeuwig Edict) accepted by Don Juan
1579.1.6   = { unrest = 0 } # Union of Arras established


1650.1.1   = { fort_14th = no fort_15th = yes }
1659.7.1   = { controller = FRA } # Mar�chal Turenne takes control of Luxembourg
1659.10.28 = { controller = SPA } # Peace of the Pyrennees
1665.9.17  = { add_claim = FRA } # Louis XIV's father-in-law, Philip IV of Spain, dies
1670.1.1   = { fort_15th = no fort_16th = yes }
1672.5.5   = { controller = FRA } # France blitzes through the Lowlands in the Franco-Dutch War
1678.9.19  = { controller = SPA } # Peace of Nijmegen (France-Spain)
1684.1.1   = { owner = FRA controller = FRA } # Louis XIV annexes Luxembourg
1687.1.1   = { fort_16th = no fort_17th = yes } # Vauban expands the fortress
1697.9.20  = { owner = SPA controller = SPA } # Peace of Rijswijck
1713.4.11  = { owner = HAB controller = HAB add_core = HAB remove_core = SPA } # Treaty of Utrecht

1792.12.1  = { controller = FRA } # Conquered by the French troops under Dumouriez
1793.3.18  = { controller = HAB } # Reconquered by Prince Josias of Coburg
1797.10.17 = {
	owner = FRA
	controller = FRA
	remove_core = HAB
} # The Treaty of Campo Formio, Luxembourg is ceded to France 
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9   = {
	owner = LUX
	controller = LUX
	remove_core = FRA
} # Grand Duchy of Luxembourg
