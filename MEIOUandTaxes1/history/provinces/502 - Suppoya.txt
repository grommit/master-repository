# 502 - Suppoya

culture = carib
religion = pantheism
capital = "Suppoya"
trade_goods = unknown
hre = no 
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 25
native_ferocity = 2
native_hostileness = 9 

1498.7.31 = {
	discovered_by = CAS
	owner = CAS
	controller = CAS
	culture = castillian
	religion = catholic
	citysize = 540
	capital = "Trinidad"
	trade_goods = sugar
	set_province_flag = trade_good_set
} # Christopher Columbus
1516.1.23 = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	citysize = 1105
}
1523.1.1 =  {
	add_core = SPA
}
1550.1.1  = {
	citysize = 1270
}
1600.1.1  = {
	citysize = 1690
}
1650.1.1  = {
	citysize = 2270
}
1680.1.1  = {
	capital = "Port-of-Spain"
}
1690.1.1  = {
	unrest = 5
} # Religious convertion of the natives
1699.1.1  = {
	controller = REB
} # Arena Massacre, Indian uprising
1700.1.1  = {
	controller = SPA
	unrest = 0
	citysize = 2340
}
1750.1.1  = {
	citysize = 2580
}
1797.1.1  = {
	controller = GBR 
	owner = GBR
	remove_core = SPA
	culture = english
	religion = protestant #anglican
} # Remained a British colony until 1962
1800.1.1  = {
	citysize = 3100
} # "True colonization", designed to attract immigrants.
1802.3.25  = {
	owner = GBR
	add_core = GBR
	remove_core = SPA
} # Treaty of Amiens
