# Wayuu

culture = guajiro
religion = pantheism
capital = "Wayuu"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 1

1498.1.1   = {
	discovered_by = CAS
}
1530.1.1   = {
	owner = SPA
	controller = SPA
	change_province_name = "Riohacha"
	rename_capital = "Rio de la Hacha"
	culture = castillian
	religion = catholic
	is_city = yes
	trade_goods = gems # Pearls
	set_province_flag = trade_good_set
}
1585.1.1   = {
	add_core = SPA
}
1650.1.1   = {
	citysize = 1750
}
1700.1.1   = {
	citysize = 2246
}
1750.1.1   = {
	citysize = 3150
	add_core = COL
	culture = colombian
}
1800.1.1   = {
	citysize = 4300
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
