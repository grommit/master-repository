# 48 - Uckermark

owner = BRA
controller = BRA
culture = pommeranian
religion = catholic
hre = yes
base_tax = 3
base_production = 3
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Prenzlau"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = PST
	add_core = BRA
	add_core = MKL
}
1472.7.30  = {
	remove_core = PST
	remove_core = MKL
}

1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1529.1.1   = { fort_14th = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1534.1.1   = { religion = protestant }


1630.1.1   = {
	religion = reformed
}# Devastating population losses in Thirty Years War, arrival of Huguenots
1701.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
	fort_14th = no fort_15th = yes
}
1750.1.1   = {  }
1775.1.1   = { fort_15th = no fort_16th = yes }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
