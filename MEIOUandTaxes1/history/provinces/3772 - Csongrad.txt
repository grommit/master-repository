# No previous file for Csongrad

owner = HUN
controller = HUN  
culture = hungarian
religion = catholic
capital = "Szeged"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1526.8.30  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
}
1530.1.4  = {
	bailiff = yes	
}
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
