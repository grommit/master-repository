# 447 - Herat

owner = KTD
controller = KTD
culture = persian
religion = sunni
capital = "Herat"
trade_goods = silk
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = KTD
	add_core = TIM
	fort_14th = yes
}
1383.1.1   = {
	owner = TIM
	controller = TIM
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	remove_core = KAB
	add_core = DUR
} # Shaybanids break free from the Timurids
1458.1.1  = {
	controller = QAR
	revolt = { }
} #Conquered by Black Sheep
1459.1.1  = {
	controller = KTD
} #Civil War
1460.1.1  = { unrest = 5 } # Besieged by Timurid Transox rebels
1465.1.1  = { unrest = 0 } # Estimated
1507.1.1  = {
	controller = SHY
}
1507.7.1  = {
	owner = SHY
}
1510.1.1  = {
	controller = SAM
}
1511.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	courthouse = yes
} # Safawids "form persia"
1515.1.1 = { training_fields = yes }
1530.1.1 = { add_claim = BUK }
#1524.1.1  = {
#	controller = REB
#	revolt = { type = pretender_rebels }
#} #Qizilbash Civil War
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1533.1.1  = {
	controller = PER
	revolt = { }
} #Shah triumphs
1581.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels }
} #Secession of Khurasan (failed)
1583.1.1  = {
	controller = PER
	revolt = { }
} #Secession of Khurasan (failed)
1585.1.1  = {
	controller = SHY
} #Uzbeks
1590.1.1  = {
	controller = PER
} #Returned
1677.1.1 = { discovered_by = FRA }
1709.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels size = 3 }
} # Widespread tribal uprisings
1711.1.1  = {
	owner = KAB
	controller = KAB
	revolt = { }
}
1738.1.1  = {
	owner = PER
	controller = PER
} # Nader Shah absorbs Afghanistan
1740.1.1 = { culture = pashtun } #Settled by Afghans
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
} # Ahmad Shah established the Durrani empire
