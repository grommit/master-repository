# No previous file for Marca Firmana

owner = FRM
controller = FRM
culture = umbrian 
religion = catholic 
capital = "Fermo" 
base_tax = 7
base_production = 7   
base_manpower = 2
is_city = yes    
trade_goods = wine
fort_14th = yes 
		# Completed 1135
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = FRM
}
1530.1.1 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1550.1.1   = {
	owner = PAP
	controller = PAP
	add_core = PAP
} # Camerino falls under direct control of the Papal administration
1805.3.17 = { owner = ITE controller = ITE add_core = ITE } # Treaty of Pressburg
1814.4.11 = { owner = PAP controller = PAP remove_core = ITE } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
