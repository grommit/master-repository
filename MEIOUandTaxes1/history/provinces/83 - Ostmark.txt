# 83 - Ostmark 
# Wien St-polten

owner = HAB
controller = HAB
add_core = HAB
culture = austrian
religion = catholic
capital = "Wien" 
base_tax = 9
base_production = 9
base_manpower = 7
is_city = yes
trade_goods = wine 

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { marketplace = yes }

1230.1.1 = { royal_palace = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1365.1.1 = { medieval_university = yes }
1469.1.1 = {  }
 # Stephansdom becomes Cathedral
1485.1.1  = {	owner = HUN
		controller = HUN
		add_core = HUN
	    } #Mathias Corvinus
1490.1.1  = { owner = HAB controller = HAB remove_core = HUN }
#1500.1.1 = { road_network = yes }


1515.1.1 = { training_fields = yes }
1530.1.1  = { fort_15th = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	city_watch = yes
	add_permanent_province_modifier = {
		name = walls_of_vienna
		duration = -1
	}
}


1599.1.1  = { fort_15th = no fort_17th = yes  }
1620.1.1  = {  }
1650.1.1  = { fort_17th = no fort_18th = yes }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
