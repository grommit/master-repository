# 3419 - Yauyo

owner = CHC
controller = CHC
add_core = CHC
culture = quechuan
religion = inti
capital = "Yauyo"
trade_goods = maize
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = south_american

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1356.1.1 = {
}
1471.1.1  =  {
	owner = CZC
	controller = CZC
	add_core = CZC
}
1480.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	remove_core = CZC
	paved_road_network = yes
	bailiff = yes
	constable = yes
	marketplace = yes
	}
1529.1.1 = {
	owner = QUI
	controller = QUI
	add_core = QUI
	add_core = CZC
	remove_core = INC
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
	unrest = 8
}
1750.1.1  = {
	add_core = PEU
	culture = peruvian
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
