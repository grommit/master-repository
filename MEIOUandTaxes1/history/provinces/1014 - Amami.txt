# 1014 - Hokuzan (Northern Mountain)
# GG/LS - Japanese Civil War

owner = RYU
controller = RYU
culture = ryukyuan
religion = animism
capital = "Amami"
trade_goods = chinaware
hre = no
base_tax = 3
base_production = 3
base_manpower = 2.0
citysize = 3000
add_core = RYU

discovered_by = chinese

1356.1.1 = {
}
1450.1.1 = { citysize = 3100 }
1500.1.1 = { citysize = 3500 }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 3400 }
1600.1.1 = { citysize = 4300 }

1609.1.1 = { controller = SMZ } # occupation de Satsuma, King Sho Nei was taken prisoner
1611.1.1 = { controller = RYU } # Sho Nei is released
1624.1.1 = { 
	owner = SMZ
	controller = SMZ
	religion = animism 
} # Annexion par les Shimazu de Satsuma
1650.1.1 = {
	citysize = 4900
	owner = JAP
	controller = JAP
	add_core = JAP
}
1700.1.1 = { citysize = 5100 }
1750.1.1 = { citysize = 5700 }
1800.1.1 = { citysize = 6600 }
