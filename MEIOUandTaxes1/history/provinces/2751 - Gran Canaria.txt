# 2751 - Maxorata

owner = CNA
controller = CNA
culture = guanche
religion = animism
capital = "Tamar�n"
trade_goods = wheat
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = CNA
discovered_by = western
discovered_by = FEZ
discovered_by = TLE
discovered_by = MOR

1356.1.1 = {
	add_core = CNA
}
1402.1.1 = {
	add_core = CAS
}#Jean de Bethencourt vassal of castille conquier the islands
1405.1.1  = {
	owner = CAS
	controller = CAS
	culture = castillian # culture = canarian
	religion = catholic
	rename_capital = "Las Palmas de Gran Canaria" 
	change_province_name = "Gran Canaria"
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
