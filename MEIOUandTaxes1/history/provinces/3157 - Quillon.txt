# No previous file for Quillon

owner = TRV
controller = TRV
culture = malayalam
religion = hinduism
capital = "Quillon"
trade_goods = pepper
hre = no
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim

1356.1.1  = { add_core = TRV }
1405.1.1  = { discovered_by = chinese }
1498.5.20 = { discovered_by = POR }
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1565.7.1  = {
	owner = MAD
	controller = MAD
	add_core = MAD
	unrest = 4
}
1570.1.1  = { unrest = 0 }
1600.1.1  = { discovered_by = ENG discovered_by = FRA discovered_by = NED }
1662.1.1  = {
	controller = NED
	owner = NED
}
1729.1.1  = {
	owner = TRV
	controller = TRV
}
