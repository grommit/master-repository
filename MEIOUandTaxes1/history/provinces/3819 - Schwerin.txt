# 3819 - Schwerin

owner = SHW
controller = SHW
add_core = SHW
culture = pommeranian
religion = catholic
hre = yes
base_tax = 3
base_production = 3
trade_goods = wheat
base_manpower = 2
is_city = yes
fort_14th = yes
capital = "B�tzow"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = { mill = yes }
1500.1.1 = { road_network = yes }
1530.1.1   = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1648.5.15  = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
