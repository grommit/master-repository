# 1018 - Hizen
# GG/LS - Japanese Civil War

owner = SHN
controller = SHN
culture = kyushu
religion = mahayana #shinbutsu
capital = "Saga"
trade_goods = fish
hre = no
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes

discovered_by = chinese

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1200.1.1 = { marketplace = yes }
1356.1.1 = {
	add_core = SHN
	add_core = RZJ
}
1542.1.1   = { discovered_by = POR }
1553.1.1   = {
	owner = RZJ
	controller = RZJ
	add_core = RZJ
}
1585.1.1   = {
	owner = SHN
	controller = SHN
	religion = catholic
}
1590.1.1   = {
	owner = SMZ
	controller = SMZ
	add_core = SMZ
}
1615.1.1   = {
	religion = mahayana #shinbutsu
}
1630.1.1 = {
	
}
1650.1.1   = {
	
	owner = JAP
	controller = JAP
	add_core = JAP
}
