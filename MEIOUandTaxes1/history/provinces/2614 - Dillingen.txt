# 2614 - Kempten

owner = AUH
controller = AUH
add_core = AUH
capital = "Dillingen"
trade_goods = wine
culture = schwabisch
religion = catholic
base_tax = 5
base_production = 5
base_manpower = 1
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1   = { fort_14th = yes }

1501.1.1 = { road_network = yes }
1530.1.1   = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1620.1.1   = {   fort_14th = yes }
1630.1.1   = {
	controller = SWE
}
1632.1.1   = {
	controller = AUH
}
#1685.1.1   = { citysize = 14000  base_manpower = 1.0}
1685.1.1   = {  base_manpower = 2.0}
1806.1.1   = {
	owner = BAV
	controller = BAV
	add_core = BAV
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
