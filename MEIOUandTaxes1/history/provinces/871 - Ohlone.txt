# No previous file for Ohlone

culture = yokut
religion = totemism
capital = "Ohlone"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 6

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1542.1.1 = { discovered_by = SPA } # Juan Rodriguez Cabrillo
1776.3.28 = {	owner = SPA
	controller = SPA
	citysize = 355
	trade_goods = wool 
} # Mission San Francisco de Assisi. 
1795.1.1 = {
	citysize = 1200
	culture = castillian
	religion = catholic
	add_core = SPA
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
