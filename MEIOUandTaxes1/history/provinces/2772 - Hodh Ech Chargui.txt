# 2772 - Walata

culture = tuareg
religion = sunni
capital = "Walata"
base_tax = 2
base_production = 2
base_manpower = 1
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = salt
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = muslim
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = TBK
}
1518.1.1   = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.4.12  = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SON
} # Battle of Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class
1670.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}
