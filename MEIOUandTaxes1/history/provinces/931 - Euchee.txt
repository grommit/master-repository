# No previous file for Euchee

culture = creek
religion = totemism
capital = "Euchee"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 2
native_hostileness = 5

1524.1.1   = {  } # Diego Gomez
1526.1.1 = {  } # Lucas V�squez de Ayll�n
1587.1.1   = {
	owner = SPA
	controller = SPA
	citysize = 150
	culture = castillian
	trade_goods = cotton
	religion = catholic
} #San Pedro de Mocama mission
1612.1.1   = {	add_core = SPA is_city = yes } 
1702.1.1   = {	owner = CRE
		controller = CRE
		add_core = CRE
		remove_core = SPA
		is_city = yes
		culture = creek
		religion = totemism } #Last Georgia missions abandoned. 
1814.8.9  = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
} #Treaty of Fort Jackson ending the Creek War
