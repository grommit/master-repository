# 1097 - Futuna

culture = polynesian
religion = animism
capital = "Futuna"
trade_goods = unknown #fish
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 2
native_hostileness = 9

1616.1.1 = {
	discovered_by = NED
} # Discovered by Willem Schouten
