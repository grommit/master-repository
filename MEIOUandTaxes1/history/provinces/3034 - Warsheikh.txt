# 3034 - Warsheikh

owner = MOG
controller = MOG
culture = somali
religion = sunni
capital = "Warsheikh"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african
hre = no

1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = MOG
}
1499.1.1 = {
	discovered_by = POR
} 
1550.1.1 = {
	discovered_by = TUR
}
1555.1.1 = {
	owner = AJU
	controller = AJU
	add_core = AJU
} #Northern part of province no longer conrolled by ADA
1650.1.1 = {
	owner = HOB
	controller = HOB
	add_core = HOB
	remove_core = AJU
}
1698.12.13 = {
	discovered_by = OMA
	owner = OMA
	controller = OMA
} #Omanis establish direct control on way to occupy Mombasa
1746.1.1  = {
	owner = ZAN
	controller = ZAN
} #Mazrui sultans establish dominance in region
1889.1.1 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
