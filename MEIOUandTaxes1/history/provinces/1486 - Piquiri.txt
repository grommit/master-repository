# 1486 - Piquiri

culture = guarani
religion = pantheism
capital = "Piquiri"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 1
native_hostileness = 2

1547.1.1   = {
	discovered_by = SPA
	add_core = SPA
}
1570.5.14  = {
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
	citysize = 500
	change_province_name = "Asunci�n"
	rename_capital = "Asunci�n"
	trade_goods = livestock
	set_province_flag = trade_good_set
}
1750.1.1   = {
	unrest = 2
   	add_core = PRG
	culture = platean
} # Spanish administration required all trade to pass via Lima, extracted taxes
1780.1.1   = {
	unrest = 4
} # The desire for independence grew
1790.1.1   = {
	unrest = 6
}
1811.5.17  = {
	owner = PRG
	controller = PRG
	unrest = 0
}
1816.7.9   = {
	remove_core = SPA
}
