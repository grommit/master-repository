# No previous file for Peskotomuhkat

culture = abenaki
religion = totemism
capital = "Peskotomuhkat"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1534.1.1  = { discovered_by = FRA } # Jacques Cartier
1629.1.1  = { discovered_by = ENG }
1631.1.1  = {	owner = FRA
		controller = FRA
		citysize = 300
		trade_goods = fish
		culture = francien
		religion = catholic
	    } # Construction of Fort la Tour

1656.1.1  = { add_core = FRA }
1692.1.1  = { fort_14th = yes } # Fort Nashwaak
1707.5.12 = { discovered_by = GBR } 
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	    } # Treaty of Paris
1785.4.25 = { capital = "Fredericton" }
1788.2.10 = { add_core = GBR }
