#1055 - Olyokma

culture = evenki
religion = tengri_pagan_reformed
capital = "Olyokma"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3
discovered_by = steppestech

1426.1.1 = {
	discovered_by = SAK
	owner = SAK
	controller = SAK
	add_core = SAK
	is_city = yes
	trade_goods = fur
	culture = yakut
}
1461.1.1 = {
	citysize = 0
	native_size = 50
	native_ferocity = 4.5
	native_hostileness = 9 
	owner = XXX
	controller = XXX
	remove_core = SAK
	culture = evenki
	trade_goods = unknown
	base_tax = 1
	base_production = 1
	base_manpower = 1
}
1632.1.1  = { discovered_by = RUS }
1632.9.25 = {
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
	base_tax = 2
	base_production = 2
	trade_goods = fur
	capital = "Mukhtuya"
}
1657.1.1 = {
	add_core = RUS
	is_city = yes
}
