# 2998 - Kambooni

owner = PTE
controller = PTE
culture = kiunguja
religion = sunni
capital = "Kambooni"
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes
trade_goods = gems
hre = no
discovered_by = east_african
discovered_by = muslim
discovered_by = indian

1100.1.1 = { marketplace = yes }
1356.1.1 = {
	add_core = PTE
}
1488.1.1 = { discovered_by = POR } #P�ro da Covilh� 
#1505.1.1 - Portuguese Tradepost (Portuguese had good relations with Malindi sultan)
1550.1.1  = {
	base_tax = 7
base_production = 7
} #Growth in wake of good relations Malindi-Portugal
1578.1.1  = {
	unrest = 5
} #Turkish Raids Destabilize Region
1585.1.1  = {
	unrest = 7
} #Mir Ali Bey's Raids destabilize region
1589.1.1  = {
	unrest = 7
} #Zimba Raids destabilize region
1590.1.1  = {
	unrest = 0
} #Zimba defeated
1600.1.1  = {
	discovered_by = TUR
} #Malindi declines as Portugal recenters trade to Mombasa
