# 1272 - Hont-Nográ

owner = HUN
controller = HUN  
culture = slovak
religion = catholic
capital = "Banska Stiawnica"
trade_goods = gold
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
	add_permanent_province_modifier = {
		name = kormocbanya_mines
		duration = -1
	}
}
1506.1.1   = {
	controller = REB
} # Szekely rebellion
1507.1.1   = {
	controller = HUN
} # Estimated
1514.4.1   = {
	controller = REB
} # Peasant rebellion against Hungary's magnates
1514.9.1 = { training_fields = yes }
1515.1.1   = {
	controller = HUN
} # Estimated
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1529.10.7 = { bailiff = yes }
1530.1.1 = { add_claim = TUR }
1541.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HAB
} # End of Ottoman-Habsburg War
1595.1.1   = {
	temple = no
	marketplace = no
	owner = HAB
	controller = HAB
} # After a devastating siege, the city is freed
1605.1.1   = {
	owner = TUR
	controller = TUR
}
1683.1.1 = {
	owner = HAB
	controller = HAB
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
