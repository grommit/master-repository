# 2947 - Logone

owner = KBO
controller = KBO
culture = kanouri		
religion = animism		 
capital = "Logone"
base_tax = 3
base_production = 3
base_manpower = 2
citysize = 5000
trade_goods = ivory
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
	add_core = KBO
	add_core = YAO
}
1395.1.1   = {
	owner = YAO
	controller = YAO
}
1505.1.1 = {
	owner = KBO
	controller = KBO
	remove_core = YAO
}
