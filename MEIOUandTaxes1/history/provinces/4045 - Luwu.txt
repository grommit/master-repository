# 4045 - Luwu

owner = BNE
controller = BNE
culture = bugis
religion = polynesian_religion
capital = "Palapo"
trade_goods = iron
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = MKS
discovered_by = MPH
discovered_by = MTR
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = sulawesi_natives
}
1356.1.1 = {
	add_core = MKS
	add_core = BNE
}
1511.1.1 = { discovered_by = POR }
1608.1.1 = {
	religion = sunni
	owner = MKS
	controller = MKS
	base_tax = 2
	base_production = 2
	base_manpower = 3.0
	trade_goods = iron
	set_province_flag = trade_good_set
}
1667.1.1 = {
	owner = NED
	controller = NED
	add_core = NED
	citysize = 3000
} # controlled by Dutch-controlled Makassar
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = {
	owner = NED
	controller = NED
} # Returned to the Dutch
