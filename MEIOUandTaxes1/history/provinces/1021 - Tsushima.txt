# 1021 - Tsushima
# GG/LS - Japanese Civil War

owner = SOO
controller = SOO
culture = kyushu
religion = mahayana #shinbutsu
capital = "Izuhara"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = SOO
}
1542.1.1 = { discovered_by = POR }
1590.1.1 = {
	
} # Preparation of the invasion of Korea
1600.1.1 = {
	
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
1760.1.1 = {  }
