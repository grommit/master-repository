# 2587 - Wa'ab

culture = polynesian
religion = polynesian_religion
capital = "Wa'ab"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 0.5
native_hostileness = 2

1526.1.1 = {
	discovered_by = SPA
} # Discovered by Toribio Alonso de Salazar
1527.1.1 = {
	discovered_by = POR
} # Visited by Diego da Rocha
1686.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 100
	trade_goods = fish
	set_province_flag = trade_good_set
}
