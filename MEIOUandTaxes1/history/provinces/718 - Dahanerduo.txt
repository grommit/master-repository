# 718 - Jalayr

owner = YUA
controller = YUA
add_core = YUA
culture = tumed
religion = tengri_pagan_reformed
capital = "Bugthot"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
citysize = 4070
discovered_by = steppestech
discovered_by = chinese

1133.1.1 = { mill = yes }
1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1435.1.1 = {
	owner = TMD
	controller = TMD
	add_core = TMD
} # Ongguds become an otog of Tumed
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1688.1.1 = {
	owner = ZUN
	controller = ZUN
}
1696.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Kangxi leads Qing army pushing Zunghars back
