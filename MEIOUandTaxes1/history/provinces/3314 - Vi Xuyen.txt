# No previous file for Vi Xuyen

owner = DAI
controller = DAI
culture = laotian		
religion = buddhism
capital = "Vi Xuyen"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = tea
discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
	add_core = DAI
	fort_14th = yes

}
1407.6.17 = { owner = MNG controller = MNG }
1427.1.1 = { owner = DAI controller = DAI }
1515.1.1 = { regimental_camp = yes }
1527.6.15 = { owner = VUU controller = VUU add_core = VUU }
1730.1.1 = { unrest = 5 } # Peasant revolt
1731.1.1 = { unrest = 0 }
1769.1.1 = { unrest = 6 } # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
	remove_core = VUU
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = { unrest = 5 } # Unsuccessful revolt
1787.1.1 = { unrest = 0 }
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = { 
	owner = FRA
	controller = FRA
}
