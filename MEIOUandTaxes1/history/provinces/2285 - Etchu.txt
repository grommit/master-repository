# 2285 - Etchu
# GG/LS - Japanese Civil War

owner = HKY
controller = HKY
culture = chubu
religion = mahayana #shinbutsu
capital = "Toyama"
trade_goods = rice
hre = no
base_tax = 7
base_production = 7
base_manpower = 6
is_city = yes

discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = HKY
}
1488.1.1 = { owner = IKK controller = IKK add_core = IKK } #Dei Gratia
1542.1.1 = { discovered_by = POR }
1576.1.1 = { owner = USG controller = USG remove_core = HKY remove_core = IKK } #Dei Gratia
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
