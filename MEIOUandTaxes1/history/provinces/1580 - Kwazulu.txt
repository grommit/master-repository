#1580 - Kwazulu

culture = zulu
religion = animism
capital = "Kraal"
trade_goods = unknown # slaves
native_size = 20
native_ferocity = 9
native_hostileness = 9
hre = no
discovered_by = QLM
discovered_by = SOF

1481.1.1 = { discovered_by = POR } # Bartolomeu Dias
