# 2248 - Josotu

owner = YUA
controller = YUA
culture = chahar
religion = tengri_pagan_reformed
capital = "Kaicheng"
trade_goods = salt
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA
}
1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1440.1.1 = {
	owner = CHH
	controller = CHH
	add_core = CHH
}
#1538.1.1 = { 
#	owner = CHH
#	controller = CHH
#	add_core = CHH
#	remove_core = YUA
#} # Uriankhai Tumen dispersed
1586.1.1 = { religion = vajrayana } # State religion
1624.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG } # The government in Taiwan surrendered
