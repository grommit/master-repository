# 467 - Al sir
# Bani Yas

owner = BYA
controller = BYA
culture = bahrani
religion = sunni
capital = "Abu Dabi"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1356.1.1 = {
	add_core = BYA
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1480.1.1 = { discovered_by = TUR }
1507.9.1 = { discovered_by = POR } # Alfonso d'Albuquerque
1818.9.9 = {
	owner = TUR
	controller = TUR
	add_core = TUR
} # The end of the Saudi State
