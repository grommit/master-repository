# 2817 - Nong Bua Lamphu

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Nong Bua Lamphu"
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1133.1.1 = { mill = yes }
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
	fort_14th = yes
} # Declared independent, Lan Xang was partitioned into 4 kingdoms; Vientiane, Champasak & Luang Prabang + Muang Phuan
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
