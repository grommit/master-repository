# 15 - �stjylland

owner = DEN
controller = DEN
add_core = DEN
culture = danish
religion = catholic
capital = "Arus"
base_tax = 4
base_production = 4
base_manpower = 5
is_city = yes
trade_goods = wheat
fort_14th = yes
estate = estate_nobles
discovered_by = western
discovered_by = eastern
discovered_by = muslim

#1088.1.1 = { dock = yes shipyard = yes }
1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.2.15 = { dock = yes shipyard = yes }

1523.6.21  = {
	base_tax = 5
	base_production = 5
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1526.1.1 = { religion = protestant } #preaching of Hans Tausen
1529.12.17 = { merchant_guild = yes }
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 2 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1  = { religion = protestant }
1644.1.25 = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13 = {
	controller = DAN
} #The Peace of Br�msebro
1661.1.1 = {
	fort_14th = no
}
1700.1.1  = {  }
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
