# 1568 - Kaffa

owner = SID
controller = SID
culture = sidamo 
religion = sunni
capital = "Hadiya"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
trade_goods = livestock
discovered_by = ETH
discovered_by = ADA
discovered_by = ALW
discovered_by = MKU
discovered_by = SID
discovered_by = muslim
discovered_by = east_african
hre = no

1356.1.1 = {
	add_core = SID
}
1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1515.2.1 = { training_fields = yes }
