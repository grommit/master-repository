# 401 - Al Ma'bar

owner = OMA
controller = OMA
culture = omani
religion = ibadi
capital = "Duqm"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1  = {
	add_core = OMA
}
1480.1.1  = { discovered_by = TUR }
1507.6.1  = {
	discovered_by = POR
	#owner = POR
	#controller = POR
} # Captured by the Portuguese
1515.1.1 = { training_fields = yes }
#1533.1.1  = { add_core = POR }
#1550.1.1  = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
#} # Occupied by the Ottomans
1551.1.1  = { owner = POR controller = POR }
1581.1.1  = { owner = TUR controller = TUR }
1588.1.1  = { owner = POR controller = POR }
1624.1.1  = {
	owner = OMA
	controller = OMA
	remove_core = TUR
	remove_core = POR
}
1630.1.1  = { fort_14th = yes }

1679.1.1  = { unrest = 5 } # Internal unrest upon Imam's death
1741.1.1  = {
	owner = PER
	controller = PER	
	add_core = PER
} # Invaded by Persia
1749.6.10 = {owner = OMA
	controller = OMA
	remove_core = PER
	unrest = 0
}
