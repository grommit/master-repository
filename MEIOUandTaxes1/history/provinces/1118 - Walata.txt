# 1118 - Walata

owner = MAL
controller = MAL
culture = bambara
religion = sunni
capital = "Walata"
base_tax = 4
base_production = 4
base_manpower = 1
is_city = yes
trade_goods = subsistence

discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = muslim
hre = no

1356.1.1  = {
	add_core = MAL
}
1433.1.1   = {
	owner = TBK
	controller = TBK
	add_core = TBK
}
1469.1.1  = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.3.15  = {
	owner = ZAF
	controller = ZAF
	add_core = ZAF
} #Collapse of Songhai in wake of Tondibi
1660.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}

1819.1.1   = {
	owner = MSN
	controller = MSN
	add_core = MSN
	remove_core = SEG
} # The Massina Empire
