# 663 - yunnan_area Panlong

owner = DLI
controller = DLI
add_core = DLI
culture = baizu
religion = mahayana
capital = "Kunming"
trade_goods = tin
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech 
fort_14th = yes


1200.1.1 = { paved_road_network = yes courthouse = yes }
1253.1.1 = {
	owner = YUA
	controller = YUA
}
1274.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
    remove_core = DLI
} #creation of liang viceroy
1356.1.1 = {
}
1382.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
} 
1500.1.1 = { citysize = 48204 }
1520.1.1 = { 
	culture = wuhan 
	religion = confucianism
}
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
}
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
