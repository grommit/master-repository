# 3368 - Barbastro

owner = ARA		
controller = ARA
add_core = ARA
culture = aragonese
religion = catholic
capital = "Barbastro" 
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

#1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille

1705.6.29  = { controller = HAB } # Aragon joins the Austrian side in the War of Spanish Succession
1707.5.26  = { controller = SPA } # Aragon surrenders
1713.7.13  = { remove_core = ARA }
1813.8.31  = { controller = REB }
1813.12.11 = { controller = SPA }
