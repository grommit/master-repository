# 2250 - Huguang Lengshuitan

owner = YUA
controller = YUA
culture = miao
religion = animism
capital = "Lingling"
trade_goods = lead
hre = no
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes

discovered_by = chinese
discovered_by = steppestech

1235.1.1  = {
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	medieval_university = yes
}
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
	bailiff = yes
}
#1643.11.1  = {
#	owner = DSH
#	controller = DSH
#	add_core = DSH
#}
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty

1645.3.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DSH
} # The Qing Dynasty
1662.1.1 = {
	remove_core = MNG
}
