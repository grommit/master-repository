# 2366 - Bailliage d'Aval

owner = BUR
controller = BUR
culture = burgundian
religion = catholic
capital = "Salins"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = salt
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1356.1.1   = {
	add_core = BUR
}
1361.11.21 = {
	owner = ARS
	controller = ARS
	add_core = ARS
}
1369.6.19  = {
	owner = BUR
	controller = BUR
	remove_core = ARS
}
1444.1.1 = { remove_core = FRA }
1477.1.6 = { add_core = FRA }
1482.3.27  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BUR
	#culture = bourguignon
} # Mary of burgondy dies, Lowlands to Austria
1530.1.1  = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = HAB
	#culture = bourguignon
	bailiff = yes
	remove_core = FRA
	hre = no
}
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1668.2.20  = { controller = FRA } # The Prince de Cond� swiftly takes Franche-Comt� in the War of Devolution
1668.5.2   = { controller = SPA } # Treaty of Aachen: Franche-Comt� returned to Spain
1670.1.1   = { add_core = FRA } # Louis XIV lays claims through the Chambres de R�union
1674.9.1   = { controller = FRA } # France captures Franche-Comt� 
1678.9.19  = {
	owner = FRA
	remove_core = SPA
} # Peace of Nijmegen (France-Spain)
