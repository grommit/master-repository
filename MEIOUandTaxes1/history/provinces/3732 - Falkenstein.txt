# 3732 - Falkenstein

owner = FAL
controller = FAL
capital = "Münzenberg"
trade_goods = wool
culture = hessian
religion = catholic
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = { road_network = yes }
1356.1.1   = { add_core = FAL }
1470.1.1   = {
	fort_15th = yes
}
1500.1.1 = { road_network = yes }
1535.1.1   = {
	owner = HES
	controller = HES
	add_core = HES
} # Inherited by Hesse
1548.1.1   = {
	fort_15th = no
	fort_16th = yes
}
1567.3.31  = {
	owner = HDA
	controller = HDA
	add_core = HDA
	remove_core = HES
}
1806.7.12  = {
	hre = no
	owner = HES
	controller = HES
	add_core = HES
	remove_core = HDA
} # The Holy Roman Empire is dissolved
1813.10.14 = {
	controller = HES
}
1866.1.1  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES
}
