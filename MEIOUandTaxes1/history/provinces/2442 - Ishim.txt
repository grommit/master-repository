# 2442 - Ishim

owner = BLU
controller = BLU
culture = khazak
religion = sunni
capital = "Ishim"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
citysize = 1270
discovered_by = steppestech

1356.1.1   = {
	add_core = BLU
	add_core = SIB
	add_core = KZH
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1465.1.1 = {
	owner = KZH
	controller = KZH
	remove_core = SIB
	remove_core = SHY
}
1515.1.1 = { training_fields = yes }
1550.1.1 = {
	discovered_by = TUR
}
1618.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
}
1752.1.1 = {
	capital = "Petropavl"
}
