# No previous file for Illubabor

culture = sidamo 
religion = animism
capital = "Agaro"
native_size = 10
native_ferocity = 2
native_hostileness = 2
trade_goods = unknown
discovered_by = KIT
discovered_by = ALW
discovered_by = ADA
discovered_by = muslim
discovered_by = east_african
hre = no
