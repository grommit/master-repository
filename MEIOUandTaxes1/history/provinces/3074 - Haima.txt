# 3074 - Haima

owner = YKA
controller = YKA
culture = hadhrami
religion = ibadi
capital = "Haima"
trade_goods = incense
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = indian

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = YKA
	add_core = HAD
}
1480.1.1 = { discovered_by = TUR }
1488.1.1 = { discovered_by = POR } # P�ro da Covilh�
1551.1.1 = { owner = HAD controller = HAD }
