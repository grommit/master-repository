# 1199 - Benadir

owner = MOG
controller = MOG
culture = somali
religion = sunni
capital = "Muqdisho"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = ivory

discovered_by = MOG
discovered_by = ADA
discovered_by = KIL
discovered_by = muslim
discovered_by = east_african
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "center_of_trade_modifier"
		duration = -1
	}
}
1088.1.1 = { dock = yes }
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = MOG
}
1499.1.1 = {
	discovered_by = POR
} 
1550.1.1 = {
	discovered_by = TUR
}
1555.1.1 = {
	owner = AJU
	controller = AJU
	add_core = AJU
} #Northern part of province no longer conrolled by ADA
1650.1.1 = {
	owner = MOG
	controller = MOG
	remove_core = AJU
}
1705.1.1 = {
	discovered_by = OMA
	owner = OMA
	controller = OMA
} #Omanis establish direct control on way to occupy Mombasa
1746.1.1  = {
	owner = ZAN
	controller = ZAN
} #Mazrui sultans establish dominance in region
1889.1.1 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
