# No previous file for Epekwitk

culture = miqmaq
religion = totemism
capital = "Epekwitk"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1 
native_hostileness = 6

1497.6.24  = {
	discovered_by = ENG
} # John Cabbot
1534.1.1   = {
	discovered_by = FRA
} # Jacques Cartier
1629.1.1   = {
	owner = FRA
	controller = FRA		
	citysize = 50
	religion = catholic
	culture = francien
	trade_goods = fur
	set_province_flag = trade_good_set
	} 
1650.1.1   = {
	add_core = FRA
}
1719.1.1   = {
	capital = "�le Saint Jean"
	citysize = 100
}
1750.1.1   = {
	add_core = QUE
	culture = canadian
}
1763.2.10  = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = FRA
	religion = protestant #anglican
	culture = american
}
