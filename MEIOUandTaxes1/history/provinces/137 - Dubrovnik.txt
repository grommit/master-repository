# 137 - Ragusa

owner = RAG
controller = RAG
culture = croatian
religion = catholic
capital = "Ragusa"
trade_goods = wax
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
fort_14th = yes


1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1088.1.1 = { dock = yes }
#1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
#	owner = VEN
#	controller = VEN
#	add_core = VEN
	add_core = RAG
#	culture = dalmatian
}
#1358.2.18  = {
#	owner = RAG
#	controller = RAG
#	culture = croatian
#} # Treaty of Zadar
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}

 # Ragusa reached its peak
1600.1.1  = { fort_14th = no fort_15th = yes }
 # Became the center of the "Dalmatian renaissance"

1650.1.1  = {  }
1690.1.1  = {  }

1806.5.26 = { controller = FRA } # Occupied by French troops
1806.6.17 = { controller = RAG } # The French are defeated
1806.7.12 = { controller = FRA owner = FRA add_core = FRA } # Treaty of Tilsit
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {
	owner = HAB
	add_core = HAB
	remove_core = FRA
} # Napoleon abdicates
