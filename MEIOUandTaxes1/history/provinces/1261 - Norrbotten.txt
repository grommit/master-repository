# 1261 - Kainuu

owner = SWE
controller = SWE
culture = finnish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = fur
base_manpower = 1
is_city = yes
capital = "Tornio"
discovered_by = western
discovered_by = eastern

1356.1.1   = {
	owner = RSW
	controller = RSW
	add_core = RSW
	add_core = SWE
}
1360.1.1   = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1527.6.1   = { religion = protestant}
1652.1.1 = { capital = "Kiruna" }
1740.1.1 = { fort_14th = yes }
1800.1.1 = { fort_14th = yes }
1809.3.29  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
}
