# 136 - Spalato

owner = CRO
controller = CRO
culture = croatian
religion = catholic
capital = "Spalatro"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = livestock
hre = no
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech


1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	owner = VEN
	controller = VEN
	add_core = VEN
	add_core = CRO
	culture = dalmatian
	add_core = HUN
}
1358.2.18  = {
	owner = CRO
	controller = CRO
	culture = croatian
} # Treaty of Zadar
1420.1.1   = {
	owner = VEN
	controller = VEN
	culture = dalmatian
}
1530.1.1 = { add_claim = TUR }
1530.1.4  = {
	bailiff = yes	
}
1540.10.2  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	culture = croatian
} # A large part of Dalmatia is incorporated into the Ottoman empire
1699.1.26  = {
	owner = VEN
	controller = VEN
	remove_core = TUR
	culture = dalmatian
} # Peace of Karlowitz, Dalmatian hinterland given to Venice
 # Intense economic and cultural growth
1750.1.1   = {  }
 # The economic production increased in almost every aspect
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	culture = croatian
} # Treaty of Campo Formio
1805.12.26 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = HAB
} # Treaty of Pressburg
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {
	owner = HAB
	add_core = HAB
	remove_core = FRA
} # Napoleon abdicates
