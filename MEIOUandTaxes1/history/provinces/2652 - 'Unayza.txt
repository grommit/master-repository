# 2652 - 'Unayza

owner = BTA
controller = BTA
add_core = BTA
culture = najdi
religion = sunni
capital = "'Unayza"
trade_goods = livestock
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = turkishtech

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1744.1.1 = {
	religion = wahhabi
	owner = NAJ
	controller = NAJ
	add_core = NAJ
}
1818.9.9 = {
	owner = TUR
	controller = TUR
} # The end of the Saudi State
