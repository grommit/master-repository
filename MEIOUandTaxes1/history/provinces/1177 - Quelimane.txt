# 1177 - Quelimane

owner = QLM
controller = QLM
culture = kimwani
religion = sunni
capital = "Quelimane"
base_tax = 8
base_production = 8
base_manpower = 4
is_city = yes
trade_goods = slaves
discovered_by = central_african
discovered_by = east_african
hre = no

1100.1.1 = { marketplace = yes }
1356.1.1  = {
	add_core = QLM
}
1498.3.16 = { discovered_by = POR } #Vasco Da Gama
1520.1.1  = {
	owner = POR
	controller = POR
	add_core = POR
	naval_arsenal = yes
	customs_house = yes 
}
1600.1.1  = { discovered_by = TUR }
1763.1.1  = { unrest = 7 }
