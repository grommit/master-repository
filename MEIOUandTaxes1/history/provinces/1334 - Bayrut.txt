# 1334 - Tyrus

owner = MAM
controller = MAM
culture = shami
religion = chaldean
capital = "Bayrut"
trade_goods = lumber
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
estate = estate_dhimmi
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1500.1.1 = { fort_14th = yes }
1516.1.1   = { add_core = TUR }
1516.8.28  = { controller = TUR }
1517.4.13  = {
	owner = TUR
	remove_core = MAM
	add_permanent_province_modifier = {
		name = "mount_lebanon_emirate_loyal"
		duration = -1
	}
} # Conquered by Ottoman troops
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1530.1.5 = {
	owner = SYR
	controller = SYR
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1591.1.1 = {
	remove_province_modifier = "mount_lebanon_emirate_loyal"
	add_permanent_province_modifier = {
		name = "mount_lebanon_emirate_disloyal"
		duration = -1
	}
} # Fakhr-al-Din II
1610.1.1 = {
	remove_province_modifier = "mount_lebanon_emirate_disloyal"
	add_permanent_province_modifier = {
		name = "mount_lebanon_emirate_rebellious"
		duration = -1
	}
} # Fakhr-al-Din II gets secret support of Ferdinand of Tuscany
1635.1.1 = {
	remove_province_modifier = "mount_lebanon_emirate_rebellious"
	add_permanent_province_modifier = {
		name = "mount_lebanon_emirate_disloyal"
		duration = -1
	}
} # Fakhr-al-Din II defeated by the Wali of Damascus
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
