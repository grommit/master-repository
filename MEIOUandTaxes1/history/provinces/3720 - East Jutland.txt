# 3720 - Vestjylland

owner = DEN
controller = DEN
add_core = DEN
culture = danish
religion = catholic
capital = "Wibi�rgh"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = livestock
discovered_by = western
discovered_by = eastern
discovered_by = muslim
1021.1.1 = { merchant_guild = yes }
#1088.1.1 = { dock = yes shipyard = yes }
1250.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.2.15 = { dock = yes shipyard = yes }
1523.6.21  = {
	base_tax = 4
	base_production = 4
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 2 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1  = { religion = protestant }
1644.1.25 = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13 = {
	controller = DAN
} #The Peace of Br�msebro
1700.1.1  = {  }
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
