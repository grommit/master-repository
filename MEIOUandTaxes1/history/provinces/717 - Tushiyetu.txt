#717 - Hovd

owner = YUA
controller = YUA
add_core = YUA 
culture = tumed
religion = tengri_pagan_reformed
capital = "T�shiyet�"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
citysize = 9400
discovered_by = steppestech

1392.1.1 = {
	owner = TMD
	controller = TMD
	add_core = TMD
	remove_core = YUA
}
1425.1.1 = {
	owner = OIR
	controller = OIR
	add_core = OIR
	remove_core = TMD
	culture = oirats
}
1480.1.1 = {
	owner = KHA
	controller = KHA
	remove_core = OIR
	add_core = KHA
	culture = khalkas
}
1515.1.1 = { training_fields = yes }
1586.1.1 = {
	religion = vajrayana
}
1688.1.1 = {
	owner = ZUN
	controller = ZUN
}
1696.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Kangxi leads Qing army pushing Zunghars back
