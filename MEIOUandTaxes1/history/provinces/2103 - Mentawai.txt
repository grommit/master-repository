# 2103 - Miberut

culture = sumatran
religion = hinduism		#later: sunni
capital = "Siberut"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 60
native_ferocity = 2
native_hostileness = 5
#citysize = 2500
#add_core = SGS
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian
