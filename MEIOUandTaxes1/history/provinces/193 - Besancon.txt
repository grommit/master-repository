# 193 - Bailliage d'Amont

owner = BUR
controller = BUR
culture = burgundian
religion = catholic
capital = "Vesoul"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
fort_14th = yes
trade_goods = wine
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = BUR
}
1361.11.21 = {
	owner = ARS
	controller = ARS
	add_core = ARS
}
1369.6.19  = {
	owner = BUR
	controller = BUR
	remove_core = ARS
}
1444.1.1 = { remove_core = FRA }
1477.1.6 = { add_core = FRA }
1482.3.27  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BUR
	#culture = bourguignon
} # Mary of burgondy dies, Lowlands to Austria
1530.1.1  = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = HAB
	#culture = bourguignon
	bailiff = yes
	remove_core = FRA
	hre = no
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1540.1.1   = { fort_14th = yes }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }

1640.1.1   = { fort_14th = no fort_15th = yes }

1668.2.20  = { controller = FRA } # The Prince de Cond� swiftly takes Franche-Comt� in the War of Devolution
1668.5.2   = { controller = SPA } # Treaty of Aachen: Franche-Comt� returned to Spain
1670.1.1   = { add_core = FRA } # Louis XIV lays claims through the Chambres de R�union
1674.9.1   = { controller = FRA } # France captures Franche-Comt� 
1678.9.19  = {
	owner = FRA
	remove_core = SPA
	hre = no
} # Peace of Nijmegen (France-Spain)
1685.1.1   = { fort_15th = no fort_16th = yes }
 # University of Dole transferred to Besan�on by Louis XIV
1740.1.1   = { fort_16th = no fort_17th = yes }
