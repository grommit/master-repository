# 711 - guangdong_area Shaozhou
# LS - Chinese Civil War

owner = YUA
controller = YUA
culture = yueyu
religion = confucianism
capital = "Gaoyao"
trade_goods = chinaware
hre = no
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes


discovered_by = chinese
discovered_by = muslim
discovered_by = steppestech
discovered_by = indian

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1279.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	add_core = YUE
}
1356.1.1 = {
	owner = YUE
	controller = YUE
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUE
	remove_core = YUA
}
1514.1.1 = { discovered_by = POR }
1529.3.17 = { 
	dock = yes
	marketplace = yes
	bailiff = yes
	courthouse = yes
	fine_arts_academy = yes
}
1662.1.1 = {
	owner = YUE
	controller = YUE
	add_core = YUE
	remove_core = MNG
}# Shang Kexi appointed as viceroy
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1680.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = YUE
}
1740.1.1 = {  }
