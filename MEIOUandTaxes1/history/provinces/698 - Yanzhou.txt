# 698 - Shantong Tai'an

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Qufu"
trade_goods = tea
hre = no
base_tax = 11
base_production = 11
base_manpower = 4
is_city = yes

discovered_by = chinese
discovered_by = steppestech

#1111.1.1 = { post_system = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
}
1522.1.1 = {
	fort_14th = yes #Jiajing Emperor builds city walls
}
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
	road_network = yes
	plantations = yes
}
1644.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
