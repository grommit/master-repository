# No previous file for Nihithawiwin

culture = cree
religion = totemism
capital = "Nihithawiwin"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 5

1692.1.1 = {	discovered_by = ENG } #Henry Kelsey
1707.5.12 = { discovered_by = GBR }
1790.1.1 = {	owner = GBR
		controller = GBR
		culture = english
		religion = protestant
		citysize = 200
		trade_goods = fur } #Split Lake House, later Norway House
1815.1.1 = { 	add_core = GBR }
