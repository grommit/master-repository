# 671 - Guanxi Jiangzhou

owner = YUA
controller = YUA
culture = zhuang
religion = confucianism
capital = "Chongshan"
trade_goods = rice
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
add_core = YUA
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech


0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1133.1.1 = { mill = yes }
1279.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1320.1.1 = { remove_core = SNG }
1325.1.1 = { capital = "Nanning" }
1356.1.1 = {
	
}
1382.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
    remove_core = LNG
	remove_core = YUA
}
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
	bailiff = yes
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty