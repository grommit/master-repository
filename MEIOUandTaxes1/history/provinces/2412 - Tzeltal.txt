# 2412 - Tzeltal

# owner = MAY
# controller = MAY
# add_core = MAY
culture = tzeltal
religion = mesoamerican_religion
capital = "Oxchuc"
#nahuatl : Huitzlan
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 20
native_ferocity = 1
native_hostileness = 8
trade_goods = maize 
discovered_by = mesoamerican
hre = no

1517.1.1   = {
	discovered_by = SPA
}
1530.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	is_city = yes
	
} #Pedro de Alvanado

1565.1.1   = {
	add_core = SPA
}
1590.1.1   = {
	religion = catholic
}
1600.1.1   = {
	citysize = 7500
}
1650.1.1   = {
	citysize = 10000
}
1700.1.1   = {
	citysize = 20000
}
1750.1.1   = {
	add_core = MEX
	citysize = 35000
}
1800.1.1   = {
	citysize = 75000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
