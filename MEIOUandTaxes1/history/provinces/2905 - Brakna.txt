# 2905 - Brakna

culture = fulani
religion = west_african_pagan_reformed
capital = "Brakna"
trade_goods = unknown
hre = no
native_size = 70
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan

1490.1.1 = {
	owner = FLO
	controller = FLO
	add_core = FLO
	base_manpower = 3
	is_city = yes
	trade_goods = slaves
}
