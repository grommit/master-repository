#89 - Picardie

owner = FRA
controller = FRA 
culture = picard
religion = catholic
capital = "Amiens"
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
trade_goods = wheat
 # Notre Dame d'Amiens
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

#1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = FRA
	add_core = FLA
}
1384.1.1   = { owner = BUR controller = BUR add_core = BUR }
1444.1.1 = { remove_core = FRA }
1477.1.5 = { add_core = FRA }
1482.3.27  = {
	owner = FRA
	controller = FRA
	remove_core = BUR
	#add_core = HAB
} # Charles the Bold dies and transfers Bourgogne to France
1513.8.16  = { controller = ENG } # Henry VIII defeats La Palice at Guinnegate and sacks Therouanne
1514.5.5   = { controller = FRA } # Henry VIII concludes a seperate peace with France

1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1520.1.1   = { fort_14th = yes }
1523.8.16 = { mill = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1544.7.5   = { controller = ENG } # English forces take hold of parts of Picardie in the Anglo-French War (1542-1546)
1546.8.1   = { controller = FRA } # Peace is concluded, back to status quo
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism

1628.1.1   = { unrest = 3 }
1630.1.1   = { unrest = 0 }
1638.1.1   = { unrest = 3 }
1640.1.1   = { unrest = 0 }
1642.1.1   = { unrest = 3 }
1644.1.1   = { unrest = 0 }
1650.1.1   = { fort_14th = no fort_15th = yes }


1740.1.1   = { fort_15th = no fort_16th = yes }

