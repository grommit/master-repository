# 1498 - Macina

owner = MAL
controller = MAL
culture = bambara
religion = west_african_pagan_reformed
capital = "Macina"
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
trade_goods = millet

discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = muslim
hre = no

1356.1.1   = { add_core = MAL }
1471.1.1   = { owner = SON controller = SON add_core = SON } #Conquered by Sunni Ali Ber of Songhai
1481.1.1   = { unrest = 3 } #Mossi Raid led by Nasere I devastates region
1591.3.15  = {
	owner = ZAF
	controller = ZAF
	add_core = ZAF
} #Collapse of Songhai in wake of Tondibi
1600.1.1   = { remove_core = MAL } #Collapse of Mali State
1642.1.1   = { remove_core = SON } #Collapse of last vestiges of unity among Songhai
1660.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}
1685.1.1   = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } #Bambara decline after death of Kaniadan Kulibali
1720.1.1   = { revolt = {} controller = SEG } #Mamia Kulibali restores Bambara authority in region
1756.1.1   = { unrest = 9 } # Denkoro seizes power in wake of father Mamali's death, civil war
1766.1.1   = { unrest = 0 } # Ngolo Diarra restores authortiy of Segu state, ends civil war

1819.1.1   = {
	owner = MSN
	controller = MSN
	add_core = MSN
	remove_core = SEG
} # The Massina Empire
