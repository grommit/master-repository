# 3953 - Kaga

owner = TGS
controller = TGS
culture = chubu
religion = mahayana
capital = "Kanazawa"
trade_goods = chinaware
hre = no
base_tax = 9
base_production = 9
base_manpower = 5
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = TGS
}
1387.1.1 = {
	add_core = SBA
	controller = SBA
	owner = SBA
}
1414.1.1 = {
	controller = TGS
	owner = TGS
}
1458.1.1 = {
	add_core = AKM
	controller = AKM
	owner = AKM
}
1464.1.1 = {
	controller = TGS
	owner = TGS
}
1542.1.1 = { discovered_by = POR }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
