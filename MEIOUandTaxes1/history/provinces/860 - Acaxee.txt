# 860 - Acaxee
# GG - 22/07/2008

culture = tarachatitian
religion = nahuatl
capital = "Quetzalla"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 4
native_hostileness = 8
discovered_by = mesoamerican

1540.1.1   = {
	discovered_by = SPA
} # Francisco Vázquez de Coronado y Luján
1567.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Santa Barbara"
	citysize = 200
	culture = castillian
	religion = catholic
	base_tax = 2
base_production = 2
	set_province_flag = trade_good_set
	trade_goods = silver
	}
1592.1.1   = {
	citysize = 500
	add_core = SPA
}
1631.1.1   = {
	capital = "San José del Parral"
	citysize = 2000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
