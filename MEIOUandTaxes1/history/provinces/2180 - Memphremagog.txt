# No previous file for Memphremagog

culture = huron
religion = totemism
capital = "Memphremagog"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1609.1.1  = { discovered_by = FRA } # Samuel de champlain
1629.1.1  = { discovered_by = ENG }
1664.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		trade_goods = fur
		citysize = 255
	    } # Network of forts established in the region
1689.1.1  = { citysize = 1000 add_core = FRA }
1707.5.12  = { discovered_by = GBR }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    	culture = french_colonial
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1788.2.10 = { add_core = GBR }
