# 1090 - Mallacoota

culture = aboriginal
religion = polynesian_religion
capital = "Mallacoota"
trade_goods = unknown #naval_supplies
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 2

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
1788.1.26 = {
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant #anglican
	citysize = 100
	capital = "Melbourne"
	trade_goods = naval_supplies
	set_province_flag = trade_good_set
}
1813.1.1 = { add_core = GBR }
