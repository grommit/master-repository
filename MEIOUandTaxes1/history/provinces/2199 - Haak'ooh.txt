# No previous file for Haak'ooh

culture = ute
religion = totemism
capital = "Haak'ooh"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 8
