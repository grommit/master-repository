# No previous file for Soyony Nuruu

owner = YUA
controller = YUA
add_core = YUA
culture = oirats
religion = tengri_pagan_reformed
capital = "Kyzyl"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = steppestech

1392.1.1 = {
	owner = OIR
	controller = OIR
	add_core = OIR
	remove_core = YUA
}
1515.1.1 = { training_fields = yes }
1615.1.1 = { religion = vajrayana } # State religion
1622.1.1 = {  }
1623.1.1 = {
	owner = OIR
	controller = OIR
} # The Oirads gained independence
1635.1.1 = {
	owner = ZUN
	controller = ZUN
	add_core = ZUN
	remove_core = OIR
}
1697.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Outer Mongolia intergrated into Qing
1709.1.1 = {  }
