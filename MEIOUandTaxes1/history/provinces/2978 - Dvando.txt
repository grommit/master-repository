# 2978 - Dvando

owner = KON
controller = KON 
add_core = KON
culture = kongolese
religion = animism
capital = "Dvando"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = copper
hre = no
discovered_by = central_african

1483.1.1   = { discovered_by = POR } # Diogo C�o
