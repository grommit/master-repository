# 2860 - Bouna

owner = HAF
controller = HAF
add_core = HAF 
culture = tunisian
religion = sunni
capital = "Baja"
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = iron
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
discovered_by = eastern
hre = no

#1088.1.1 = { dock = yes shipyard = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
	add_claim = TUR
}
1574.9.13 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = ALG
	remove_core = HAF
}  # Established direct control of the province 
1585.1.1 = {
	remove_core = SPA
}
1671.1.1 = {
	owner = ALG
	controller = ALG
	remove_core = TUR
} # Virtually independent of the Ottoman empire
1852.1.1 = {
	owner = TUN
	controller = TUN
}
1881.5.12 = {
	owner = FRA
	controller = REB
}
1881.10.28 = { controller = FRA }
