# No previous file for Mazatzal

culture = ute
religion = totemism
capital = "Mazatzal"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 4
native_hostileness = 8

1598.1.1 = {
	owner = NJO
	controller = NJO
	add_core = NJO
	is_city = yes
	trade_goods = wool
}
