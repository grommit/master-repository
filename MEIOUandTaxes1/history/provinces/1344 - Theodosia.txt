# 1344 - Theodosia

owner = GEN
controller = GEN 
add_core = GEN 
culture = pontic
religion = orthodox
capital = "Kaffa"
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
trade_goods = silk
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1356.1.1 = {
}
1444.1.1 = {
	add_claim = TUR
	remove_core = BYZ
}
1475.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = GEN
	culture = turkish
	religion = sunni
} # Seized by Gedik Ahmet Pasha
1774.7.21 = {
	owner = CRI
	controller = CRI
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
