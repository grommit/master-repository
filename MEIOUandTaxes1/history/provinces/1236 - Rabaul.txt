# 1236 - Rabaul

culture = papuan
religion = polynesian_religion
capital = "Rabaul"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 30
native_ferocity = 4
native_hostileness = 9

1000.1.1   = {
	set_province_flag = papuan_natives
}
1770.2.27 = { discovered_by = GBR } # William Dampier
