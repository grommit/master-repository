# 571 - Brahmaputra

owner = ASS
controller = ASS
culture = assamese
religion = hinduism
capital = "Charaideo"
trade_goods = tea
hre = no
base_tax = 9
base_production = 9
#base_manpower = 2.0
base_manpower = 4.0
citysize = 20000
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1100.1.1 = { marketplace = yes }
1356.1.1 = { add_core = ASS
	fort_14th = yes
}
1450.1.1 = { citysize = 24000 }
1500.1.1 = { citysize = 28000
	fort_14th = yes
	fort_15th = yes
}
1515.1.1 = { training_fields = yes }
1540.1.1 = { capital = "Garhgaon" }
1550.1.1 = { citysize = 35000 }
1600.1.1 = { citysize = 40000 }
1627.1.1 = { discovered_by = POR }
1650.1.1 = { citysize = 54000 }
1700.1.1 = { citysize = 30000 }
1750.1.1 = { citysize = 35000 }
1769.1.1 = {
	fort_15th = no
	fort_18th = yes
}
1770.1.1 = { unrest = 8 } # Moamoria rebellion
1800.1.1 = { citysize = 39000 }
1806.1.1 = { unrest = 0 }
