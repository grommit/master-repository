# 1553 - Zazzau zaria

owner = ZZZ
controller = ZZZ
culture = haussa		
religion = west_african_pagan_reformed 
capital = "Zaria"
base_tax = 11
base_production = 11
base_manpower = 5
is_city = yes
trade_goods = ivory


discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = ZZZ
}
1807.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1 = {
	remove_core = ZZZ
}
