# 751 - Urubu kaapoor

culture = ge
religion = pantheism
capital = "Urubu'kaapoor"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 9

1500.1.1   = {
	discovered_by = CAS
	discovered_by = POR
	add_permanent_claim = POR
} # Pinz�n, Pedro �lvares Cabral 
1516.1.23  = {
	discovered_by = SPA
}
1612.1.1   = {
	owner = FRA
	controller = FRA
	change_province_name = "France Equinoctiale"
	rename_capital = "Saint Louis"
	citysize = 560
	culture = francien
	religion = catholic
	trade_goods = brazil
	set_province_flag = trade_good_set
}
1641.1.1   = {
	owner = NED
	controller = NED
	culture = dutch
	religion = reformed
}
1644.1.1   = {
	owner = POR
	controller = POR
	culture = portugese
	religion = catholic
	citysize = 1000
	change_province_name = "Maranh�o"
	rename_capital = "Sao Luis"
}
1718.1.1  = {
	unrest = 6
} # Natives led by Mandu Ladino rebelled against the Portugese expansion
1725.1.1  = {
	unrest = 0
} # The rebellion is subdued
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1800.1.1   = {
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
