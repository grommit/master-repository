# No previous file for Ravenna
#Ravenna + Cesena +  Lugo + Bagnacallo

owner = DAP
controller = DAP
culture = romagnol
religion = catholic 
hre = no 
base_tax = 8
base_production = 8        
trade_goods = wine      
base_manpower = 3
is_city = yes
capital = "Rav�na"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }
#1111.1.1 = { post_system = yes }

1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = DAP
}
1440.1.1   = {
	owner = VEN
	controller = VEN
} # Ostasio III Da Polenta ousted by Venetians
1509.1.1   = {
	owner = PAP
	controller = PAP
	add_core = PAP
	bailiff = yes
} # To the Papal States following the Italian Wars
1522.3.20 = { dock = no naval_arsenal = yes }
1527.1.1   = {
	owner = VEN
	controller = VEN
	road_network = no paved_road_network = yes 
}
1529.1.1   = {
	owner = PAP
	controller = PAP
	road_network = no paved_road_network = yes 
}

1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = PAP
} # Cispadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11  = {
	owner = PAP
	controller = PAP
	add_core = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = PAP
}
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
	remove_core = SPI
}
