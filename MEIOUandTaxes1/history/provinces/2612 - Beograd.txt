# 2612 - Beograd

owner = SER
controller = SER
culture = serbian
religion = orthodox
capital = "Beograd"
trade_goods = cloth
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = SER
	add_local_autonomy = 15
#	add_core = HUN
}
1427.1.1  = {
	owner = HUN
	controller = HUN
	add_core = HUN
	add_local_autonomy = -15
} # Despotate of Serbia forced to give back Belgrad
1515.2.1 = { training_fields = yes }
1521.8.28 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1595.1.1  = { fort_14th = yes } # Estimated

1688.11.7 = { unrest = 7 } # Serb rebellion under D. Brankovic

1717.8.22 = {
	owner = HAB
	controller = HAB
	
} # Austrian occupation
1739.9.18 = {
	owner = TUR
	controller = TUR
} # Treaty of Beograd, the city is given back
1750.1.1  = {  }
1788.3.1  = { controller = HAB } # Occupied by Austrian forces
1791.1.1  = { controller = TUR }
1806.1.8  = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The first Serbian uprising
1813.1.1  = {
	revolt = { }
	controller = TUR
}
1815.4.23 = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} # The second Serbian uprising
1817.1.1  = {
	revolt = { }
	owner = SER
	controller = SER
} # Serbia released as a vassal
