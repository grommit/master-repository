# 431 - Qishm
# MEIOU-GG - Turko-Mongol mod

owner = ORM
controller = ORM
culture = omani
religion = sunni
capital = "Qishm"
trade_goods = fish
hre = no

base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
discovered_by = muslim
discovered_by = indian
discovered_by = turkishtech
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1088.1.1 = { dock = yes }
#1111.1.1 = { post_system = yes }
1200.1.1 = { marketplace = yes }
1356.1.1  = {
	add_core = ORM
}
1488.1.1  = { discovered_by = POR } # P�ro da Covilh�

#1507.1.1  = {
#	controller = POR
#	owner = POR
#}
#1512.1.1  = {
#	add_core = PER
#}
1530.1.3 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1557.1.1  = {
	add_core = POR
}
1622.1.1  = {
	controller = PER
}
1623.1.1  = {
	owner = PER
	
}
1783.1.1   = {
	owner = OMA
	controller = OMA
	add_core = OMA
	fort_14th = yes
} # Khan of Kalat grants suzerainty to Taimur Sultan, ruler of Muscat
1868.1.1   = {
	owner = PER
	controller = PER
	remove_core = OMA
}
