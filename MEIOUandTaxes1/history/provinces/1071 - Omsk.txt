# 1071 - Om

owner = SIB
controller = SIB
culture = siberian
religion = tengri_pagan_reformed
capital = "Omsk"
trade_goods = fur
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1356.1.1 = {
	add_core = SIB
}
1441.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = GOL
}
1468.1.1 = {
	owner = SIB
	controller = SIB
	discovered_by = SIB
	remove_core = SHY
} # Sibir Khanate formed from northern Uzbek lands
1585.1.1  = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	add_core = RUS
	religion = orthodox
	culture = russian
	is_city = yes
	base_tax = 2
	base_production = 2
	trade_goods = fur
} # Yermak Timofeevich
1587.1.1  = { capital = "Omsk" }
1606.1.1  = { unrest = 3 } # Rebellions
1608.1.1  = { unrest = 5 }
1610.1.1  = { unrest = 2 }
1616.1.1  = { unrest = 6 }
1620.1.1  = { unrest = 0 }
