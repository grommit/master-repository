# No previous file for Tumbuka

owner = TMB
controller = TMB
culture = nyasa
religion = animism
capital = "Tumbuka"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = gems
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = TMB
}
