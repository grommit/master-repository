# No previous file for Kampong Chhang

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Longvek"
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1133.1.1 = { mill = yes }
1356.1.1 = {
	#controller = AYU
	add_core = KHM
	add_core = AYU
}
1360.1.1 = {
	controller = KHM
}
1767.4.8 = { 
	add_core = SIA
	remove_core = AYU
}
1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = { 			# agreement with France
	owner = SIA
	controller = SIA
}
1907.1.1 = { 			
	owner = FRA
	controller = FRA
}
