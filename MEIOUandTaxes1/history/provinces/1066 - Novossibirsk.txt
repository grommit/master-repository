# 1066 - Novossibirsk

culture = siberian
religion = tengri_pagan_reformed
capital = "Tomsk"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 2
native_hostileness = 3

discovered_by = SIB
discovered_by = WHI
discovered_by = BLU

1585.1.1  = {
	discovered_by = RUS 
	owner = RUS
	controller = RUS
	religion = orthodox
	culture = russian
	citysize = 120
	trade_goods = fur
	set_province_flag = trade_good_set
} # Yermak Timofeevic
1600.1.1 = { citysize = 240 }
1631.1.1 = { add_core = RUS }
1650.1.1 = { citysize = 455 }
1666.1.1 = { unrest = 4 } # Samoyed rebellion
1669.1.1 = { unrest = 0 }
1679.1.1 = { unrest = 3 }
1680.1.1 = { unrest = 0 }
1700.1.1 = { citysize = 831 }
1750.1.1 = { citysize = 1200 }
1800.1.1 = { citysize = 1877 }
