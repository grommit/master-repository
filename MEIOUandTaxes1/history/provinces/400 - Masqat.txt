#400 - Muscat

owner = OMA
controller = OMA
add_core = OMA
culture = omani
religion = ibadi
capital = "Muscat"
trade_goods = fish
hre = no

base_tax = 8
base_production = 8
base_manpower = 2
is_city = yes
discovered_by = ADA
discovered_by = muslim
discovered_by = indian

1000.1.1 = {
	
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = minor_center_of_trade_modifier
		duration = -1
	}
}
1088.1.1 = { dock = yes }
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1480.1.1  = { discovered_by = TUR }
1488.1.1  = { discovered_by = POR } # P�ro da Covilh�
1507.6.1  = {
	owner = POR
	controller = POR
	add_core = POR
	set_province_flag = TP_trading_post
	trading_post = yes
} # Captured by the Portuguese
1510.1.1  = { fort_14th = yes  }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1600.1.1  = { fort_14th = yes }
1624.1.1  = {
	owner = OMA
	controller = OMA
	remove_core = POR
} # Rule restored
 # Muscat prospered
1679.1.1  = { unrest = 5 } # Internal unrest upon Imam's death
1720.1.1  = {  }
1741.1.1  = {
	owner = PER
	controller = PER	
	add_core = PER
} # Invaded by Persia
1749.6.10 = {
	owner = OMA
	controller = OMA
	remove_core = PER
	unrest = 0
}
