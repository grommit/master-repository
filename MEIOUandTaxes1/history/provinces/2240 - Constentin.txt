# 2240 - Cotentin (Cherbourg)

owner = FRA
controller = FRA 
culture = normand
religion = catholic
capital = "Chirebourc"
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes
trade_goods = fish
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1354.1.1   = {
	owner = NAV
	controller = NAV
	add_core = NAV
	add_core = FRA
	add_core = NRM
} # Jean le Bon gives Cherbourg to Charles le Mauvais
1369.1.1   = {
	fort_14th = yes
}
1378.4.20  = {
	owner = FRA
	controller = FRA
} # Public trial of Jacques de Rue for regicide and treason. Charles V moves against Charles le Mauvais1500.1.1   = { citysize = 15000 }
1419.1.19  = {
	owner = ENG
	controller = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
}
1434.12.1   = {
	owner = FRA
	controller = FRA
}
1475.8.29  = {
	remove_core = ENG
} # Treaty of Picquigny, ending the Hundred Year War
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism

1631.1.1   = { unrest = 3 } # Region is restless
1633.1.1   = { unrest = 0 } 
1639.1.1   = { unrest = 3 }
1641.1.1   = { unrest = 0 }
1650.1.1   = { fort_14th = no fort_15th = yes }
 # Colbert

1760.1.1   = { fort_15th = no fort_16th = yes }

1793.3.7   = {
} # Guerres de l'Ouest
1796.12.23 = {
} # The last rebels are defeated at the battle of Savenay
1799.10.15 = {
} # Guerres de l'Ouest
1800.1.18  = {
}
