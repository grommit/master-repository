# 4004 - Bentheim

owner = BNT
controller = BNT 
culture = dutch
religion = catholic
capital = "Bentheim"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
fort_14th = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1356.1.1   = {
	add_core = BNT
}
1500.1.1 = { road_network = yes }
1587.1.1   = {
	religion = protestant
}
1753.1.1 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = LUN
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # Congress of Vienna, Luneburg is incorporated into the Kingdom of Hannover
