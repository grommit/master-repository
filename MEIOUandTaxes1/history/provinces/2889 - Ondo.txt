# 2889 - Ondo

owner = BEN
controller = BEN
culture = yorumba
religion = west_african_pagan_reformed
capital = "Ondo"
base_tax = 10
base_production = 10
base_manpower = 2
is_city = yes
trade_goods = gems
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = BEN
}
1806.1.1 = {	
	owner = SOK
	controller = SOK
	add_core = SOK
}
