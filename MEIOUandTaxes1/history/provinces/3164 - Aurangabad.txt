# No previous file for Aurangabad

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Daulatabad"
trade_goods = cotton
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
discovered_by = indian
discovered_by = steppestech
discovered_by = muslim
discovered_by = turkishtech
discovered_by = chinese
add_local_autonomy = 25

1100.1.1 = { marketplace = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
#1180.1.1 = { post_system = yes }
1199.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = BAH
	add_core = BAS
	fort_14th = yes
	add_permanent_province_modifier = {
		name = daulatabad_fort
		duration = -1
	}
}
1490.1.1  = {
	remove_core = BAH
	controller = BAS
	owner = BAS
} # The Breakup of the Bahmani sultanate
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1530.2.3 = {
	add_permanent_claim = MUG
}
1595.1.1 = { controller = MUG } # captured by Mughal Empire
1596.1.1 = {
	owner = MUG
	add_core = MUG
} # captured by Mughal Empire
1615.1.1 = { controller = BAS } # Ahmednagar Anti-Mughal Campaign
1622.1.1 = {
	controller = MUG
}
1622.3.1 = {
	controller = REB
	revolt = {
		type = pretender_rebels
		size = 0
		name = "Khurrams Faction"
		leader = "Shah Jahan Timurid"
	}
} #Should be named Khurram at this point
1622.8.15 = {
	controller = MUG
	revolt = { }
} #Khurram flees on into Ahmednagar, Golconda and then Orissa. Eventually attacks Mughal Bengal.
1650.1.1 = {
	add_core = MAR #Maratha identity
}
1653.1.1 = { capital = "Aurangabad" } # Aurangzeb renames the city
1658.1.15 = {
	controller = REB
	revolt = {
		type = pretender_rebels
		size = 5
		name = "Aurangzeb's Faction"
		leader = "Aurangzeb Alamgir Timurid"
	}
} # Aurangzeb joins in rebelion against his father
1658.6.8 = {
	controller = MUG
	revolt = { }
} #Shah Jahan surrenders and becomes the prisoner of his son Aurangzeb
1712.1.1 = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1 = {
	owner = HYD
	controller = HYD
	remove_core = MUG
} # Asif Jah declared himself Nizam-al-Mulk
1760.1.1 = {
	owner = MAR
	controller = MAR
} # Marathas
1804.1.1 = {
	owner = HYD
	controller = HYD
	remove_core = MAR
}
