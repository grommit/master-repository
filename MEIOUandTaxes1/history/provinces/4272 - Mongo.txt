# No previous file for Mongo

culture = kuba
religion = animism
capital = "Mongo"
native_size = 50
native_ferocity = 1
native_hostileness = 7
trade_goods = unknown # ivory
hre = no
discovered_by = central_african

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
