# 2440 - Hilin

culture = uralic
religion = tengri_pagan_reformed
capital = "Khlynov"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 5
discovered_by = eastern
discovered_by = SIB
discovered_by = muslim

1181.1.1  = {
	owner = NOV
	controller = NOV
	add_core = NOV
	religion = orthodox
	culture = pomor
	is_city = yes
	trade_goods = lumber
	set_province_flag = trade_good_set
}
1478.1.14 = {
	remove_core = NOV
	owner = KAZ
	controller = KAZ
	add_core = KAZ
	capital = "Hilin"
} # End of the Novgorod republic
1489.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = KAZ
	capital = "Khlynov"
}
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1606.1.1 = { unrest = 3 } # Rebellions against Russian rule
1608.1.1 = { unrest = 5 }
1610.1.1 = { unrest = 2 }
1616.1.1 = { unrest = 6 }
1620.1.1 = { unrest = 0 }
1781.1.1 = {
	capital = "Viatka"
}
