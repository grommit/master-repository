# 2236 - Bukovina

owner = MOL
controller = MOL  
add_core = MOL
culture = moldovian
religion =  orthodox
capital = "Cernauti"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = wine
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1200.1.1 = { road_network = yes }
#1498.1.1  = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1530.1.4  = {
	bailiff = yes	
}
1660.1.1  = {
	
}
1772.1.1  = {
	add_core = HAB
}
1775.1.1  = {
	owner = HAB
	controller = HAB
	remove_core = TUR
}
