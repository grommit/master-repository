# No previous file for Mauvila

culture = choctaw
religion = totemism
capital = "Mauvila"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1519.1.1 = { discovered_by = SPA } # Alonzo Alvarez de Pineda
1702.1.20 = {
	owner = FRA
	controller = FRA
	citysize = 355
	culture = francien
	religion = catholic
	trade_goods = cotton
} # Founding of Mobile
#1717.1.1 = { fort_14th = yes } # Fort Toulouse on the Coosa River constructed
1727.1.1 = { add_core = FRA }
1750.1.1 = { citysize = 2140 }
1756.5.16 = { controller = GBR } # Occupied by the British
1763.2.10 = {
	owner = GBR
	remove_core = FRA
	culture = english
	religion = protestant
} # Treaty of Paris - ceded to Britain, France gave up its claim
1783.9.3 = { owner = SPA controller = SPA } # Spanish occupation
1800.1.1 = { citysize = 3350 }
1808.9.3 = { add_core = SPA }
1813.4.1 = {
	owner = USA
	controller = USA
	add_core = USA
	remove_core = SPA
} # Captured by the Americans
