# No previous file for Muh-he-kan

culture = mahican
religion = totemism
capital = "Muh-he_kan"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 5

1609.1.1  = { discovered_by = FRA } # Samuel de Champlain
1629.1.1  = { discovered_by = ENG }
1724.1.1  = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
		citysize = 1030
	culture = english
	religion = protestant
} # Fort Dummer, British settlement
1749.1.1  = { add_core = GBR }
1750.1.1  = { citysize = 6000 }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1765.1.1  = { unrest = 7 } # Controversy regarding New York's territorial claim to Vermont
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Still part of New York at this time, even if there was disagreement
1777.1.15  = { unrest = 0 revolt = { type = particularist_rebels size = 3 } controller = REB } # Republic of Vermont is proclaimed (no tag available)
1782.11.1 = {	remove_core = GBR unrest = 0   } # Preliminary articles of peace, the British recognized Amercian independence
1791.3.4  = { revolt = {} controller = USA } # Admitted to the union as its own state
1799.1.1  = { base_tax = 2 } # Federal tax on property
1800.1.1  = { citysize = 85450 }
