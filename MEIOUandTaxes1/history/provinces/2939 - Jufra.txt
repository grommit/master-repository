# 2939 - Kufra

culture = toubous
religion = sunni
capital = "Al Jawf"
trade_goods = wool
hre = no
native_size = 30
native_ferocity = 4.5
native_hostileness = 9
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}

