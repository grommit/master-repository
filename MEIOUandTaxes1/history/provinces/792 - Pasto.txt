# 792 - Pasto

culture = muisca
religion = pantheism
capital = "Pasto"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 2
discovered_by = south_american

1356.1.1 = {
}
1524.1.1   = { discovered_by = SPA }	#FB was 1524
1530.1.1 = {
	owner = PIZ
	controller = PIZ
	add_core = PIZ
	remove_core = SPA
	culture = castillian
	religion = catholic
	trade_goods = maize
	road_network = yes
	training_fields = yes
	marketplace = yes
}
1551.3.6   = {
	add_core = SPA
	owner = SPA
	controller = SPA
	remove_core = PIZ
	culture = castillian
	religion = catholic
	is_city = yes
	trade_goods = maize
	change_province_name = "Mocoa"
	rename_capital = "Mocoa"
}
1750.1.1  = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
