# No previous file for Jambyl

owner = MGH
controller = MGH
culture = kirgiz
religion = sunni
capital = "Bishkek"
trade_goods = cloth
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = MGH
	add_core = KAS
}
1482.1.1 = {
	owner = KAS
	controller = KAS
}
1504.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
   	remove_core = SHY
}
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
   	remove_core = BUK
}
