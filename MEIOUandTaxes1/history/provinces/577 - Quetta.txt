#Province: Quetta
#file name: 577 - Quetta
#MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Turko-Mongol mod

owner = MUL
controller = MUL
culture = pashtun
religion = sunni
capital = "Quetta"
trade_goods = wheat
hre = no
base_tax = 5
base_production = 5
#base_manpower = 2.0
base_manpower = 4.0
citysize = 12000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1356.1.1  = { add_core = MUL add_core = KAB fort_14th = yes }
1414.1.1  = {
 	owner = DLH
	controller = DLH
	remove_core = MUL
}
1437.1.1  = {
	owner = MUL
	controller = MUL
	add_core = MUL
	remove_core = PUN
	remove_core = DLH
} # Captured by Langas and separated from Sultanate
1444.1.1 = { add_core = KAB }
1450.1.1  = { citysize = 14000 }
1500.1.1  = { citysize = 16000 }
1515.1.1 = { training_fields = yes }
1526.1.1  = {
	owner = SND
	controller = SND
} # Conquered by Arguns
1527.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
	bailiff = yes
} # Turned over to Babur
1529.12.11 = { add_core = DUR remove_core = KAB }
1550.1.1  = { citysize = 20000 }
1600.1.1  = { citysize = 16000 } 
1650.1.1  = { citysize = 22000 }
1677.1.1  = { discovered_by = FRA }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 30000  }
1707.5.12 = { discovered_by = GBR }
1747.10.1 = {
	owner = DUR
   	controller = DUR
	add_core = DUR
} # Ahmad Shah established the Durrani empire
1750.1.1  = { citysize = 32000 }
1800.1.1  = { citysize = 35000 }
