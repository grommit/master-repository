# 849 - Yopihtzinco
# GG - 28/07/2008
# Mesoamerican mod v1

owner = TLP
controller = TLP
add_core = TLP
culture = tlapanec
religion = nahuatl
capital = "Chilipantzinco" 

base_tax = 7
base_production = 7
#base_manpower = 2.0
base_manpower = 4.0

trade_goods = cotton 
citysize = 5000


discovered_by = mesoamerican

hre = no

1506.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	citysize = 2000
	add_core = SPA
	capital = "Acapulco de Ju�rez"
	dock = yes
	road_network = yes
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 5000
}
1650.1.1   = {
	citysize = 10000
}
1700.1.1   = {
	citysize = 15000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 25000
}
1800.1.1   = {
	citysize = 30000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
