# 2225 - Qom

owner = CHU
controller = CHU
culture = persian
religion = sunni #Dei Gratia
capital = "Qom"
trade_goods = hemp
hre = no
base_tax = 8
base_production = 8
base_manpower = 5
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }

1356.1.1 = {
	add_core = CHU
	add_core = WHI
	unrest = 4
	add_core = SDB
	add_core = MUZ
}
1357.1.1   = {
	owner = WHI
	controller = WHI
	remove_core = CHU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
	remove_core = WHI
}
1384.1.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1444.1.1 = {
	remove_core = JAI
	add_core = AKK
}	
1444.1.1 = {
	controller = ISF
	owner = ISF
	remove_core = TIM
}
1447.3.11 = {
	owner = QAR
	controller = QAR
	add_core = QAR
	remove_core = TIM
} # Fars and surroundings to Qara Quyunlu
1469.1.1 = {
	controller = AKK
}
1470.1.1 = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1497.1.1   = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Civil War

1501.1.1   = {
	controller = SAM
	revolt = { }
} # Safawid Expansion
1508.1.1   = {
	owner = SAM
} # Safawid Expansion
1512.1.1  = {
	owner = PER
	controller = PER
	remove_core = AKK
	add_core = PER
	religion = shiite
	training_fields = yes
	bailiff = yes
} # The Safavids took over, Shi'ism becomes the state religion
1550.1.1  = { discovered_by = TUR }
1587.1.1  = {
	controller = REB
	revolt = { type = pretender_rebels }
}
1587.12.11  = {
	controller = PER
	revolt = {  }
}
1620.1.1  = { fort_14th = yes }
1677.1.1 = { discovered_by = FRA }
1747.1.1  = { unrest = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { unrest = 4 } # The empire began to decline
1779.1.1  = { unrest = 0 } # With the Qajar dynasty the situation stabilized
