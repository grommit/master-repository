# 2762 - Senouf (Korhogo)

owner = BON
controller = BON
culture = akaa
religion = west_african_pagan_reformed
capital = "Senoufo"
trade_goods = leather
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = BON
}
