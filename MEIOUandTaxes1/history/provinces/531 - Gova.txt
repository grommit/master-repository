# 531 - Gova

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Gov�"
trade_goods = pepper
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes
discovered_by = indian
discovered_by = muslim


	
1000.1.1   = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1100.1.1 = { marketplace = yes }
1347.8.3  = {
	add_core = BAH
	add_core = BIJ
}
1380.1.1  = {
	owner = VIJ
	controller = VIJ
	add_core = BIJ
}
1469.1.1 = {
	controller = BAH
}
1470.1.1  = {
	owner = BAH
	remove_core = VIJ
} # Conquered by the Bahmani sultans
1490.1.1  = {
	owner = BIJ
	controller = BIJ
	add_core = BIJ
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1502.1.1  = { discovered_by = POR }		#FB was: 1498.1.1
#1510.1.1 = { set_country_flag = TP_trading_post }
1510.1.1  = {
	owner = POR
	controller = POR
	capital = "Goa"
	add_core = POR
	remove_core = BIJ
	fort_15th = yes
	naval_arsenal = yes
	armory = yes
	customs_house = yes
	set_province_flag = TP_trading_post
	trading_post = yes
	road_network = yes
} # Conquered by Afonso de Albuquerque
1518.1.1  = { remove_core = BAH } # The breakup of the Bahmani sultanate
1526.4.21 = { remove_core = DLH } # Battle of Panipat
1535.1.1  = { add_core = POR }

1580.1.1  = { fort_15th = no fort_16th = yes }
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = {  discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }

1674.1.1  = { add_core = MAR } # Maratha Empire

1707.5.12 = { discovered_by = GBR }
1715.1.1  = {  }

1818.6.3  = { remove_core = MAR }
