# 2795 - Limburg

owner = BRB
controller = BRB 
add_core = BRB
culture = brabantian
religion = catholic
capital = "Mestreech" #Maastricht
is_city = yes # citysize = 1500
fort_14th = yes
base_tax = 7
base_production = 7
base_manpower = 2
trade_goods = linen
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

1292.1.1   = {
	add_permanent_province_modifier = {
		name = "imperial_abbey_of_thorn"
		duration = -1
	}
}
1430.1.1   = { owner = BUR controller = BUR add_core = BUR }
1477.1.5   = { unrest = 10 } # death of Charles the Bold
1477.8.18  = { unrest = 0 } # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27  = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Mary of burgondy dies, Lowlands to Austria
1492.7.12  = { unrest = 0 } # Peace of Cadzand, peace returns
 # Sint-John's Cathedral finished in 's Hertogenbosch
1500.1.1 = { road_network = yes }
1530.1.1   = { fort_14th = yes }
1530.1.2  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = BUR
}

1542.1.1   = { controller = GEL } # Gelre occupies Noord-Brabant in the Austro-Gelrian War
1543.1.1   = { controller = HAB } # Charles V is back in control
1549.11.4  = { add_core = NED remove_core = BRB }
1550.1.1   = { add_core = NED }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1559.5.12  = { unrest = 3 } # New bishoprics in the Lowlands create an outrage
1565.1.1   = { unrest = 5 } # Letters of Segovia, Philip I orders the harsh persecution of Calvinists
1566.4.5   = { unrest = 3 } # 'Eedverbond der Edelen', Margaretha of Parma promises leniency
1567.9.10  = { unrest = 4 } # Counts of Egmont & Hoorne arrested
1568.6.5   = { unrest = 6 } # Counts of Egmont & Hoorne beheaded
1569.1.1   = { unrest = 12 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = { unrest = 20   } # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1572.1.1   = { religion = reformed }
1577.2.12  = { unrest = 16 } # 'Perpetual Edict' (Eeuwig Edict) accepted by Don Juan
1579.1.23  = { owner = NED controller = NED remove_core = SPA unrest = 0 } # Union of Utrecht - Noord-Brabant joins
1581.7.2   = { controller = SPA unrest = 12 } # Alessandro Farnese takes Breda
1590.3.4   = { controller = NED unrest = 0 } # Breda is retaken after a succesful diversion
 # Later on: Generality Lands, military important
1600.1.1   = { fort_14th = no fort_15th = yes }
 # Vereenigte Westindische Compagnie

1625.4.3   = { controller = SPA unrest = 15 } # After the 12-year armistice, the fight is resume: Spinola takes Breda 
1629.1.1   = { controller = NED unrest = 0 } # Frederik Hendrik captures 's Hertogenbosch
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1672.5.15  = { controller = FRA } # France blitzes through the Lowlands in the Franco-Dutch War
1678.8.10  = { controller = NED } # Peace of Nijmegen (Netherlands-France)
1681.1.1   = { base_tax = 10
base_production = 10 } # Protestants expelled from France

1715.1.1   = { fort_15th = no fort_16th = yes }
1720.1.1   = {  }
1747.9.18  = { controller = FRA } # French troops storm and take Bergen-op-Zoom
1748.10.18 = { controller = NED } # Peace of Aachen
1810.7.10  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1813.11.30 = {
	owner = NED
	controller = NED
	remove_core = FRA
} # William returns to the Netherlands
