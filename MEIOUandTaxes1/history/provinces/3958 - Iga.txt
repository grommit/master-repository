# 3958 - Iga

owner = NIK
controller = NIK
culture = kansai
religion = mahayana
capital = "Uyeno"
trade_goods = rice
hre = no
base_tax = 2
base_production = 2
base_manpower = 3
is_city = yes
discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = NIK
}
1433.1.1 = {
	controller = YMN
	owner = YMN
}
1435.1.1 = {
	owner = NIK
	controller = NIK
}
1542.1.1   = { discovered_by = POR }
1572.1.1   = {
	owner = ODA
	controller = ODA
}
1583.1.1   = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
