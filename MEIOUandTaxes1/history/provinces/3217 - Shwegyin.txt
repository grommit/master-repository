# No previous file for Shwegyin

owner = PEG
controller = PEG
culture = monic
religion = buddhism
capital = "Shwegyin"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1 = {
	add_core = AVA
	add_core = SST
	add_core = PEG
}
1509.1.1 = { discovered_by = POR }
1530.1.1 = { remove_core = AVA remove_core = SST add_core = TAU }
1539.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = SST
	remove_core = AVA
} # Annexed to the Kingdom of Taungoo
1574.1.1 = { owner = AYU controller = AYU } # Siamese occupation
1575.1.1 = { owner = TAU controller = TAU }
1740.1.1 = { controller = REB } # Pegu rebellion
1752.2.28 = {
	owner = PEG
	controller = PEG
	remove_core = TAU
} # Pegu is sacked & returned to Burmese control
1759.1.1 = {
	owner = BRM
	controller = BRM
	add_core = BRM
} # Annexed by Burma
1852.12.20 = { owner = GBR controller = GBR } #Mon allied with british return to homeland fred by burmese
