# 2665 - Tabouk

owner = HEJ
controller = HEJ
culture = hejazi
religion = sunni
capital = "Tabouk"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	owner = MAM
	controller = MAM
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1520.1.1 = { fort_14th = yes }
#1530.1.1   = {
#	owner = HEJ
#	controller = HEJ
#	add_core = HEJ
#	remove_core = TUR
#}
1530.1.5 = {
	owner = HEJ
	controller = HEJ
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1770.1.1 = {
	owner = MAM
	controller = MAM
	remove_core = TUR
} # Ali Bey gained control of the Hijaz, reconstituting the Mamluk state
1772.1.1 = {
	owner = HEJ
	controller = HEJ
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # The First Saudi State
1811.1.1 = {
	add_core = HEJ
} # Intervention of Mehmet Ali on behalf of the Sultan
1812.6.1 = {
	owner = TUR
	controller = TUR
	remove_core = NAJ
}
