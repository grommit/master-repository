# 3733 - Hanau

owner = HNU
controller = HNU
capital = "Hanau"
trade_goods = wool
culture = hessian
religion = catholic
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = { add_core = HNU }
1500.1.1 = { road_network = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1535.1.1   = {
	religion = protestant
}
1600.1.1   = {
	fort_14th = yes
}
1736.3.28  = {
	owner = HKA
	controller = HKA
	add_core = HKA
} # Johann Rainhard III dies, inherited by the Landgrave of Hesse-Kassel (actually autonomous for an extra fifty years)
1803.2.25 = {
	owner = HES
	controller = HES
	add_core = HES
	remove_core = HKA
}
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	fort_14th = no
} # The Second Treaty of Tilsit, the kingdom of Westfalia - Napoleon has the fortifications destroyed
1813.9.1   = { controller = RUS } # Occupied by Russian troops
1813.10.14 = {
	owner = HES
	controller = HES
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
1866.1.1  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES 
}
