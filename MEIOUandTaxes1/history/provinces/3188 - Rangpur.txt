# No previous file for Rangpur

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Rangpur"
trade_goods = livestock
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1120.1.1 = { farm_estate = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BNG
}
1500.1.1  = {
	discovered_by = POR
}
1530.1.1 = { 
	add_permanent_claim = MUG
	bailiff = yes	
	training_fields = yes
}
1530.1.2 = { add_core = TRT }
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1810.1.1  = {
	add_core = GBR
}
