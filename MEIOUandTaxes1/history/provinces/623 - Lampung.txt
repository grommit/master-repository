# 623 - Lampung

owner = SRV
controller = SRV
culture = sumatran
religion = hinduism
capital = "Lampung"
trade_goods = pepper
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
	add_core = SRV
	add_core = MPH
	add_core = LPG
}
1377.1.1 = {
	owner = LPG
	controller = LPG
	remove_core = SRV
} # Majapahit destroys the remains of the Srivijaya
1509.1.1 = { discovered_by = POR } # Diego Lopez de Sequiera
1550.1.1 = { religion = sunni }
1552.1.1 = {
	owner = BAN
	controller = BAN
	add_core = BAN
}
1600.1.1 = { discovered_by = NED }
1725.1.1 = {
	owner = NED
	controller = NED
	trade_goods = gold
} # The Dutch gradually gained control
1750.1.1 = { add_core = NED remove_core = BAN }
