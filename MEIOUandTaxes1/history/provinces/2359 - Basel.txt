# 2359 - Basel

owner = BSL
controller = BSL
culture = high_alemanisch
religion = catholic
capital = "Basel"
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
trade_goods = wine
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = BSL
}

1459.1.1 = { medieval_university = yes }
1500.1.1 = { road_network = yes }
1501.1.1 = { 
	add_core = SWI 
	owner = SWI
	controller = SWI
}
1528.1.1  = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
