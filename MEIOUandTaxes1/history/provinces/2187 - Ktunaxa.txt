# 2187 - Westfalen
# GG - 22/07/2008

owner = KOL
controller = KOL
culture = old_saxon
religion = catholic
capital = "Arnsberg"
trade_goods = wheat
hre = yes
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
add_core = KOL
discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1500.1.1 = { road_network = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1802.1.1 = {
	owner = HES
	controller = HES
}
1803.4.27 = {
	add_core = HES
} #Reichsdeputationshauptschluss
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES
}#Congress of Vienna
