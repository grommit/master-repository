#1109 - Wairau

culture = polynesian
religion = polynesian_religion
hre = no
trade_goods = unknown # wool
base_tax = 1
base_production = 1
base_manpower = 1
capital = "Wairau"
native_size = 10
native_ferocity = 4
native_hostileness = 9

1642.1.1 = { discovered_by = NED } # Abel Tasman
