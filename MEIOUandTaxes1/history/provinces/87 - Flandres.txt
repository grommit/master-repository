#87 - Rijssel
#Douai, Rijssel(Lille), Orchies

owner = FLA
controller = FLA
culture = flemish
religion = catholic
capital = "Do�"
base_tax = 10
base_production = 10
base_manpower = 7
is_city = yes
trade_goods = cloth
 # Exception to the rule, trade junction & established here
 
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = FLA
	add_core = BUR
}
1385.1.1   = { owner = BUR controller = BUR }

1444.1.1 = { remove_core = FRA }
1477.1.5 = { add_core = FRA }
1477.1.5  = { unrest = 10 } # death of Charles the Bold
1477.8.18 = { unrest = 0 } # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27  = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Mary of burgondy dies, Lowlands to Austria} # Frederick III dies, Charles VII cedes Artois to Maximilian I von Habsburg

1500.1.1 = { road_network = yes }
1522.2.15 = { shipyard = yes }
1530.1.1  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
	remove_core = FRA
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = BUR
}
1548.6.26  = { hre = yes } # Flanders incorporated into the Holy Roman Empire
1550.1.1   = { add_core = NED }
1556.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB }
1566.8.10  = { controller = REB } # 'Beeldenstorm' also hits parts of Artois
1567.1.8   = { controller = SPA } # Spain is back in control
1569.1.1   = { unrest = 7 } # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1   = {
	unrest = 11
	
} # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1577.2.12  = { unrest = 5 } # The 'Perpetual Edict' (Eeuwig Edict) is accepted by Don Juan
1579.1.6   = { add_core = EBU add_core = ARS } # traite d'arras

1640.1.1   = { fort_14th = no fort_15th = yes }
1667.1.1   = {
	controller = FRA
}
1668.5.2   = {
	owner = FRA
	add_core = FRA
	hre = no
} # Treaty of Aix-la-Chapelle
1670.1.1   = { fort_15th = no fort_16th = yes }
1680.1.1   = { fort_16th = no fort_17th = yes } # Vauban's 'pointed' fort in Calais
1708.1.1   = {
	controller = HAB
}
1713.4.11  = {
	controller = FRA
} # Treaty of Utrecht

