# No previous file for Tuk�ti

culture = ge
religion = pantheism
capital = "Tuk�ti"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3

1728.9.18 = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	citysize = 280
	culture = portugese
	religion = catholic
	trade_goods = gems
	change_province_name = "Diamantino"
	rename_capital = "Diamantino"
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
	citysize = 1280
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
