# 2826 - Strung Treng

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Strung Treng"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes  
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1250.1.1 = { temple = yes }
1535.1.1 = { discovered_by = POR }
1594.1.1 = {
	owner = LXA
	controller = LXA
	add_core = LXA
}
1694.1.1 = { add_core = ANN } # Vietnamese Intervention
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
} # Lan Xangs declared itself independent, partitioned into three kingdoms; Vientiane, Champasak & Luang Prabang
1713.1.1 = {
	owner = CHK
	controller = CHK
	add_core = CHK
	remove_core = VIE
}
1811.1.1 = { unrest = 5 } # The Siamese-Cambodian Rebellion
1812.1.1 = { unrest = 0 }
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
