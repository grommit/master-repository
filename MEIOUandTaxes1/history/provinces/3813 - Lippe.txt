# 3813 - Lippe

owner = LPP
controller = LPP
culture = old_saxon
religion = catholic
capital = "Detmold"
trade_goods = livestock
hre = yes
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
fort_14th = yes
add_core = LPP
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = { road_network = yes }
1530.1.4  = {
	bailiff = yes	
}
1538.1.1   = {
	religion = protestant
}
