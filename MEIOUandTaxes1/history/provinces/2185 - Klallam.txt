# No previous file for Klallam

culture = salish
religion = totemism
capital = "Klallam"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 40
native_ferocity = 3
native_hostileness = 4
