# 2572 - Dodekanisios

owner = KNI 		# Knights of St. John Hospitaler of Jerusalem
controller = KNI
culture = greek
religion = orthodox
capital = "Kos"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
#base_manpower = 1.5
base_manpower = 3.0
citysize = 14884

discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1088.1.1 = { dock = yes }
1356.1.1 = {
	add_core = KNI
	add_permanent_claim = BYZ
	set_province_flag = greek_name
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1444.1.1 = {
	add_core = TUR
}
1500.1.1   = { citysize = 17510 }
1523.12.21 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = KNI
	change_province_name = "Oniki Ada"
	rename_capital = "Kasot"
} # Part of the Ottoman Empire
1550.1.1   = { citysize = 14688 }
1580.1.1   = { fort_14th = yes  }
1600.1.1   = { citysize = 12468 }
1650.1.1   = { citysize = 12600 }
1700.1.1   = { citysize = 13460 }
1720.1.1   = {  }
1750.1.1   = { citysize = 13910 }
1800.1.1   = { citysize = 15192 }
