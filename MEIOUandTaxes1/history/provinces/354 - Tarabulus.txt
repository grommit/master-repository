# 354 - Tarabullus (Tripolotana)

owner = HAF
controller = HAF
culture = libyan
religion = sunni
capital = "Tarabalus"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = KBO
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

900.1.1    = { set_province_flag = barbary_port }
1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_center_of_trade_modifier
		duration = -1
	}
}
1088.1.1 = { dock = yes shipyard = yes }
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1356.1.1  = {
	add_core = HAF
	add_core = TRP
}
1510.1.1  = {
	owner = CAS
	controller = CAS
	add_core = CAS
}
1516.1.23 = {
	owner = SPA	
	controller = SPA
	add_core = SPA
	remove_core = CAS
}
1528.1.1  = {
	owner = KNI
	controller = KNI
	add_core = KNI
	remove_core = SPA
	remove_core = HAF
	add_core = TRP
} # Ferdinand gave Tripoli to the Knights of St John
1530.1.3 = {
	road_network = no paved_road_network = yes 
	add_claim = TUR
}
1551.1.1  = {
	owner = TRP
	controller = TRP
	add_core = TRP
	remove_core = KNI
} # Under direct Ottoman control until 1629
1609.1.1  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Janissary revolt
1610.1.1  = { revolt = {} controller = TUR }
1711.1.1 = {
	owner = TRP
	controller = TRP
} # The Karamanli pashas gain some autonomy as the Ottoman central power weakens
