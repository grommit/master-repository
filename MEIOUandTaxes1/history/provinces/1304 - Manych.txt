# 1304 - Manych
# MEIOU-GG - Turko-Mongol mod

owner = WHI
controller = WHI
culture = astrakhani
religion = sunni
capital = "Cherny Yar"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	unrest = 3
	add_core = WHI
	add_core = AST
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1459.1.1 = {
	owner = AST
	controller = AST
	remove_core = GOL
} # Independance of Astrakhan
1515.1.1 = { training_fields = yes }
1556.1.1   = {
	owner = NOG
	controller=  NOG
	add_core = NOG
	add_core = RUS
	remove_core = AST
}
1634.1.1   = {
	owner = RUS
	controller = RUS
	remove_core = NOG
	culture = russian
	religion = orthodox
} # Conquest of the Khanante by Ivan Grozny
