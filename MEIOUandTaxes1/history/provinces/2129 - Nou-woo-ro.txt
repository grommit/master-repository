# 2129 - Nou-woo-ro

culture = aboriginal
religion = polynesian_religion
trade_goods = unknown #grain
capital = "Nou-woo-ro"
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 1

1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
1788.1.26 = {
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant #anglican
	citysize = 350
	capital = "Eden"
	trade_goods = livestock
	set_province_flag = trade_good_set
}
1813.1.1 = { add_core = GBR }
