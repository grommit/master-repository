# No previous file for Ossossane

owner = HUR
controller = HUR
add_core = HUR
is_city = yes
culture = huron
religion = totemism
capital = "Ossossane"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 2
native_hostileness = 5

1649.1.1  = {
	owner = IRO
	controller = IRO
	add_core = IRO
	culture = iroquois
} #Taken by Iroquois in Beaver Wars.
1650.1.1 = {
	owner = OJI
	controller = OJI
	add_core = OJI
	culture = anishinabe
} #Iroquois focus on Lake Ontario, Ojibwe moves in
