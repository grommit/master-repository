# 1181 - Mocimboa

owner = KIL
controller = KIL
add_core = KIL
culture = kimgao
religion = sunni
capital = "Mocimboa"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = east_african
discovered_by = indian
hre = no

1356.1.1 = {
	add_core = KIL
}
1498.3.16 = {
	discovered_by = POR
} #Vasco Da Gama
1520.1.1  = {
	#owner = POR
	#controller = POR
	#add_core = POR
	naval_arsenal = yes
	customs_house = yes 
	marketplace = yes
}
1600.1.1  = {
	discovered_by = TUR
}
1730.1.1  = {
	owner = POR
	controller = POR
	add_core = POR
}
1763.1.1  = {
	unrest = 7
}
1784.1.1  = {
	owner = OMA
	controller = OMA
	add_core = OMA
} #Omanis impose direct rule in Kilwa
1856.6.1 = {
	owner = ZAN
	controller = ZAN
   	remove_core = OMA
} # Said's will divided his dominions into two separate principalities, with Thuwaini to become the Sultan of Oman and Majid to become the first Sultan of Zanzibar.
