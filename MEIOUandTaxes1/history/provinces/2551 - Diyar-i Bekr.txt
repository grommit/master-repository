# 2551 - Diyar-i Bekr

owner = QAR
controller = QAR
culture = ge_armenian
religion = coptic
capital = "Diyar-i Bekr"
trade_goods = cloth
hre = no
base_tax = 10
base_production = 10
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = QAR
	add_permanent_province_modifier = {
		name = "beylik_of_artuqids"
		duration = -1
	}
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1406.1.1   = {
	owner = AKK
	controller = AKK
} # Independance granted by Tamerlane
1409.1.1 = {
	remove_province_modifier = beylik_of_artuqids
}
1444.1.1 = {
	remove_core = JAI
}
1453.1.1  = { discovered_by = western }
1507.1.1  = {
	controller = SAM
}
1508.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = PER
	remove_core = SAM
	estate = estate_nobles
} # Safawids "form persia"
1514.8.23 = { add_core = TUR  } # Diyarbakir conquered by Ottomans, Van remains Persian

1530.1.4  = {
	bailiff = yes	
}
1534.7.1  = { controller = TUR } # Wartime occupation
1535.1.1  = { controller = PER } # Persians recapture Van
1548.8.25 = { controller = TUR }
1549.12.1 = {
	owner = TUR
	remove_core = PER		
} # Part of the Ottoman empire
1722.1.1  = { unrest = 5 } # Rebellion against the Ottomans
1730.1.1  = { unrest = 0 }
1756.1.1  = {
	culture = kurdish
	religion =  sunni
}
