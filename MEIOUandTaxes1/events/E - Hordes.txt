# Horde Events

# This file is for horde-specific events actually visable to the player. For invisible events, use M - Hordes

namespace = horde_events

##########################
# Chaghatai Horde Events #
##########################

# A man requests a tumen formerly belonging to his father

#country_event = {
#	id = horde_events.1
#	title = horde_events.1.t
#	desc = horde_events.1.d
#	picture = HORDE_ON_HORSEBACK_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		tag = CHG
#		NOT = { is_year = 1360 }
#	}
#	
#	mean_time_to_happen = {
#		months = 8
#	}
#	
#	option = {
#		name = horde_events.1.a # Concede the tumen
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_influence_modifier = {
#				estate = estate_nomadic_tribes
#				desc = tribal_concessions_est
#				influence = 20
#				duration = 3650
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = tribal_concessions_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 10
#		}
#	}
#	option = {
#		name = horde_events.1.b # deny him the tumen
#		hidden_effect = {
#			set_ruler_flag = denied_tumen
#		}
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_loyalty = {
#				estate = estate_nomadic_tribes
#				loyalty = -15
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = denied_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#		factor = 90
#		}
#	}
#}
#
## The son of Borolday returns to exact revenge
#
#country_event = {
#	id = horde_events.2
#	title = horde_events.2.t
#	desc = horde_events.2.d
#	picture = ASSASSINATION_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		tag = CHG
#		has_ruler_flag = denied_tumen
#	}
#	
#	mean_time_to_happen = {
#		months = 8
#		modifier = {
#			factor = 0.5
#			NOT = {
#				estate_loyalty = {
#					estate = estate_nomadic_tribes
#					loyalty = 30
#				}
#			}
#		}
#		modifier = {
#			factor = 2.0
#			estate_loyalty = {
#				estate = estate_nomadic_tribes
#				loyalty = 60
#			}
#		}
#	}
#	
#	option = {
#		name = horde_events.2.a # die
#		kill_ruler = yes
#	}
#
#}
#
## Abdullah's decision to move the capital
#
#country_event = {
#	id = horde_events.3
#	title = horde_events.3.t
#	desc = horde_events.3.d
#	picture = CITY_VIEW_eventPicture
#	
#	is_triggered_only = yes # on_monarch_death
#	
#	fire_only_once = yes
#	
#	trigger = {
#		tag = CHG
#		has_ruler = "Abdullah"
#		NOT = { has_country_flag = abdullah_not_in_samarqand }
#		NOT = { capital = 457 } # makes no sense if Samarqand is the capital
#		owns_core_province = 457 # they should also actually own Samarqand
#	}
#	
#	option = {
#		name = horde_events.3.a # move capital to Samarqand
#		hidden_effect = {
#			set_ruler_flag = capital_samarqand
#		}
#		set_capital = 457 # Samarqand
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_loyalty = {
#				estate = estate_nomadic_tribes
#				loyalty = -15
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = encroached_on_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 90
#		}
#	}
#	option = {
#		name = horde_events.3.b # keep capital in the south
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_influence_modifier = {
#				estate = estate_nomadic_tribes
#				desc = backed_down_from_tribes_est
#				influence = 20
#				duration = 3650
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = backed_down_from_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 10
#		}
#	}
#}
#
## The tribes demand the throne!
#
#country_event = {
#	id = horde_events.4
#	title = horde_events.4.t
#	desc = horde_events.4.d
#	picture = HORDE_ON_HORSEBACK_eventPicture
#	
#	fire_only_once = yes
#	
#	immediate = {
#		hidden_effect = {
#			set_country_flag = buyan_suldus_revolt
#		}
#	}
#	trigger = {
#		OR = {
#			has_ruler_flag = capital_samarqand
#			OR = {
#				AND = {
#					has_dlc = "The Cossacks"
#					NOT = {
#						estate_loyalty = {
#							estate = estate_nomadic_tribes
#							loyalty = 30
#						}
#					}
#				}
#				AND = {
#					NOT = {
#						has_dlc = "The Cossacks"
#						has_estate = estate_nomadic_tribes
#					}
#					average_unrest = 5
#				}
#			}
#		}
#		NOT = { is_year = 1365 }
#	}
#
#	mean_time_to_happen = {
#		months = 7
#		
#		modifier = {
#			factor = 0.7
#			AND = {
#				has_ruler_flag = capital_samarqand
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#				NOT = {
#					estate_loyalty = {
#						estate = estate_nomadic_tribes
#						loyalty = 30
#					}
#				}
#			}
#		}
#		modifier = {
#			factor = 0.7
#			average_unrest = 6
#		}
#	}
#	
#	option = {
#		name = horde_events.4.a # to war!
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#			}
#			every_owned_province = {
#				limit = {
#					has_estate = estate_nomadic_tribes
#				}
#				spawn_rebels = {
#					type = tribal_rebels
#					size = 0.5
#				}
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#				}
#			}
#			every_owned_province = {
#				limit = {
#					local_autonomy = 80
#				}
#				spawn_rebels = {
#					type = tribal_rebels
#					size = 0.5
#				}
#			}
#		}
#		457 = { # Samarqand
#			spawn_rebels = {
#				type = tribal_rebels
#				size = 2
#				leader = "Hajji Beg"
#			}
#		}
#		ai_chance = {
#			factor = 15
#		}
#	}
#	option = {
#		name = horde_events.4.b # concede defeat
#		country_event = { 
#			id = horde_events.5
#			days = 1
#			tooltip = horde_events.4.b.t
#		}
#		ai_chance = {
#			factor = 85
#		}
#	}
#}
#
## The throne is usurped by Buyan Suldus
#
#country_event = {
#	id = horde_events.5
#	title = horde_events.5.t
#	desc = horde_events.5.d
#	picture = HORDE_ON_HORSEBACK_eventPicture
#	
#	fire_only_once = yes
#	
#	is_triggered_only = yes
#	
#	trigger = {
#		has_country_flag = buyan_suldus_revolt # very explicit circumstances here
#	}
#	
#	option = {
#		name = horde_events.5.a
#		hidden_effect = {
#			define_ruler = {
#				name = "Buyan"
#				dynasty = "Suldus"
#				ADM = 0
#				DIP = 0
#				MIL = 0
#				fixed = yes
#			}
#			kill_heir = yes # in case Abdullah had a kid
#			clr_country_flag = buyan_suldus_revolt
#		}
#	}
#
#}
#
## Alternate path: the tribes demand Qazagham's son to leave Samarqand
#
#country_event = {
#	id = horde_events.6
#	title = horde_events.6.t
#	desc = horde_events.6.d
#	picture = HORDE_ON_HORSEBACK_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		NOT = { capital = 457 } # makes no sense if Samarqand is the capital
#		owns_core_province = 457 # they should also actually own Samarqand
#		has_heir = "Abdullah"
#		OR = {
#			AND = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#				estate_influence = {
#					estate = estate_nomadic_tribes
#					influence = 60
#				}
#			}
#			average_home_autonomy = 80
#		}
#	}
#	mean_time_to_happen = {
#		months = 12
#		modifier = {
#			factor = 0.5
#			OR = {
#				AND = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#					NOT = {
#						estate_loyalty = {
#							estate = estate_nomadic_tribes
#							loyalty = 40
#						}
#					}
#				}
#				AND = {
#					NOT = {
#						has_dlc = "The Cossacks"
#						has_estate = estate_nomadic_tribes
#					}
#					average_unrest = 3
#				}
#			}
#			modifier = {
#				factor = 0.5
#				OR = {
#					AND = {
#						has_dlc = "The Cossacks"
#						has_estate = estate_nomadic_tribes
#						estate_influence = {
#							estate = estate_nomadic_tribes
#							influence = 70
#						}
#					}
#					AND = {
#						NOT = {
#							has_dlc = "The Cossacks"
#							has_estate = estate_nomadic_tribes
#						}
#						average_home_autonomy = 90
#					}
#				}
#			}
#		}
#	}
#	
#	option = {
#		name = horde_events.6.a # concede
#		hidden_effect = {
#			set_country_flag = abdullah_not_in_samarqand
#		}
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_influence_modifier = {
#				estate = estate_nomadic_tribes
#				desc = backed_down_from_tribes_est
#				influence = 20
#				duration = 3650
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = backed_down_from_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 60
#		}
#	}
#	option = {
#		name = horde_events.6.b # leave Abdullah in control
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_loyalty = {
#				estate = estate_nomadic_tribes
#				loyalty = -15
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = encroached_on_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 15
#		}
#	}
#	option = {
#		name = horde_events.6.c # outright move the capital to Samarqand out of spite
#		hidden_effect = {
#			set_ruler_flag = capital_samarqand
#		}
#		set_capital = 457 # Samarqand
#		if = {
#			limit = {
#				has_dlc = "The Cossacks"
#				has_estate = estate_nomadic_tribes
#			}
#			add_estate_loyalty = {
#				estate = estate_nomadic_tribes
#				loyalty = -30
#			}
#			add_estate_influence_modifier = {
#				estate = estate_nomadic_tribes
#				desc = curtailed_tribes_est
#				influence = -20
#				duration = 3650
#			}
#		}
#		if = {
#			limit = {
#				NOT = {
#					has_dlc = "The Cossacks"
#					has_estate = estate_nomadic_tribes
#				}
#			}
#			add_ruler_modifier = {
#				name = encroached_on_tribes_mod
#				duration = 7300
#			}
#			add_ruler_modifier = {
#				name = curtailed_tribes_mod
#				duration = 3650
#			}
#		}
#		ai_chance = {
#			factor = 25
#		}
#	}
#}