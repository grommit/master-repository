########################################
# Major Flavor Events                  #
########################################

namespace = flavor_fra

# Samuel de Champlain and the Hurons
country_event = {
	id = flavor_fra.3115
	title = "flavor_fra.EVTNAME3115"
	desc = "flavor_fra.EVTDESC3115"
	picture = MERCHANTS_TALKING_eventPicture
	
	trigger = {
		NOT = { has _global_flag = north_amerindian_removed }
		tag = FRA
		north_america_superregion = {
			owned_by = FRA
			is_colony = yes
		}
		any_known_country = {
			OR = {
				tag = HUR
				tag = IRO
				tag = SHA
				tag = CHE
				tag = CRE
			}
			NOT = { alliance_with = FRA }
			NOT = { is_subject = no }
		}
		is_year = 1600
		NOT = { is_year = 1650 }
		NOT = { has_country_flag = FRA_had_event_3115 }
	}

	mean_time_to_happen = {
		months = 1000
	}
	
	immediate = {
		set_country_flag = FRA_had_event_3115
	}

	option = {		# Let us befriend the Hurons
		name = "flavor_fra.EVTOPTA3115"
		trigger = {
			knows_country = HUR
			NOT = { alliance_with = HUR }
			HUR = { NOT = { is_subject = no } }
		}
		HUR = {
			add_opinion = {
				who = FRA
				modifier = samuel_de_champlain
			}
		}
	}
	option = {		# Let us befriend the Iroquois
		name = "flavor_fra.EVTOPTB3115"
		trigger = {
			knows_country = IRO
			NOT = { alliance_with = IRO }
			IRO = { NOT = { is_subject = no } }
		}
		IRO = {
			add_opinion = {
				who = FRA
				modifier = samuel_de_champlain
			}
		}
	}
	option = {		# Let us befriend the Shawnee
		name = "flavor_fra.EVTOPTC3115"
		trigger = {
			knows_country = SHA
			NOT = { alliance_with = SHA }
			SHA = { NOT = { is_subject = no } }
		}
		SHA = {
			add_opinion = {
				who = FRA
				modifier = samuel_de_champlain
			}
		}
	}
	option = {		# Let us befriend the Cherokee
		name = "flavor_fra.EVTOPTD3115"
		trigger = {
			knows_country = CHE
			NOT = { alliance_with = CHE }
			CHE = { NOT = { is_subject = no } }
		}
		CHE = {
			add_opinion = {
				who = FRA
				modifier = samuel_de_champlain
			}
		}
	}	
	option = {		# Let us befriend the Creek
		name = "flavor_fra.EVTOPTE3115"
		trigger = {
			knows_country = CRE
			NOT = { alliance_with = CRE }
			CRE = { NOT = { is_subject = no } }
		}
		CRE = {
			add_opinion = {
				who = FRA
				modifier = samuel_de_champlain
			}
		}
	}
}
