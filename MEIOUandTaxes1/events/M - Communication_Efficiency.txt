#########################################
#		Communication Efficiency		#
# 		  by Sun_Wu and Malorn			#
#########################################


country_event = {
	id = comm_effic.1
	title = "comm_effic.1.t"
	desc = "comm_effic.1.d"
	picture = PALAIS_MAZARIN_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	option = {
		name = "comm_effic.1.a"
		every_owned_province = {
			set_variable = { which = "Distance" value = 21 }
			clr_province_flag = DistanceSet
			sea_zone = {
				clr_province_flag = DistanceSet
				set_variable = { which = "Distance" value = 21 }
			}
			
			remove_province_modifier = perfect_communication
			remove_province_modifier = rapid_communication
			remove_province_modifier = quick_communication
			remove_province_modifier = fairly_quick_communication
			remove_province_modifier = decent_communication
			remove_province_modifier = average_communication
			remove_province_modifier = moderate_communication
			remove_province_modifier = difficult_communication
			remove_province_modifier = poor_communication
			remove_province_modifier = very_poor_communication
			remove_province_modifier = terrible_communication
		}
		
		# Roads, and Terrain
		every_owned_province = {
			if = {
				limit = {
					has_building = road_and_rail_network
				}
				subtract_variable = { which = "Distance" value = 2 }
			}
			if = {
				limit = {
					OR = {
						has_building = highway_network
						has_building = paved_road_network
					}
				}
				subtract_variable = { which = "Distance" value = 1 }
			}
			if = {
				limit = {
					owner = { has_country_modifier = military_aristocracy }
				}
				change_variable = { which = "Distance" value = 1 }
			}
			#terrain
			if = {
				limit = {
					OR = {
						has_terrain = forest
						has_terrain = hills
						has_terrain = jungle
						has_terrain = marsh
						has_terrain = desert
						has_terrain = taiga
						has_terrain = coastal_desert
					}
					AND = {
						NOT = { has_building = road_and_rail_network }
						NOT = { has_building = highway_network }
						NOT = { has_building = paved_road_network }
						NOT = { has_building = road_network }
					}
				}
				change_variable = { which = "Distance" value = 1 }
			}
			if = {
				limit = {
					OR = {
						has_terrain = jungle
						has_terrain = marsh
						has_terrain = taiga
						has_terrain = tundra
						has_terrain = mountain
						has_terrain = desert_mountain
						has_terrain = highlands
					}
					AND = {
						NOT = { has_building = road_and_rail_network }
						NOT = { has_building = highway_network }
					}
				}
				change_variable = { which = "Distance" value = 1 }
			}
			if = {
				limit = {
					OR = {
						has_terrain = arctic
						has_terrain = mountain
						has_terrain = desert_mountain
					}
				}
				change_variable = { which = "Distance" value = 1 }
			}
			if = {
				limit = {
					has_province_modifier = oasis_route
				}
				change_variable = { which = "Distance" value = 3 }
			}
			if = {
				limit = { 
					owner = { has_active_policy = relay_system }
					has_building = regional_capital_building
				}
				subtract_variable = { which = "Distance" value = 1 }
			}	
			if = {
				limit = { 
					owner = { has_active_policy = policing_the_roads }
					has_building = regional_capital_building
				}
				subtract_variable = { which = "Distance" value = 1 }
			}
		}
		# Capital and surrounding provinces
		capital_scope = {
			if = {
				limit = { owner = { has_active_policy = relay_system } }
				subtract_variable = { which = "Distance" value = 1 }
			}	
			if = {
				limit = { owner = { has_active_policy = policing_the_roads } }
				subtract_variable = { which = "Distance" value = 1 }
			}
			subtract_variable = { which = "Distance" value = 21 }
			
			#If capital at -4
			if = {
				limit = { 
					NOT = {	has_province_flag = DistanceSet	}
					NOT = { check_variable = { which = "Distance" value = -3 } } 
				}
				set_province_flag = DistanceSet
				sea_zone = {
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 24 }
				}
				every_neighbor_province = {
					limit = {
						owned_by = ROOT
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 22 }
					if = {
						limit = { NOT = { check_variable = { which = "Distance" value = -2 } } }
						sea_zone = {
							if = {
								limit = {
									NOT = {	has_province_flag = DistanceSet	}
								}
								set_province_flag = DistanceSet
								subtract_variable = { which = "Distance" value = 23 }
							}
						}
						every_neighbor_province = {
							limit = {
								owned_by = ROOT
								NOT = {	has_province_flag = DistanceSet	}
							}
							set_province_flag = DistanceSet
							subtract_variable = { which = "Distance" value = 21 }
						}
					}
				}
			}
			
			#If capital at -3
			if = {
				limit = { 
					NOT = {	has_province_flag = DistanceSet	}
					NOT = { check_variable = { which = "Distance" value = -2 } } 
				}
				set_province_flag = DistanceSet
				sea_zone = {
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 23	}
				}
				every_neighbor_province = {
					limit = {
						owned_by = ROOT
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 21 }
					if = {
						limit = { NOT = { check_variable = { which = "Distance" value = -1 } } }
						sea_zone = {
							if = {
								limit = {
									NOT = {	has_province_flag = DistanceSet	}
								}
								set_province_flag = DistanceSet
								subtract_variable = { which = "Distance" value = 22 }
							}
						}
						every_neighbor_province = {
							limit = {
								owned_by = ROOT
								NOT = {	has_province_flag = DistanceSet	}
							}
							set_province_flag = DistanceSet
							subtract_variable = { which = "Distance" value = 20 }
						}
					}
				}
			}
			
			#If capital at -2
			if = {
				limit = { 
					NOT = {	has_province_flag = DistanceSet	}
					NOT = { check_variable = { which = "Distance" value = -1 } } 
				}
				set_province_flag = DistanceSet
				sea_zone = {
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 22 }
				}
				every_neighbor_province = {
					limit = {
						owned_by = ROOT
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 20 }
					if = {
						limit = { NOT = { check_variable = { which = "Distance" value = 0 } } }
						sea_zone = {
							if = {
								limit = {
									NOT = {	has_province_flag = DistanceSet	}
								}
								set_province_flag = DistanceSet
								subtract_variable = { which = "Distance" value = 21 }
							}
						}
						every_neighbor_province = {
							limit = {
								owned_by = ROOT
								NOT = {	has_province_flag = DistanceSet	}
							}
							set_province_flag = DistanceSet
							subtract_variable = { which = "Distance" value = 19 }
						}
					}
				}
			}
			
			#If capital at -1
			if = {
				limit = { 
					NOT = {	has_province_flag = DistanceSet	}
					NOT = { check_variable = { which = "Distance" value = 0 } } 
				}
				set_province_flag = DistanceSet
				sea_zone = {
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 21 }
				}
				every_neighbor_province = {
					limit = {
						owned_by = ROOT
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 19 }
					if = {
						limit = { NOT = { check_variable = { which = "Distance" value = 1 } } }
						sea_zone = {
							if = {
								limit = {
									NOT = {	has_province_flag = DistanceSet	}
								}
								set_province_flag = DistanceSet
								subtract_variable = { which = "Distance" value = 20 }
							}
						}
						every_neighbor_province = {
							limit = {
								owned_by = ROOT
								NOT = {	has_province_flag = DistanceSet	}
							}
							set_province_flag = DistanceSet
							subtract_variable = { which = "Distance" value = 18 }
						}
					}
				}
			}
			
			#If capital at 0
			if = {
				limit = { 
					NOT = {	has_province_flag = DistanceSet	}
					NOT = { check_variable = { which = "Distance" value = 1 } } 
				}
				set_province_flag = DistanceSet
				sea_zone = {
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 20 }
				}
				every_neighbor_province = {
					limit = {
						owned_by = ROOT
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 18 }
					if = {
						limit = { NOT = { check_variable = { which = "Distance" value = 2 } } }
						sea_zone = {
							if = {
								limit = {
									NOT = {	has_province_flag = DistanceSet	}
								}
								set_province_flag = DistanceSet
								subtract_variable = { which = "Distance" value = 19 }
							}
						}
						every_neighbor_province = {
							limit = {
								owned_by = ROOT
								NOT = {	has_province_flag = DistanceSet	}
							}
							set_province_flag = DistanceSet
							subtract_variable = { which = "Distance" value = 17 }
						}
					}
				}
			}
			
		}

		# CONDITIONAL Neighboring Distance -3
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -3 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = DistanceSet }
					OR = {
						any_neighbor_province = {
							NOT = { check_variable = { which = "Distance" value = -2 } }
							owned_by = ROOT
						}
						sea_zone = {
							NOT = { check_variable = { which = "Distance" value = -2 } }
						}
					}
				}
				set_province_flag = DistanceSet
				subtract_variable = { which = "Distance" value = 21 }
			}		
		}

		# CONDITIONAL Neighboring Distance -2 SEA
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -3 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { check_variable = { which = "Distance" value = -1 } }
				}
				sea_zone = {
					if = {
						limit = {
							NOT = {	has_province_flag = DistanceSet	}
						}
						set_province_flag = DistanceSet
						subtract_variable = { which = "Distance" value = 22 }
					}
				}
			}		
		}			
		# CONDITIONAL Neighboring Distance -2
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -2 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = DistanceSet }
					OR = {
						any_neighbor_province = {
							NOT = { check_variable = { which = "Distance" value = -1 } }
							owned_by = ROOT
						}
						sea_zone = {
							NOT = { check_variable = { which = "Distance" value = -1 } }
						}
					}
				}
				set_province_flag = DistanceSet
				subtract_variable = { which = "Distance" value = 20 }
			}		
		}	

		# CONDITIONAL Neighboring Distance -1 SEA
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -2 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { check_variable = { which = "Distance" value = 0 } }
				}
				sea_zone = {
					if = {
						limit = {
							NOT = {	has_province_flag = DistanceSet	}
						}
						set_province_flag = DistanceSet
						subtract_variable = { which = "Distance" value = 21 }
					}
				}
			}		
		}			
		# CONDITIONAL Neighboring Distance -1
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -1 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = DistanceSet }
					OR = {
						any_neighbor_province = {
							NOT = { check_variable = { which = "Distance" value = 0 } }
							owned_by = ROOT
						}
						sea_zone = {
							NOT = { check_variable = { which = "Distance" value = 0 } }
						}
					}
				}
				set_province_flag = DistanceSet
				subtract_variable = { which = "Distance" value = 19 }
			}		
		}

		# CONDITIONAL Neighboring Distance 0 SEA
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = -1 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { check_variable = { which = "Distance" value = 1 } }
				}
				sea_zone = {
					if = {
						limit = {
							NOT = {	has_province_flag = DistanceSet	}
						}
						set_province_flag = DistanceSet
						subtract_variable = { which = "Distance" value = 20 }
					}
				}
			}		
		}			
		# CONDITIONAL Neighboring Distance 0
		if = {
			limit = {
				capital_scope = { NOT = { check_variable = { which = "Distance" value = 0 } } }
			}
			every_owned_province = {
				limit = {
					NOT = { has_province_flag = DistanceSet }
					OR = {
						any_neighbor_province = {
							NOT = { check_variable = { which = "Distance" value = 1 } }
							owned_by = ROOT
						}
						sea_zone = {
							NOT = { check_variable = { which = "Distance" value = 1 } }
						}
					}
				}
				set_province_flag = DistanceSet
				subtract_variable = { which = "Distance" value = 18 }
			}		
		}
		
		# Neighboring Distance 1 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 2 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 19 }
				}
			}
		}			
		# Neighboring Distance 1
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 2 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 2 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 17 }
		}		

	
		# Regional Capitals set to 2 distance
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				has_building = regional_capital_building
				NOT = { check_variable = { which = "Distance" value = 18 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}
		# Neighboring Distance 2 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 3 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {	has_province_flag = DistanceSet	}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 18 }
				}
			}
		}	
		# Neighboring Distance 2
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 3 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 3 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 16 }
		}

		# Regional Capitals set to 3 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 19 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}
		# Neighboring Distance 3 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 4 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 17 }
				}
			}
		}
		# Neighboring Distance 3
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 4 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 4 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}

		# Regional Capitals set to 4 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 20 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}
		# Neighboring Distance 4 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 5 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 16 }
				}
			}
		}
		# Neighboring Distance 4
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 5 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 5 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 14 }
		}

		# Regional Capitals set to 5 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 21 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}		
		# Neighboring Distance 5 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 6 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 15 }
				}
			}
		}
		# Neighboring Distance 5
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 6 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 6 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 13 }
		}

		# Regional Capitals set to 6 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 22 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}			
		# Neighboring Distance 6 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 7 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 14 }
				}
			}
		}
		# Neighboring Distance 6
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 7 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 7 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 12 }
		}

		# Regional Capitals set to 7 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 23 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}	
		# Neighboring Distance 7 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 8 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 13 }
				}
			}
		}
		# Neighboring Distance 7
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 8 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 8 } }
					}
					AND = {
						owner = { has_active_policy = organized_counter_piracy }
						has_building = naval_base
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 11 }
		}	

		# Regional Capitals set to 8 distance
		every_owned_province = {
			limit = {
				has_building = regional_capital_building
				NOT = { has_province_flag = DistanceSet }
				NOT = { check_variable = { which = "Distance" value = 24 } }
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 15 }
		}			
		# Neighboring Distance 8 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 9 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 12 }
				}
			}
		}
		# Neighboring Distance 8 (naval base)
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 9 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 9 } }
					}
					has_building = naval_base
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 10 }
		}

		# Neighboring Distance 9 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 10 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 11 }
				}
			}
		}
		# Neighboring Distance 9
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 10 } }	
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 10 } }
					}
					AND = {
						owner = { has_active_policy = organized_counter_piracy }
						has_building = naval_arsenal
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 9 }
		}

		# Neighboring Distance 10 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 11 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 10 }
				}
			}
		}
		# Neighboring Distance 10 (naval_arsenal)
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 11 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 11 } }
					}
					has_building = naval_arsenal
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 8 }
		}

		# Neighboring Distance 11 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 12 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 9 }
				}
			}
		}
		# Neighboring Distance 11
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 12 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 12 } }
					}
					AND = {
						owner = { has_active_policy = organized_counter_piracy }
						has_building = dock
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 7 }
		}
		
		# Neighboring Distance 12 SEA
		every_owned_province = {
			limit = {
				NOT = { check_variable = { which = "Distance" value = 13 } }
			}
			sea_zone = {
				if = {
					limit = {
						NOT = {
							has_province_flag = DistanceSet
						}
					}
					set_province_flag = DistanceSet
					subtract_variable = { which = "Distance" value = 8 }
				}
			}
		}
		# Neighboring Distance 12 (Dock)
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 13 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 13 } }
					}
					has_building = dock
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 6 }
		}		
		
		# Neighboring Distance 13
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 14 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 14 } }
					}
					AND = {
						owner = { has_active_policy = organized_counter_piracy }
						has_port = yes
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 5 }
		}					

		# Neighboring Distance 14 (Port)
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 15 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 15 } }
					}
					has_port = yes
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 4 }
		}
		
		# Neighboring Distance 15
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 16 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 16 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 3 }
		}
		
		# Neighboring Distance 16
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 17 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 17 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 2 }
		}
		
		# Neighboring Distance 17
		every_owned_province = {
			limit = {
				NOT = { has_province_flag = DistanceSet }
				OR = {
					any_neighbor_province = {
						NOT = { check_variable = { which = "Distance" value = 18 } }
						owned_by = ROOT
					}
					sea_zone = {
						NOT = { check_variable = { which = "Distance" value = 18 } }
					}
				}
			}
			set_province_flag = DistanceSet
			subtract_variable = { which = "Distance" value = 1 }
		}
		country_event = { id = comm_effic.2 days = 3 }
	}	
}

country_event = {
	id = comm_effic.2
	title = "comm_effic.2.t"
	desc = "comm_effic.2.d"
	picture = PALAIS_MAZARIN_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	option = {
		name = "comm_effic.2.a"
		every_owned_province = {
			clr_province_flag = CE_Set
			
			remove_province_modifier = perfect_communication
			remove_province_modifier = rapid_communication
			remove_province_modifier = quick_communication
			remove_province_modifier = fairly_quick_communication
			remove_province_modifier = decent_communication
			remove_province_modifier = average_communication
			remove_province_modifier = moderate_communication
			remove_province_modifier = difficult_communication
			remove_province_modifier = poor_communication
			remove_province_modifier = very_poor_communication
			remove_province_modifier = terrible_communication
		}
		
		
		#applies modifiers
		every_owned_province = {
			limit = { 
				is_colony = no
			}
			#1
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 1 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "perfect_communication" 
					duration = -1 
				}
			}
			#2
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 3 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "rapid_communication" 
					duration = -1 
				}
			}
			#3
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 5 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "quick_communication"
					duration = -1 
				}
			}
			#4
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 7 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "fairly_quick_communication" 
					duration = -1 
				}
			}
			#5
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 9 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "decent_communication" 
					duration = -1 
				}
			}
			#6
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 11 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "average_communication"
					duration = -1 
				}
			}
			#7
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 13 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "moderate_communication"
					duration = -1 
				}
			}
			#8
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 15 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "difficult_communication"
					duration = -1 
				}
			}
			#9
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 17 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "poor_communication" 
					duration = -1 
				}
			}			
			#10
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
					NOT = { check_variable = { which = "Distance" value = 19 } }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "very_poor_communication"
					duration = -1 
				}
			}
			#11
			if = {
				limit = { 
					NOT = { has_province_flag = CE_Set }
				}
				set_province_flag = CE_Set
				add_province_modifier = { 
					name = "terrible_communication"
					duration = -1 
				}
			}
		}
	}
}