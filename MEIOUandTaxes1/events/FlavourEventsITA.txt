#########################################
#     Major Flavor Events for Italy     #
#########################################


# Emperor chooses to relinquish Iron Crown
country_event = {
	id = flavor_ita.1
	title = "flavor_ita.1.n"
	desc = "flavor_ita.1.t"
	picture = IRONCROWN_eventPicture

	is_triggered_only = yes

	option = {
		name = "flavor_ita.1.a"			#Accept
		ai_chance = {
			factor = 50
			modifier = {
				has_opinion = { who = FROM value = 50 }
				factor = 1.10
			}
			modifier = {
				has_opinion = { who = FROM value = 100 }
				factor = 1.10
			}
			modifier = {
				has_opinion = { who = FROM value = 150 }
				factor = 1.10
			}
			modifier = {
				has_opinion = { who = FROM value = 200 }
				factor = 1.10
			}
			modifier = {
				num_of_owned_provinces_with = {
					value = 1
					region = italy_region
				}
				factor = 0.90
			}
			modifier = {
				num_of_owned_provinces_with = {
					value = 5
					region = italy_region
				}
				factor = 0.90
			}
			modifier = {
				num_of_owned_provinces_with = {
					value = 10
					region = italy_region
				}
				factor = 0.90
			}
			modifier = {
				FROM = {
					num_of_owned_provinces_with = {
						value = 15
						OR = { region = italy_region region = sicily_region }
					}
				}
				factor = 1.10
			}
			modifier = {
				FROM = {
					num_of_owned_provinces_with = {
						value = 20
						OR = { region = italy_region region = sicily_region }
					}
				}
				factor = 1.10
			}
			modifier = {
				FROM = {
					num_of_owned_provinces_with = {
						value = 25
						OR = { region = italy_region region = sicily_region }
					}
				}
				factor = 1.10
			}
			modifier = {
				FROM = {
					num_of_owned_provinces_with = {
						value = 30
						OR = { region = italy_region region = sicily_region }
					}
				}
				factor = 1.10
			}
		}
		FROM = {
			country_event = { id = flavor_ita.2 days = 5 }
		}
		# loss of imperial authority
	}

	option = {
		name = "flavor_ita.1.b"			#Refuse
		ai_chance = {
			factor = 50
			modifier = {
				NOT = { has_opinion = { who = FROM value = -50 } }
				factor = 1.10
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = -100 } }
				factor = 1.10
			}
			modifier = {
				NOT = { has_opinion = { who = FROM value = -150 } }
				factor = 2.00
			}
		}
		FROM = {
			country_event = { id = flavor_ita.3 days = 5 }
		}
	}
}

country_event = {
	id = flavor_ita.2
	title = "flavor_ita.2.n"
	desc = "flavor_ita.2.t"
	picture = IRONCROWN_eventPicture

	is_triggered_only = yes

	option = {
		name = "flavor_ita.2.a"				# Excellent
		italy_region = { limit = { owned_by = ROOT } remove_core = ITA add_core = ITA }
		italy_region = { limit = { NOT = { owned_by = ROOT } } add_claim = ITA }
		add_prestige = 20
		if = {
			limit = { NOT = { government_rank = 5 } }
			set_government_rank = 5
		}
		inherit = ITA
		change_tag = ITA
		#latin_union_effect = yes
		#add_accepted_culture = lombard
		#add_accepted_culture = corsican
		#add_accepted_culture = piedmontese
		#add_accepted_culture = ligurian
		#add_accepted_culture = venetian
		#add_accepted_culture = emilian
		#add_accepted_culture = tuscan
		#add_accepted_culture = dalmatian
		#add_accepted_culture = umbrian
		#add_accepted_culture = romagnol
		#add_accepted_culture = friulian
		increase_centralisation = yes
		if = {
			limit = {
				has_custom_ideas = no
				NOT = { has_idea_group = ITA_ideas }
			}
			swap_national_ideas_effect = yes
		}
		country_event = { id = holyromanempire.1 days = 5 }
	}
}

country_event = {
	id = flavor_ita.3
	title = "flavor_ita.3.n"
	desc = "flavor_ita.3.t"
	picture = IRONCROWN_eventPicture

	is_triggered_only = yes

	option = {
		name = "flavor_ita.3.a"				# this is an outrage
		italy_region = { limit = { owned_by = ROOT } remove_core = ITA add_core = ITA }
		italy_region = { limit = { NOT = { owned_by = ROOT } } add_claim = ITA }
		add_prestige = 20
		if = {
			limit = { NOT = { government_rank = 5 } }
			set_government_rank = 5
		}
		inherit = ITA
		change_tag = ITA
		#latin_union_effect = yes
		#add_accepted_culture = lombard
		#add_accepted_culture = corsican
		#add_accepted_culture = piedmontese
		#add_accepted_culture = ligurian
		#add_accepted_culture = venetian
		#add_accepted_culture = emilian
		#add_accepted_culture = tuscan
		#add_accepted_culture = dalmatian
		#add_accepted_culture = umbrian
		#add_accepted_culture = romagnol
		#add_accepted_culture = friulian
		increase_centralisation = yes
		if = {
			limit = {
				has_custom_ideas = no
				NOT = { has_idea_group = ITA_ideas }
			}
			swap_national_ideas_effect = yes
		}
		country_event = { id = holyromanempire.1 days = 5 }
		FROM = {
			country_event = { id = flavor_ita.4 days = 5 }
		}
	}

	option = {
		name = "flavor_ita.3.b"				# diplomacy
		FROM = {
			country_event = { id = flavor_ita.5 days = 5 }
		}
		add_prestige = -20
	}
}

country_event = {
	id = flavor_ita.4
	title = "flavor_ita.4.n"
	desc = "flavor_ita.4.t"
	picture = IRONCROWN_eventPicture

	is_triggered_only = yes

	option = {
		name = "flavor_ita.4.a"				# this is an outrage
		every_province = {
			limit = {
				owned_by = ITA
			}
			add_claim = ROOT
		}
	}

	option = {
		name = "flavor_ita.4.a"				# let it pass
		every_province = {
			limit = {
				owned_by = ITA
			}
			set_in_empire = no
		}
	}
}

country_event = {
	id = flavor_ita.5
	title = "flavor_ita.5.n"
	desc = "flavor_ita.5.t"
	picture = IRONCROWN_eventPicture

	is_triggered_only = yes

	option = {
		name = "flavor_ita.5.a"				# this is an outrage
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = italian_crown_refused
			}
		}
	}
}
