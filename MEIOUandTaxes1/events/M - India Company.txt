#Creates a India trade country_event

namespace = trade_company_country

country_event = {	#Pick a capital
	id = trade_company_country.1
	title = trade_company_country.1.t
	desc = trade_company_country.1.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = { 
		name = trade_company_country.1.a #Calcutta
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 25
				OR = {
					culture_group = british
					culture_group = colonial_british
				}
			}
		}
		if = {
			limit = {
				OR = {
					culture_group = langue_d_oil
					culture_group = langue_d_oc
					culture_group = colonial_french
				}	
			}
			561 = { rename_capital = "Chandernagor" }
		}
		trigger = { 
			west_bengal_area = {
				type = all
				owned_by = ROOT
			}
		}
		561 = {
			set_province_flag = capital_of_india
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.b #Dacca
		ai_chance = { 
			factor = 1 
		}
		trigger = { 
			east_bengal_area = {
				type = all
				owned_by = ROOT
			}
		}
		2696 = {
			set_province_flag = capital_of_india
			if = {
				limit = {
					owner = {  
						OR = {
							culture_group = british
							culture_group = colonial_british
						}
					}
				}
				change_province_name = "Dacca"
				rename_capital = "Dacca"
			}
		}
		
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.c #Madras
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 5
				OR = {
					culture_group = british
					culture_group = colonial_british
				}
			}
		}
		trigger = { 
			coromandel_area = {
				type = all
				owned_by = ROOT
			}
		}
		542 = {
			set_province_flag = capital_of_india
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = {
		name = trade_company_country.1.e #Pondicherry
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 25
				OR = {
					culture_group = langue_d_oil
					culture_group = langue_d_oc
				}	
			}
		}
		trigger = { 
			coromandel_area = {
				type = all
				owned_by = ROOT
			}
		}
		2245 = {
			set_province_flag = capital_of_india
			if = {
				limit = {
					owner = {
						OR = {
							culture_group = langue_d_oil
							culture_group = langue_d_oc
						}	
					}
				}
				change_province_name = "Pondichéry"
				rename_capital = "Pondichéry"
			}
		}
		
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.f #Karaikal
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 5
				OR = {
					culture_group = langue_d_oil
					culture_group = langue_d_oc
				}	
			}
			modifier = {
				factor = 25
				OR = {
					culture_group = nord_germanic
					culture_group = swedish_group
				}
			}
		}
		trigger = { 
			madura_area = {
				type = all
				owned_by = ROOT
			}
		}
		537 = {
			set_province_flag = capital_of_india
			if = {
				limit = {
					owner = {
						OR = {
							culture_group = nord_germanic
							culture_group = swedish_group
						}
					}
				}
				change_province_name = "Tranquebar"
				rename_capital = "Tranquebar"
			}
			if = {
				limit = {
					owner = {
						OR = {
							culture_group = langue_d_oil
							culture_group = langue_d_oc
							culture_group = colonial_french
						}	
					}
				}
				change_province_name = "Karikal"
				rename_capital = "Karikal"
			}
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.g #Goa
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 25
				culture_group = portuguese
			}
		}
		trigger = { 
			kanara_area = {
				type = all
				owned_by = ROOT
			}
		}
		531 = {
			set_province_flag = capital_of_india
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.h #Bombay
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 5
				culture_group = portuguese
			}
		}
		trigger = { 
			konkan_area = {
				type = all
				owned_by = ROOT
			}
		}
		3144 = {
			set_province_flag = capital_of_india
			if = {
				limit = {
					owner = {  
						OR = {
							culture_group = british
							culture_group = colonial_british
						}
					}
				}
				change_province_name = "Bombay"
				rename_capital = "Bombay"
			}
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
	option = { 
		name = trade_company_country.1.i #Surat
		ai_chance = { 
			factor = 1 
		}
		trigger = { 
			gujarat_area = {
				type = all
				owned_by = ROOT
			}
		}
		
		2568 = {
			set_province_flag = capital_of_india
			if = {
				limit = {
					owner = {
						OR = {
							culture_group = langue_d_oil
							culture_group = langue_d_oc
						}	
					}
				}
				change_province_name = "Surate"
				rename_capital = "Surate"
			}
		}
		hidden_effect = {
			india_trade_company_area = {
				cede_province = ROOT
				add_core = ROOT
			}
			country_event = { id = trade_company_country.2 days = 3 }
		}
	}
}

country_event = { #Determine autonomy level desired by overlord
	id = trade_company_country.2
	title = "trade_company_country.2.t" 
	desc = "trade_company_country.2.d"
	picture = COMPANY_BASE_eventPicture

	is_triggered_only = yes
	
	option = {
		name = "trade_company_country.2.a" #Autonomous
		ai_chance = { 
			factor = 90
		}
		hidden_effect = {
			every_subject_country = {
				limit = {
					any_owned_province = {
						area = india_trade_company_area
					}
					is_colonial_nation = yes
					colonial_parent = {
						any_owned_province = {
							province_group = india_charter
						}
					}
					NOT = { has_country_flag = trade_company_country }
				}
				set_country_flag = trade_country_protectorate
				country_event = { 
					id = trade_company_country.3
				}
			}
		}
	}
	option = {
		name = "trade_company_country.2.b" #Semi-Autonomous
		ai_chance = { 
			factor = 10
		}
		hidden_effect = {
			every_subject_country = {
				limit = {
					any_owned_province = {
						area = india_trade_company_area
					}
					is_colonial_nation = yes
					colonial_parent = {
						any_owned_province = {
							province_group = india_charter
						}
					}
					NOT = { has_country_flag = trade_company_country }
				}
				country_event = { 
					id = trade_company_country.3
				}
			}
		}
	}
}


country_event = { #Configure India
	id = trade_company_country.3
	title = "trade_company_country.3.t" 
	desc = "trade_company_country.3.t"
	picture = COMPANY_BASE_eventPicture

	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			colonial_parent = {
				every_owned_province = {
					limit = {
						province_group = india_charter
						is_city = yes 
						is_overseas = yes
					}
					remove_core = THIS
					add_core = ROOT
					cede_province = ROOT
				}
			}
			if = {
				limit = { has_country_flag = trade_country_protectorate }
				colonial_parent = { 
					create_protectorate = ROOT
				}
			}
			set_country_flag = trade_company_country
			change_government = trade_company_gov
			change_technology_group = indian
			change_unit_type = indian

		}
	}
	
	option = {
		name = "trade_company_country.3.a"
		hidden_effect = {
			add_stability_3 = yes
			every_owned_province = {
				limit = { 
					owned_by = ROOT
					area = india_trade_company_area
				}
				remove_core = ROOT
				cede_province = XXX
			}
			if = {
				limit = { 561 = { has_province_flag = capital_of_india } } #Calcutta
					if = {
						limit = {
							NOT = { capital = 561 }
						}
						set_capital = 561
					}
					561 = { clr_province_flag = capital_of_india }
			}
			if = {
				limit = { 2696 = { has_province_flag = capital_of_india } } #Dacca
				if = {
					limit = {
						NOT = { capital = 2696 }
					}
					set_capital = 2696
				}
			}
			if = {
				limit = { 542 = { has_province_flag = capital_of_india } } #Madras
				if = {
					limit = {
						NOT = { capital = 542 }
					}
					set_capital = 542
					
				}
				542 = { clr_province_flag = capital_of_india }
			}
			if = {
				limit = { 2245 = { has_province_flag = capital_of_india } } #Pondicherry
				if = {
					limit = {
						NOT = { capital = 2245 }
					}
					set_capital = 2245
				}
			}
			if = {
				limit = { 537 = { has_province_flag = capital_of_india } } #Karaikal
				if = {
					limit = {
						NOT = { capital = 537 }
					}
					set_capital = 537
				}
				537 = {
					clr_province_flag = capital_of_india
				}
			}
			if = {
				limit = { 531 = { has_province_flag = capital_of_india } } #Goa
				if = {
					limit = {
						NOT = { capital = 531 }
					}
					set_capital = 531
				}
				531 = { clr_province_flag = capital_of_india }
			}
			if = {
				limit = { 3144 = { has_province_flag = capital_of_india } } #Bombay
				if = {
					limit = {
						NOT = { capital = 3144 }
					}
					set_capital = 3144
				}
			}
			if = {
				limit = { 2568 = { has_province_flag = capital_of_india } } #Surat
				if = {
					limit = {
						NOT = { capital = 2568 }
					}
					set_capital = 2568
				}
			}
			if = {
				limit = {
					NOT = { num_of_cities = 50 }
				}
				set_government_rank = 3
			}
			if = {
				limit = {
					num_of_cities = 50
					NOT = { num_of_cities = 100 }
				}
				set_government_rank = 4
			}
			if = {
				limit = {
					num_of_cities = 100
					NOT = { num_of_cities = 200 }
				}
				set_government_rank = 5
			}
			if = {
				limit = {
					num_of_cities = 200
				}
				set_government_rank = 6
			}
			every_owned_province = {
				add_nationalism = -40
				add_local_autonomy = -50
			}
			random_owned_province = {
				limit = { controlled_by = ROOT  }
				cavalry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
			}
			overlord = { country_event =  { id = trade_company_country.4 } }
			swap_free_idea_group = yes
		}
	}
}

country_event = { #Switch to India?
	id = trade_company_country.4
	title = trade_company_country.4.t
	desc = trade_company_country.4.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.4.a
		ai_chance = { factor = 0 }
		switch_tag = FROM
	}
	
	option = {
		name = trade_company_country.4.b
		ai_chance = { factor = 1 }
	}
}
#Company Rule

country_event = { #Ask for trade priviliges
	id = trade_company_country.5
	title = trade_company_country.5.t
	desc = trade_company_country.5.d
	picture = COMPANY_BASE_eventPicture
	
	trigger = {					
		government = trade_company_gov
		any_neighbor_country = {
			NOT = { technology_group = western }
			NOT = { technology_group = eastern }
			NOT = { technology_group = chinese }
			NOT = { government = trade_company_gov }
			NOT = { government = amalgamation_government }
			is_subject = no
			NOT = { alliance_with = ROOT }
			NOT = { is_rival = ROOT }
			NOT = { government_rank = 5 }
		}	
	}
	
		
	mean_time_to_happen = {
		months = 1000
		modifier = {
			factor = 0.5
			is_year = 1700	
		}
		modifier = {
			factor = 0.25
			is_year = 1750	
		}
	}
	
	option = {
		name = trade_company_country.5.a #Make the offer
		ai_chance = { factor = 100 }
		random_neighbor_country = {
			limit = {
				NOT = { technology_group = western }
				NOT = { technology_group = eastern }
				NOT = { technology_group = chinese }
				NOT = { government = trade_company_gov }
				NOT = { government = amalgamation_government }
				is_subject = no
				NOT = { alliance_with = ROOT }
				NOT = { is_rival = ROOT }
				NOT = { government_rank = 5 }
			}
			country_event = { id = trade_company_country.6 }
		}
	}
	
	option = {
		name = trade_company_country.5.b #too risky
		ai_chance = { factor = 0 }
		add_prestige = -5
	}
}

country_event = { #Trade priviliges offered
	id = trade_company_country.6
	title = trade_company_country.6.t
	desc = trade_company_country.6.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.6.a #accept
		ai_chance = { factor = 90 }
		create_alliance = FROM
		FROM = { country_event = { id = trade_company_country.7 } }
	}
	
	option = {
		name = trade_company_country.6.b #decline
		ai_chance = { factor = 10 }
		FROM = { country_event = { id = trade_company_country.8 } }
	}
}

country_event = { #Offer Accepted
	id = trade_company_country.7
	title = trade_company_country.7.t
	desc = trade_company_country.7.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.7.a #Excellent
		add_prestige = 5
	}
}

country_event = { #Offer Declined
	id = trade_company_country.8
	title = trade_company_country.8.t
	desc = trade_company_country.8.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.8.a #Crush Them
		ai_chance = { 
			factor = 90
			modifier = {
				factor = 0.1
				is_at_war = yes	
			}
		}
		declare_war_with_cb = {
			who = FROM
			casus_belli = cb_unite_the_raj
		}
	}
	
	option = {
		name = trade_company_country.8.b #Leave them be
		ai_chance = { factor = 10 }
		add_prestige = -10
	}
}
#Subsidiary Alliance
country_event = { #Subsidiary Alliance
	id = trade_company_country.9
	title = trade_company_country.9.t
	desc = trade_company_country.9.d
	picture = COMPANY_BASE_eventPicture
	
	trigger = {					
		government = trade_company_gov
		any_ally = {
			NOT = { technology_group = western }
			NOT = { technology_group = eastern }
			NOT = { technology_group = chinese }
			NOT = { government = trade_company_gov }
			is_subject = no
		}	
	}
	
		
	mean_time_to_happen = {
		months = 1000
		modifier = {
			factor = 0.5
			is_year = 1700	
		}
		modifier = {
			factor = 0.25
			is_year = 1750	
		}
		modifier = {
			factor = 0.1
			is_year = 1800	
		}
	}
	
	option = {
		name = trade_company_country.5.a #Make the offer
		ai_chance = { factor = 100 }
		random_ally = {
			limit = {
				NOT = { technology_group = western }
				NOT = { technology_group = eastern }
				NOT = { technology_group = chinese }
				NOT = { government = trade_company_gov }
				is_subject = no
			}
			country_event = { id = trade_company_country.10 }
		}
	}
	
	option = {
		name = trade_company_country.5.b #too risky
		ai_chance = { factor = 0 }
		add_prestige = -5
	}
}

country_event = { #Subsidiary Alliance offered
	id = trade_company_country.10
	title = trade_company_country.10.t
	desc = trade_company_country.10.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.6.a #accept
		ai_chance = { factor = 75 }			
		add_country_modifier = {
			name = indian_subsidiary_alliance
			duration = -1
		}
		if = {
			limit = {
				NOT = { technology_group = indian }
			}
			change_technology_group = indian
			change_unit_type = indian
		}
		FROM = { country_event = { id = trade_company_country.11 } }
	}
	
	option = {
		name = trade_company_country.6.b #decline
		ai_chance = { factor = 25 }
		add_opinion = { who = FROM modifier = reaction_tp_grabbed }
		FROM = { country_event = { id = trade_company_country.12 } }
	}
}

country_event = { #Offer Accepted
	id = trade_company_country.11
	title = trade_company_country.7.t
	desc = trade_company_country.7.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.7.a #Excellent
		vassalize = FROM
		add_prestige = 5
	}
}

country_event = { #Offer Declined
	id = trade_company_country.12
	title = trade_company_country.8.t
	desc = trade_company_country.8.d
	picture = COMPANY_BASE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = trade_company_country.8.a #Crush Them
		ai_chance = { 
			factor = 99
		}
		declare_war_with_cb = {
			who = FROM
			casus_belli = cb_unite_the_raj
		}
	}
	
	option = {
		name = trade_company_country.8.b #Leave them be
		ai_chance = { factor = 1 }
		add_prestige = -10
	}
}