# Horde mechanics

# This is for invisible horde-specific events. For events visible to the player, use E - Hordes

namespace = horde_mechanic_events


# Set up the Khanate and Puppet Khanate modifiers
country_event = {
	id = horde_mechanic_events.1
	title = horde_mechanic_events.1.t
	desc = horde_mechanic_events.1.d
	picture = PALAIS_MAZARIN_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	option = {
		name = horde_mechanic_events.1.a
		if = {
			limit = {
				NOT = { has_country_modifier = genghis_khanate }
				NOT = { has_country_modifier = puppet_khanate }
				OR = {
					dynasty = "Jochid"
					dynasty = "Chagatayid"
					dynasty = "Borjigin"
					dynasty = "Giray"
					dynasty = "Edig�id"
					dynasty = "Shaybanid"
				}
			}
			add_country_modifier = {
				name = genghis_khanate
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = genghis_khanate
				NOT = {
					dynasty = "Jochid"
					dynasty = "Chagatayid"
					dynasty = "Borjigin"
					dynasty = "Giray"
					dynasty = "Edig�id"
					dynasty = "Shaybanid"
				}
			}
			remove_country_modifier = genghis_khanate
			add_country_modifier = {
				name = puppet_khanate
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = puppet_khanate
				OR = {
					dynasty = "Jochid"
					dynasty = "Chagatayid"
					dynasty = "Borjigin"
					dynasty = "Giray"
					dynasty = "Edig�id"
					dynasty = "Shaybanid"
				}
			}
			remove_country_modifier = puppet_khanate
			add_country_modifier = {
				name = genghis_khanate
				duration = -1
			}
		}
	}
	
	trigger = {
		OR = {
			has_country_modifier = genghis_khanate
			has_country_modifier = puppet_khanate
		}
	}
}

# Give modifiers to hordes that have their forcelimit scale directly with military prowress
country_event = {
	id = horde_mechanic_events.2
	title = horde_mechanic_events.2.t
	desc = horde_mechanic_events.2.d
	picture = PALAIS_MAZARIN_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	option = {
		name = horde_mechanic_events.2.a
		if = {
			limit = {
				NOT = {
					MIL = 1
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_0
			}
		}
		if = {
			limit = {
				MIL = 1
				NOT = {
					MIL = 2
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_1
			}
		}
		if = {
			limit = {
				MIL = 2
				NOT = {
					MIL = 3
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_2
			}
		}
		if = {
			limit = {
				MIL = 3
				NOT = {
					MIL = 4
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_3
			}
		}
		if = {
			limit = {
				MIL = 4
				NOT = {
					MIL = 5
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_4
			}
		}
		if = {
			limit = {
				MIL = 5
				NOT = {
					MIL = 6
				}
			}
			add_ruler_modifier = {
				name = horde_ruler_military_5
			}
		}
		if = {
			limit = {
				MIL = 6
			}
			add_ruler_modifier = {
				name = horde_ruler_military_6
			}
		}
	}
	
	trigger = {
		OR = {
			government = steppe_horde
			government = altaic_monarchy
		}
	}
}

# Event to clean stray country flags on new successors for hordes. Feel free to make similar events for other government types, if applicable.

country_event = {
	id = horde_mechanic_events.3
	title = horde_mechanic_events.3.t
	desc = horde_mechanic_events.3.d
	picture = PALAIS_MAZARIN_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	option = {
		name = horde_mechanic_events.3.a
		if = {
			limit = {
				has_country_flag = abdullah_not_in_samarqand
				NOT = { has_ruler = "Abdullah" }
			}
			clr_country_flag = abdullah_not_in_samarqand
		}
	}
	trigger = {
		has_country_flag = abdullah_not_in_samarqand
	}
	
}